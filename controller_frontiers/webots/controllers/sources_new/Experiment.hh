#ifndef __EXPERIMENT_HH__
#define __EXPERIMENT_HH__

#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <fstream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <map>
#include <cmath>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/any.hpp>

#include "State.hh"
#include "Rob.hh"


double convertToDouble(std::string const& s);
class Phase;
using namespace std;


class Experiment
{
public:
	map<string, double> fitnesses;
	Rob *body;
	vector<Phase *> phases;
	bool last_phase;
	vector<Phase *>::iterator phase;
	ofstream *fitness_file;
	ofstream *parameters_level1_file;
	ofstream *parameters_level1_file_track;
	ofstream *parameters_level1_file_associated_state;
	map<string,vector<double>> parameters_level1_control_file;
	double max_wait;
	double waited;
	double spent_energy;
	bool changed;
	bool first_saved;
	double changed_at_dist;
	int changed_at_nbsl;
	int time_step;
	int change_number;
	// fitness
	bool last_trial();
	std::map<std::string,double> buildFitness();
	int number_of_trials();
	void initiation();
	void termination();
	int RepeatNumber();
	bool loadingEnded;
	
	int color;
	// ---------------------------------------------------
	//vector phases;
	int                     nb_parameters;
	double energylimit;  //energy we provide to the robot at each trial
	string mode;

	//  vector<string> raw_files;
	map<string, ofstream*> raw_files;
	vector<string> raw_filesname;
	map<string, double> lastparameters_level1_value;
	
	map<string, vector<double>> data_joint_angles;
	map<string, vector<double>> data_joint_torques;
	vector<double> data_leftTO;
	vector<double> data_rightTO;
	vector<double> data_time;
	vector<double> data_steplength;
	vector<double> data_steplength_left;
	vector<double> data_steplength_right;
	
	double laststeppos;
	//----------------- METHODES -----------------------
	
	
	//Experiment();
	Experiment(Rob * body);
	void init();
	void    run();//the main of this class
	void loadReflexParameters();
	void loadReflexParametersFromOpti();
	void loadReflexParametersFromFile(string);
	void loadLaunchingReflexParameters(string);
	void loadCpgParameters();
	void loadCpgParametersFromOpti();
	void loadCpgParametersFromFile(string);
	
	void createRawFiles();
	void writeRawHeader();
	void writeRawContent();
	
	void addRawData();
	int changeVectorSize(vector<double>& out, vector<double>& x, vector<double>& y, long start, long end, int N_out);
	double getJointsTorqueCorrelationWithHuman(string joint);
    double getJointsAngleCorrelationWithHuman(string joint);
	double getTorqueSum(string joint);
	double getSNRWithHuman(string joint);
	void one_trial( string mode); //a single trial that return a fitness
	void step();
	double  rnd();             //rnd number generator [0;1]
	
    void endExp();
	void saveForEvaluation(string what);
};


#endif /* __GRADIENT_HH__ */