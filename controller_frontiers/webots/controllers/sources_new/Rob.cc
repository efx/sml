#include "Rob.hh"
#include <sml/sml-tools/Settings.hh>
#include <webots/Node.hpp>
#include <boost/xpressive/xpressive.hpp>

using namespace std;
using namespace webots;
using namespace boost::xpressive;


extern double debug;
extern EventManager* eventManager;
extern SmlParameters parameters;
extern std::map<string,double> state;


void Rob::ConstantInit(){
    // Segment constant
    sml::Constant[CONSTANT::LENGTH_TRUNK] = 0.8;
    sml::Constant[CONSTANT::LENGTH_THIGH] = 0.5;
    sml::Constant[CONSTANT::LENGTH_SHIN] = 0.5;
    sml::Constant[CONSTANT::LENGTH_ANKLE] = 0.1;
    sml::Constant[CONSTANT::LENGTH_FOOT] = 0.16;
    sml::Constant[CONSTANT::WEIGHT_TRUNK] = 53.5;
    sml::Constant[CONSTANT::WEIGHT_THIGH] = 8.5;
    sml::Constant[CONSTANT::WEIGHT_SHIN] = 3.5;
    sml::Constant[CONSTANT::WEIGHT_ANKLE] = 0.0;
    sml::Constant[CONSTANT::WEIGHT_FOOT] = 1.25;
    sml::Constant[CONSTANT::MODEL_WEIGHT] = 70;
    sml::Constant[CONSTANT::MODEL_HEIGHT] = 1.8;
    
        

    
    // Joints constant
    sml::Constant[CONSTANT::REF_POS_HIP_LEFT] = 0;
    sml::Constant[CONSTANT::REF_POS_HIP_RIGHT] = 0;
    sml::Constant[CONSTANT::MAX_POS_HIP_LEFT] = 50.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
    sml::Constant[CONSTANT::MAX_POS_HIP_RIGHT] = 50.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
    sml::Constant[CONSTANT::MIN_POS_HIP_LEFT] = -160.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
    sml::Constant[CONSTANT::MIN_POS_HIP_RIGHT] = -160.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
#ifdef MODEL3D
    sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT] = 0.0;
    sml::Constant[CONSTANT::REF_POS_HIPCOR_RIGHT] = 0.0;
    sml::Constant[CONSTANT::MAX_POS_HIPCOR_LEFT] = 80.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT];
    sml::Constant[CONSTANT::MAX_POS_HIPCOR_RIGHT] = 80.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT];
    sml::Constant[CONSTANT::MIN_POS_HIPCOR_LEFT] = 0.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT];
    sml::Constant[CONSTANT::MIN_POS_HIPCOR_RIGHT] = 0.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT];
#endif
    
    

    sml::Constant[CONSTANT::REF_POS_KNEE_LEFT] = 0;
    sml::Constant[CONSTANT::REF_POS_KNEE_RIGHT] = 0;
    sml::Constant[CONSTANT::MAX_POS_KNEE_LEFT] = 135  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::MAX_POS_KNEE_RIGHT] = 135  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];    
    sml::Constant[CONSTANT::MIN_POS_KNEE_LEFT] = 5  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::MIN_POS_KNEE_RIGHT] = 5  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];



    sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT] = 0;
    sml::Constant[CONSTANT::REF_POS_ANKLE_RIGHT] = 0;
    sml::Constant[CONSTANT::MAX_POS_ANKLE_LEFT] = 40.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::MAX_POS_ANKLE_RIGHT] = 40.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::MIN_POS_ANKLE_LEFT] = -20.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::MIN_POS_ANKLE_RIGHT] = -20.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    // Mono-articular Muscle constant
    
    
    sml::Constant[CONSTANT::R_SOL] = 0.05;
    sml::Constant[CONSTANT::R_TA] = 0.04;
    sml::Constant[CONSTANT::R_VAS] = 0.06;
    sml::Constant[CONSTANT::R_GLU] = 0.10;
    sml::Constant[CONSTANT::R_HF] = 0.10;
    sml::Constant[CONSTANT::MAX_PHI_SOL] = 20.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::MAX_PHI_TA] = -10.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];;
    sml::Constant[CONSTANT::MAX_PHI_VAS] = 15.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];;
    sml::Constant[CONSTANT::MAX_PHI_GLU] = -180.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];;
    sml::Constant[CONSTANT::MAX_PHI_HF] = -180.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];;
    sml::Constant[CONSTANT::REF_PHI_SOL] = -10.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];;
    sml::Constant[CONSTANT::REF_PHI_TA] = 20.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];;
    sml::Constant[CONSTANT::REF_PHI_VAS] = 55.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];;
    sml::Constant[CONSTANT::REF_PHI_GLU] = -30.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];;
    sml::Constant[CONSTANT::REF_PHI_HF] = 0.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];;
    sml::Constant[CONSTANT::OPT_L_SOL] = 0.04;
    sml::Constant[CONSTANT::OPT_L_TA] = 0.06;
    sml::Constant[CONSTANT::OPT_L_VAS] = 0.08;
    sml::Constant[CONSTANT::OPT_L_GLU] = 0.11;
    sml::Constant[CONSTANT::OPT_L_HF] = 0.11;
    sml::Constant[CONSTANT::SLACK_L_SOL] = 0.26;
    sml::Constant[CONSTANT::SLACK_L_TA] = 0.24;
    sml::Constant[CONSTANT::SLACK_L_VAS] = 0.23;
    sml::Constant[CONSTANT::SLACK_L_GLU] = 0.13;
    sml::Constant[CONSTANT::SLACK_L_HF] = 0.10;
    sml::Constant[CONSTANT::MAX_V_SOL] = 6.0;
    sml::Constant[CONSTANT::MAX_V_TA] = 12.0;
    sml::Constant[CONSTANT::MAX_V_VAS] = 12.0;
    sml::Constant[CONSTANT::MAX_V_GLU] = 12.0;
    sml::Constant[CONSTANT::MAX_V_HF] = 12.0;
    sml::Constant[CONSTANT::MASS_SOL] = 1.36;
    sml::Constant[CONSTANT::MASS_TA] = 0.29;
    sml::Constant[CONSTANT::MASS_VAS] = 2.91;
    sml::Constant[CONSTANT::MASS_GLU] = 1.4;
    sml::Constant[CONSTANT::MASS_HF] = 1.87;
    sml::Constant[CONSTANT::PENNATION_SOL] = 0.5;
    sml::Constant[CONSTANT::PENNATION_TA] = 0.7;
    sml::Constant[CONSTANT::PENNATION_VAS] = 0.7;
    sml::Constant[CONSTANT::PENNATION_GLU] = 0.5;
    sml::Constant[CONSTANT::PENNATION_HF] = 0.5;
    sml::Constant[CONSTANT::TYPE1FIBER_SOL] = 0.81;
    sml::Constant[CONSTANT::TYPE1FIBER_TA] = 0.7;
    sml::Constant[CONSTANT::TYPE1FIBER_VAS] = 0.5;
    sml::Constant[CONSTANT::TYPE1FIBER_GLU] = 0.5;
    sml::Constant[CONSTANT::TYPE1FIBER_HF] = 0.5;
    sml::Constant[CONSTANT::DIRECTION_SOL] = -1.0;
    sml::Constant[CONSTANT::DIRECTION_TA] = 1.0;
    sml::Constant[CONSTANT::DIRECTION_VAS] = 1.0;
    sml::Constant[CONSTANT::DIRECTION_GLU] = -1.0;
    sml::Constant[CONSTANT::DIRECTION_HF] = 1.0;
    // Mono-articular Muscle constant    
    sml::Constant[CONSTANT::R_GAS_ANKLE] = 0.05;
    sml::Constant[CONSTANT::R_GAS_KNEE] = 0.05;
    sml::Constant[CONSTANT::R_HAM_KNEE] = 0.05;
    sml::Constant[CONSTANT::R_HAM_HIP] = 0.08;
    sml::Constant[CONSTANT::MAX_PHI_GAS_ANKLE] = 20.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::MAX_PHI_GAS_KNEE] = 40.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::MAX_PHI_HAM_KNEE] = 0.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::MAX_PHI_HAM_HIP] =  -180.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
    sml::Constant[CONSTANT::REF_PHI_GAS_ANKLE] = -10.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::REF_PHI_GAS_KNEE] = 15.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::REF_PHI_HAM_KNEE] = 0.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::REF_PHI_HAM_HIP] = -25.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
    sml::Constant[CONSTANT::OPT_L_GAS] = 0.05;
    sml::Constant[CONSTANT::OPT_L_HAM] = 0.10;
    sml::Constant[CONSTANT::SLACK_L_GAS] = 0.40;
    sml::Constant[CONSTANT::SLACK_L_HAM] = 0.31;
    sml::Constant[CONSTANT::MAX_V_GAS] = 12.0;
    sml::Constant[CONSTANT::MAX_V_HAM] = 12.0;
    sml::Constant[CONSTANT::MASS_GAS] = 0.45;
    sml::Constant[CONSTANT::MASS_HAM] = 1.82;
    sml::Constant[CONSTANT::PENNATION_GAS] = 0.7;
    sml::Constant[CONSTANT::PENNATION_HAM] = 0.7;
    sml::Constant[CONSTANT::TYPE1FIBER_GAS] = 0.54;
    sml::Constant[CONSTANT::TYPE1FIBER_HAM] = 0.44;
    sml::Constant[CONSTANT::DIRECTION_GAS] = -1.0;
    sml::Constant[CONSTANT::DIRECTION_HAM] = -1.0;
    
   if(Settings::get<int>("coman")){
    sml::Constant[CONSTANT::LENGTH_TRUNK] = 0.8;
    sml::Constant[CONSTANT::LENGTH_THIGH] = 0.5;
    sml::Constant[CONSTANT::LENGTH_SHIN] = 0.5;
    sml::Constant[CONSTANT::LENGTH_ANKLE] = 0.1;
    sml::Constant[CONSTANT::LENGTH_FOOT] = 0.16;
    sml::Constant[CONSTANT::WEIGHT_TRUNK] = 53.5;
    sml::Constant[CONSTANT::WEIGHT_THIGH] = 8.5;
    sml::Constant[CONSTANT::WEIGHT_SHIN] = 3.5;
    sml::Constant[CONSTANT::WEIGHT_ANKLE] = 0.0;
    sml::Constant[CONSTANT::WEIGHT_FOOT] = 1.25;
    sml::Constant[CONSTANT::MODEL_WEIGHT] = 70;
    sml::Constant[CONSTANT::MODEL_HEIGHT] = 0.9;
    }
}

void Rob::InputInit(){
        for (unsigned joint_pos=INPUT::FIRST_JOINT, joint=JOINT::FIRST_JOINT; joint_pos<=INPUT::LAST_JOINT; joint_pos++, joint++){
            getServo(JOINT::toString(joint))->enablePosition(time_step);
        }
        for (unsigned sensor_force=INPUT::FIRST_FORCE_SENSOR; sensor_force<=INPUT::LAST_FORCE_SENSOR; sensor_force++){
            getTouchSensor(INPUT::toString(sensor_force))->enable(time_step);
        }
        InputUpdate();
}
void Rob::InputUpdate(){
    double sintheta = supervisor::getFromDef("REGIS")->getOrientation()[5];
    sml::Input[INPUT::THETA_TRUNK] = -asin(sintheta) ;
    for (unsigned joint_pos=INPUT::FIRST_JOINT, joint=JOINT::FIRST_JOINT; joint_pos<=INPUT::LAST_JOINT; joint_pos++, joint++){
        //cout << getServo("HIP_LEFT")->getPosition() << endl;
         sml::Input[joint_pos] = getServo(JOINT::toString(joint))->getPosition();
    }
    for (unsigned sensor_force=INPUT::FIRST_FORCE_SENSOR; sensor_force<=INPUT::LAST_FORCE_SENSOR; sensor_force++){
        sml::Input[sensor_force] = getTouchSensor(INPUT::toString(sensor_force))->getValues()[1];
    }
    
    sml::Input[INPUT::NECK_Y]       = supervisor::getFromDef("REGIS")->getPosition()[1];
    sml::Input[INPUT::NECK_X]       = supervisor::getFromDef("REGIS")->getPosition()[2];
#ifdef MODEL3D
    sml::Input[INPUT::ANGLE_HIPCOR_RIGHT] *= -1.0;
    sml::Input[INPUT::THETACOR_TRUNK] = -atan2(supervisor::getFromDef("REGIS")->getOrientation()[1],supervisor::getFromDef("REGIS")->getOrientation()[4]);
#endif
	sml::Input[INPUT::TOE_LEFT_X] = supervisor::getFromDef("SENSOR_TOE_LEFT")->getPosition()[0];
	sml::Input[INPUT::TOE_LEFT_Y] = supervisor::getFromDef("SENSOR_TOE_LEFT")->getPosition()[1];
	sml::Input[INPUT::TOE_LEFT_Z] = supervisor::getFromDef("SENSOR_TOE_LEFT")->getPosition()[2];
	sml::Input[INPUT::TOE_RIGHT_X] = supervisor::getFromDef("SENSOR_TOE_RIGHT")->getPosition()[0];
	sml::Input[INPUT::TOE_RIGHT_Y] = supervisor::getFromDef("SENSOR_TOE_RIGHT")->getPosition()[1];
	sml::Input[INPUT::TOE_RIGHT_Z] = supervisor::getFromDef("SENSOR_TOE_RIGHT")->getPosition()[2];
	sml::Input[INPUT::HEEL_LEFT_X] = supervisor::getFromDef("SENSOR_HEEL_LEFT")->getPosition()[0];
	sml::Input[INPUT::HEEL_LEFT_Y] = supervisor::getFromDef("SENSOR_HEEL_LEFT")->getPosition()[1];
	sml::Input[INPUT::HEEL_LEFT_Z] = supervisor::getFromDef("SENSOR_HEEL_LEFT")->getPosition()[2];
	sml::Input[INPUT::HEEL_RIGHT_X] = supervisor::getFromDef("SENSOR_HEEL_RIGHT")->getPosition()[0];
	sml::Input[INPUT::HEEL_RIGHT_Y] = supervisor::getFromDef("SENSOR_HEEL_RIGHT")->getPosition()[1];
	sml::Input[INPUT::HEEL_RIGHT_Z] = supervisor::getFromDef("SENSOR_HEEL_RIGHT")->getPosition()[2];
	double lefthipx = supervisor::getFromDef("HIP_LEFT")->getPosition()[0];
	double lefthipy = supervisor::getFromDef("HIP_LEFT")->getPosition()[1];
	double lefthipz = supervisor::getFromDef("HIP_LEFT")->getPosition()[2];
	double righthipx = supervisor::getFromDef("HIP_RIGHT")->getPosition()[0];
	double righthipy = supervisor::getFromDef("HIP_RIGHT")->getPosition()[1];
	double righthipz = supervisor::getFromDef("HIP_RIGHT")->getPosition()[2];
	sml::Input[INPUT::MIDHIP_X] = lefthipx/2.0+righthipx/2.0;
	sml::Input[INPUT::MIDHIP_Y] = lefthipy/2.0+righthipy/2.0;
	sml::Input[INPUT::MIDHIP_Z] = lefthipz/2.0+righthipz/2.0;
}
void Rob::initPerturbation(){
    perturbator = PerturbatorManager(5.0);
    if(Settings::get<int>("perturbation") == 1) {
        perturbator.add(JOINT::HIP_LEFT);
        perturbator.add(JOINT::KNEE_LEFT);
        perturbator.add(JOINT::ANKLE_LEFT);
    }
    //perturbator[JOINT::KNEE_LEFT] = new Perturbator(start_time+duration,duration);
    //perturbator[JOINT::ANKLE_LEFT] = new Perturbator(start_time+2*duration,duration);
    
    
    
}
void Rob::initKeyboardControl(){
    keyboardEnable(100);
    if(!Settings::isOptimization()){
        parameterControlVec.push_back("amp_change_reflex");
        parameterControlVec.push_back("amp_change_cpg");
        parameterControlVec.push_back("amp_change_cst");
        parameterControlVec.push_back("offset_change_reflex");
        parameterControlVec.push_back("offset_change_cpg");
        parameterControlVec.push_back("offset_change_cst");
        parameterControlVec.push_back("freq_change");
        //parameterControlVec.push_back("trunk_ref");
        parameterControlVec.push_back("stance_end");
        //parameterControlVec.push_back("swing_end");
        
        parameterControl["amp_change_reflex"]="A sen";
        parameterControl["offset_change_reflex"]="O sen";
        parameterControl["amp_change_cpg"]="A cpg";
        parameterControl["offset_change_cpg"]="O cpg";
        parameterControl["amp_change_cst"]="A bas";
        parameterControl["offset_change_cst"]="O cst";
        parameterControl["freq_change"]="Freq";
        parameterControl["stance_end"]="STend";
        //parameterControl["swing_end"]="SWend";
        //parameterControl["trunk_ref"]="Trunk ref";
    }
    control = 0;
    //parameterControlValue = ;
    counter = 0;
}

void Rob::initWebotsCommunication(){
    // -----------------------------------------------------------
    //get and enable receiver
    receiver = getReceiver("RECEIVER");
    receiver ->enable(time_step);
    emitter  = getEmitter("EMITTER");
    emitter_matlab = getEmitter("EMITTER_MATLAB");
    //  emitter_analyzer = getEmitter("EMITTER_ANALYZER");
}

void Rob::init(){
    initPerturbation();
    initKeyboardControl();
    //Communication initialisation
    initWebotsCommunication();
    supervisor::step(time_step);
}


int Rob::step(){
    /** -------------- listening for packets, updating robots state and sending packets ---- */
    listen_(); debug == true ? cout << "[ok] : listening (Rob.cc)" << endl:true;
    // clock.start();
    // InputUpdate();
    // clock.end();
    // clock.printMean();
    sml::step();

    
    static double torque;
    static int perturbation = Settings::get<int>("perturbation");
    static int fixedViewPoint = Settings::get<int>("fixedViewPoint");
    static int coman = Settings::get<int>("coman");
    
    
    static bool finishedPert;
    static bool startPert=false;
    static bool finishedPertPrinted=false;
    finishedPert = true;
    
    for (
        unsigned joint=JOINT::FIRST_JOINT,
                 joint_tau=OUTPUT::FIRST_JOINT; 
        joint_tau<=OUTPUT::LAST_JOINT; 
        joint_tau++,
        joint++
        ){
         //if(joint_tau == OUTPUT::TORQUE_HIP_RIGHT)
         //    cout << OUTPUT::toString(joint_tau) << "\t" << sml::Input[joint_tau] << endl;
#ifdef MODEL3D
        if (joint_tau == OUTPUT::TORQUE_HIPCOR_LEFT)
             sml::Output[joint_tau] *= -1.0;
#endif
#ifdef WEBOTS730
         torque = -sml::Output[joint_tau];
#else
         torque = sml::Output[joint_tau];
#endif

         if(
            perturbation == 1 && 
            perturbator.find(JOINT::Joints(joint))
            )
         {
             if(perturbator[JOINT::Joints(joint)]->doPerturbation() )
             {
                perturbator[JOINT::Joints(joint)]->updatePerturbation();
                torque += perturbator[JOINT::Joints(joint)]->getPerturbation();
                //if ( perturbator[JOINT::Joints(joint)]->getPerturbation() != 0.0)
                //cout << perturbator[JOINT::Joints(joint)]->getPerturbation() << endl;
                startPert = true;
                finishedPert = false;
             }
         }
         
         
         getServo(JOINT::toString(joint))->setForce(torque);
    }
    
    if (perturbation == 1){
        if(finishedPert && !finishedPertPrinted && startPert){
            finishedPertPrinted = true;
            cout << "perturbation on one joint ended" << endl;
            //eventManager->set(STATES::STAY_IN_LOOP,false);
        }
    }
    
    
    emit_(); debug == true ? cout << "[ok] : emitting (Rob.cc)" << endl:true;
    if(fixedViewPoint != 0){
        if(coman)
        {
            fixedYAxis(0.67);
        }
        else
        {
            fixedYAxis(1.2);
        }
    }
    /*
    cout << getLimbState("left") << "(" <<  left_foot->heel->get(1)+left_foot->toe->get(1) <<  ")" 
         <<  "," 
         << getLimbState("right") << "(" << right_foot->heel->get(1)+right_foot->toe->get(1)<< ")" 
         << endl;
    cout 
        << trunk->theta << ","
        << trunk->theta_ref << ","
        << trunk->theta_offset << ","
        << trunk->theta_to << ","
        << trunk->d_theta << "," << endl;
        */
    return supervisor::step(time_step);
}

void Rob::fixedYAxis(double Y){
    Node * viewpoint;
    Field * viewpoint_pos;
    viewpoint = this->getRoot()->getField("children")->getMFNode(1);
    viewpoint_pos = viewpoint->getField("position");
    
    
    double * newViewPointPos = new double[3];
    double * actualViewPointPos = new double[3];
    
    actualViewPointPos = (double *)viewpoint_pos->getSFVec3f();
    newViewPointPos[0] = actualViewPointPos[0];
    newViewPointPos[1] = Y;
    newViewPointPos[2] = (eventManager->get<double>(STATES::POSITION_X));
    
    //if(viewpoint->getField("follow")->getSFString() == "")
    viewpoint_pos->setSFVec3f(newViewPointPos);
}
void Rob::listen_(){
    while (receiver->getQueueLength()){
        message_PtoW = (PtoW *)receiver->getData();
        if(Settings::get<string>("extract") == "force"){
            
            state["llast_force_y"] = state["last_force_y"];
            state["llast_force_z"] = state["last_force_z"];
            state["llast_force_pos"] = state["last_force_pos"];
            state["last_force_y"] = message_PtoW->last_force_y; 
            state["last_force_z"] = message_PtoW->last_force_z;
            state["last_force_pos"] = message_PtoW->last_force_pos;
            
        }
        receiver->nextPacket();
    }
}
void Rob::emit_(){
    //------------  we send a message to the physic plugin -------------
    message_WtoP.backward = Settings::get<int>("backward");
    if(Settings::get<string>("extract") == "force" || Settings::get<string>("extract") == "rp_eval"){
        message_WtoP.force_amplitude = Settings::get<double>("force");
    }
    emitter->send(&message_WtoP, sizeof(message_WtoP));
}

void Rob::plotTextInWindow(int color){
    int black = 0x000000; // color of the text in the 3d windows
    int red = 0xFF0000; // color of the text in the 3d windows
    //int green = 0x00FF00; // color of the text in the 3d windows
    //int blue = 0x0000FF; // color of the text in the 3d windows
    
    for(auto kv : parameters[1])
        parametersColor[kv.first] = 0x000000;
    
    char label[64];
    // '1' -> 49
    
    // <- 14
    // ^  15
    // -> 16
    // |  17
    int currentKey = keyboardGetKey();
    if(currentKey > 48 && currentKey < 57){
        control = currentKey-48;
    }
    if(currentKey == 315 && control >= 0)
        control--;
    if(currentKey == 317 && control < (int)parameterControlVec.size())
        control++;
    if(currentKey == 70) fixedViewPoint = !fixedViewPoint;
    
    //if(currentKey != 0)
    //cout << "Key pressed : " << currentKey << endl;
    double step = 0.01;
    if(control >= 1 && control <= (int)parameterControlVec.size()){
        parameterControlName = parameterControlVec[control-1];
        parameterControlValue = &parameters[1][parameterControlName];
    }
    else{
        control = 0;
        parameterControlName="";
    }
    if(currentKey==316 && control != -1)
        *parameterControlValue += step;
    if(currentKey==314 && control != -1)
        *parameterControlValue -= step;
    

    if((int)(eventManager->get<double>(STATES::TIME)*1000) % 10 == 0){
    sprintf(label, "v=%.2f [m/s]\na=%.1lf [m/s2]\ncycle=%d cycle", eventManager->get<double>(STATES::IN_VELOCITY_FILTERED), eventManager->get<double>(STATES::IN_ACCELERATION_FILTERED),eventManager->get<int>(STATES::CYCLE_COUNT));
    this->setLabel(1, label, 0.01, 0.01, 0.08, color, 0.0);
    
    if(true)
    {
        sprintf(label, "d=%.1f [m]\nh=%.1f [m]", 
        eventManager->get<double>(STATES::DISTANCE) , eventManager->get<double>(STATES::HEIGHT));
        this->setLabel(2, label, 0.01, 0.15, 0.08, color, 0.0);
    }
    else
    {
        sprintf(label, "%.1f [m], %.1f [m] \n%.0f [J] ", 
        eventManager->get<double>(STATES::DISTANCE) , eventManager->get<double>(STATES::HEIGHT), eventManager->get<double>(STATES::ENERGY));
        this->setLabel(2, label, 0.01, 0.07, 0.08, color, 0.0);
    }
    double start = 0.3;
    
    int i=3;
    for(auto &kv : parameterControlVec){
        sprintf(label, "%s, %.2f", parameterControl[kv].c_str(),parameters[1][kv]);
        if(kv== parameterControlName)
            this->setLabel(i, label, 0.01, start, 0.08, black, 0.0);
        else
            this->setLabel(i, label, 0.01, start, 0.08, color, 0.0);
        start+=0.04;
        i++;
    }
    }
    /*
    if(control != 0){
        sprintf(label, "%s, %.2f", parameterControl[control].c_str(),*parameterControlValue);
        //sprintf(label, "%s, %.1f", parameterControl[3].c_str(),parameters[1]["amp_change"]);
        this->setLabel(3, label, 0.01, 0.47, 0.08, black, 0.0);
    }
    else{
        sprintf(label, "control off");
        this->setLabel(3, label, 0.01, 0.47, 0.08, color, 0.0);
    }*/
}
