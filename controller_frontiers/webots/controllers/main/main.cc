#include <stdio.h>
#include <sml/sml.hh>

#include "Experiment.hh"
#include <string>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>




using namespace std;

CentralClock * centralclock;
double debug=false;
std::map<std::string, double_int_string> Settings::sets;

extern SmlParameters parameters;

void loadControlParametersName(std::map<std::string, std::map<std::string, double> >& param);
void loadGeyerParametersName(std::map<std::string, std::map<std::string, double> >& param);

int main(int argc, char* argv[])
{
    
    /**__**/ cout << "1) Loading settings...............";

	if(argc >= 2)
		Settings::filename = argv[1];
	if(argc >= 3){
		Settings::prefix = "_";
		Settings::prefix += argv[2];
	}
	cout << Settings::prefix << endl;
	Settings settings;
	parameters = SmlParameters();
    cout << Settings::sets["experiment"] << endl;
    /**__**/ cout << "Ok" << endl;
	if(Settings::get<int>("log") == 2){
		ofstream out("../../../log/webots_log.txt", ios::app);
		streambuf *coutbuf = cout.rdbuf(); //save old buf
		cout.rdbuf(out.rdbuf()); //redirect std::cout to out.txt!
	}
	srand(time(0));
	
	
    /**__**/ cout << "3) Create CentralClock...............";
	centralclock = new CentralClock(parameters[1]["freq_change"]);
	/**__**/ cout << "Ok" << endl;
    /**__**/ cout << "4) Create Robot...............";
    Rob *Regis = new Rob();
    
    //eventManager = Regis->eventManager;
    /**__**/ cout << "Ok" << endl;
    /**__**/ cout << "5) Create Experiment...............";
	Experiment exp(Regis);
	/**__**/ cout << "Ok" << endl;
	//if(argc == 3)
	//	exp.change_number = convertToInt(argv[2])-1;
	// Run the experiment
	exp.run();
	
	return 0;
}
