#include "Experiment.hh"
#include <sml/sml-tools/str2double.hh>
//#include <unordered_map>
#include "config.hh"
#include "Phase.hh"

#include <boost/algorithm/string.hpp>

using namespace std;
extern bool debug;

extern EventManager * eventManager;

inline int convertToInt(string const& s);
//constructor
Experiment::Experiment(Rob * body) : body(body) { this->init(); }


// Init
void Experiment::init(){
	changed=false;
	first_saved=false;
	max_wait=5.0;
	spent_energy=0.0;
	waited=0.0;
	changed_at_dist=0.0;
	changed_at_nbsl=0;
	change_number=-1;
	loadingEnded = false;
	color = 0xffffff; // color of the text in the 3d windows
	laststeppos = 0.0;
	last_phase = false;
	eventManager->set<bool>(STATES::IS_LAST_PHASE,false);
	data_time.push_back(0.0);
	
	if(Settings::get<int>("save_for_matlab") == 1)
		this->createRawFiles();
	
	if(Settings::get<string>("experiment") == "initialPhase"){
		phases.push_back(new InitialPhaseFinder(this));
	}
	else{
		phases.push_back(new InitialPhase(this));
        string expString = Settings::get<string>("experiment");
        boost::trim(expString);
        boost::replace_all(expString, "\t", "");
		boost::replace_all(expString, " ", "");
        boost::replace_all(expString, "\n", "|");
		istringstream f(expString);
		string p;    
		while (getline(f, p, '|')) {

			map<string, string> options = {
				{"start_after_step","3"},
				{"p_file",""},
				{"restart_state","restart state"}
			};
			
			vector<string> strs;
			boost::split(strs, p, boost::is_any_of("+"));
			if(strs.size() > 1){
				options["start_after_step"] = strs[1];
				p = strs[0];
				strs.clear();
			}
			
			boost::split(strs, p, boost::is_any_of("{"));			



			// If option
			if(strs.size() > 1)
			{
				vector<string> strs2;
				boost::split(strs2, strs[1], boost::is_any_of("}"));
				vector<string> strs3;
				boost::split(strs3, strs2[0], boost::is_any_of(","));
				
				for(auto &it:strs3){
					vector<string> strs4;
					boost::split(strs4, it, boost::is_any_of("="));
					options[strs4[0]] = strs4[1];
				}
			}
			
			int start_after_step = stoi( options["start_after_step"]);
			string p_file = options["p_file"];
			string restart_state = options["restart_state"] != "restart state" ? "" : "restart state";
			
			
			if(strs[0] == "fullReflex"){
				phases.push_back(new FullReflexPhase(this,p_file,start_after_step,restart_state));
			}
	
			else if(strs[0]=="semiCpg"){
				phases.push_back(new SemiReflexPhase(this,p_file,start_after_step,restart_state));
			}
			else if(strs[0]=="systematicStudy"){
				phases.push_back(new SystematicStudyPhase(this, start_after_step, restart_state));
			}
			else
			{
				cout << "unknown phase : " << p << endl;
			}
		}
	}
	phases[phases.size()-1]->last=true;
	phase = phases.begin();
	
	
	
}

// Run an experiment
void Experiment::run(){
	initiation();
	one_trial("energyoptimisation");
	this->termination();
}

// The trial
void Experiment::one_trial(string mode){
	bool quit = false;
	while(!eventManager->isRunFinished() && !quit) {
		if(phase < phases.end())
		{
			if((*phase)->rule(&phase)){
				//_size ++;
				//cout << "Phase " << _size << " ended" << endl;
			}
		}
		if(phase == phases.end())
            eventManager->set<bool>(STATES::IS_LAST_PHASE,true);
        else if ((*phase)->last)
            eventManager->set<bool>(STATES::IS_LAST_PHASE,true);
		if(phase ==  phases.end() && !last_phase){
			last_phase = true;
#ifdef WEBOTS7
#else
            if(Settings::get<int>("extract_video") == 1)
				body->startMovie(Settings::get<string>("video_name"),500,400,0,100);
#endif
		}
		this->step();
	}
}


void Experiment::step(){
	

	body->step();
	
    addRawData();
    writeRawContent();
    
    if(!Settings::isOptimizing()){
        body->plotTextInWindow(color);
    }
	
	
	debug == true ? cout << "[ok] : one step (Experiment.cc)" << endl:true;
}
