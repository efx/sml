#ifndef __Perturbator_HH__
#define __Perturbator_HH__

#include <sml/types/types.h>

class Perturbator {
private:
    double  start;
    double  duration;
    double  current_perturbation_value;
    double  current_perturbation_step;
    double  perturbation_start_time;
    double  perturbation_duration;
    double  perturbation_amplitude;
    int     perturbation_direction;
    int     perturbation_number;
    int     d_perturbation_number;
    bool    perturbation_on_going;
    
    void    initVariables();
    void    initPerturbation();
    void    updatePerturbationValue();
    double  getPerturbationValue();
    bool    perturbationOnGoing();
    bool    inExpectedPhase();
    double  getEstimatedPercentage();
    void    resetPerturbation();
    
    
    void    checkPerturbationEnd();
    double  getStartingTime();
public:
    Perturbator(double,double);
    bool    doPerturbation();
    double  getPerturbation();
    void    updatePerturbation();
    double  getCurrentPerturbation();
};


class PerturbatorManager {
private:
    int start_time;
    int duration;
    std::map<JOINT::Joints, Perturbator*> perturbator;
public:
    PerturbatorManager();
    PerturbatorManager(double duration);
    std::map<JOINT::Joints, Perturbator*> getMap();
    void add(JOINT::Joints joint);
    Perturbator * operator[](JOINT::Joints joint);
    bool find(JOINT::Joints joint);
    
    std::string getHeader();
    std::string getRow();
    
    
};
#endif