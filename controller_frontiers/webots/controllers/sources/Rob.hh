#ifndef __Rob_HH__
#define __Rob_HH__


#include <string>
#include <math.h>
#include <vector>
#include <fstream>
#include <map>

//#include <boost/algorithm/string.hpp>


#include <webots/Servo.hpp>
#include <webots/Supervisor.hpp>
#include <webots/TouchSensor.hpp>

#include <sml/types/types.h>
#include <sml/musculoSkeletalSystem/Sml.hh>


#include "Perturbator.hh"
#define DEBUG 0


typedef struct{
  int is_in_lift_mode; 
  double last_force_y;
  double last_force_z;
  double last_force_pos;
} PtoW; //from Physic plugin to Webots

typedef struct{
//  int reinitialisation;
  int backward;
  double force_amplitude;
} WtoP; //from Webots to Physic plug in

typedef struct{
  double muscle_signal;
  double muscle_activity;
} WtoM; //from Webots to Raw supervisor

class Rob: public webots::Supervisor, public Sml
{
private:
    bool fixedViewPoint;
    int counter;
    std::string name;

    void ConstantInit();
    void InputInit();
    void InputUpdate();
public:

    std::array<double,8> jointInitialAngle;
    PerturbatorManager perturbator;
    std::vector<std::string> parameterControlVec;
    std::map<std::string, std::string> parameterControl;
    int control;
    double* parameterControlValue;
    std::string parameterControlName;
    std::map<std::string, int> parametersColor;
    typedef Supervisor supervisor;
    typedef Sml sml;
    int step();

    double dt;
    double phi_knee_off;
    double soft_limit_stiffness;
    
    //Receiver
    webots::Receiver* receiver;
    PtoW *message_PtoW;
    webots::Emitter* emitter;
    WtoP message_WtoP;
    //Emitter
    webots::Emitter* emitter_matlab;
    webots::Emitter* emitter_analyzer;
    WtoM message_WtoM;
    
    /**
     * ROBOT POSITION
     * 
     */
    std::map<std::string, const double * > initial_rotation;
    std::map<std::string, const double * > initial_translation;
    void set_position(std::string field_name);
    void save_field(std::string field_name);

    /**
     *  JOINTS
     * 
     */
    std::vector<webots::Servo *> servo;

  //-----------------------------------
    
    //constructor
    //Rob(){};
    //Rob(
    //   std::map<std::string, double> parameters_level0):
    //        Sml(parameters_level0){ init(); };
    Rob(): Sml(){
        sml::init();
        init();
        }
    
    //destructor
    //~Rob();
    void initKeyboardControl();
    void initPerturbation();
    void initWebotsCommunication();
    void init();
    //function
    double compute_tf(double phi, double phi_max, double phi_ref);
    double compute_ir(double phi,double phi_max);
    
    void setParameters(std::vector<double> parameters);
    void listen_();
    void emit_();
    /**
     * STATE UPDATE
     */
    void update_state_touchGround();
    void check_if_stabilized();
    void check_if_end();
    void update_state_energy();
    void update_state_global();
    void update_state();
    void fixedYAxis(double Y);
    
    void plotTextInWindow(int color);
};
/*
namespace KINEMATIC_DATA_FORMAT{
    typedef enum
    {
        COM_SAGITAL_ANGLE   = 0,
        L_HIP_SAGITAL_ANGLE     = 1,
        L_KNEE_SAGITAL_ANGLE    = 2,
        L_ANKLE_SAGITAL_ANGLE   = 3,
        R_HIP_SAGITAL_ANGLE     = 4,
        R_KNEE_SAGITAL_ANGLE    = 5,
        R_ANKLE_SAGITAL_ANGLE   = 6,
        COM_X           = 7,
        COM_Y           = 8,
        L_HIP_SAGITAL_X     = 9,
        L_HIP_SAGITAL_Y     = 10,
        L_KNEE_SAGITAL_X    = 11,
        L_KNEE_SAGITAL_Y    = 12,
        L_ANKLE_SAGITAL_X   = 13,
        L_ANKLE_SAGITAL_Y   = 14,
        R_HIP_SAGITAL_X     = 15,
        R_HIP_SAGITAL_Y     = 16,
        R_KNEE_SAGITAL_X    = 17,
        R_KNEE_SAGITAL_Y    = 18,
        R_ANKLE_SAGITAL_X   = 19,
        R_ANKLE_SAGITAL_Y   = 20,
    } Format;
}
class KinematicData{
public:
    int count;
    std::vector<double> current;
    std::vector<double> total;
    KinematicData()
        {
            count = 0;
            for(int i=0;i<size;i++){
                current.push_back(0.0);
                total.push_back(0.0);
            }
            
        }
    void update(){
        get_current();
        add_current();
        cout++
    }
    void get_current(){
        
    }
    void add_current(){
        for (i=0;i<current.size();i++){
            total[i]+=current[i];
        }
    }
    void get_mean(std::vector<double> out){
        for (i=0;i<total.size();i++){
            if (count != 0){
                out[i]=total[i]/(double)count;
            }
            else
                out[i]=0.0;
        }
    }
};
*/
const double MIN = -100000.0;
const double MAX =  100000.0;
class KeepTrack{
public:
    std::vector<double> current;
    std::vector< std::vector<double> > memory;
    int max_size;
    KeepTrack(){
        max_size = 2;
    }
    void add(std::vector<double> one){
        if(one.size() == memory[0].size()){
            if ((int)memory.size() < max_size)
                memory.push_back(one);
            else{
                memory.erase(memory.begin());
                memory.push_back(one);
            }
            
        }
        if(condition(current,one))
            current.swap(one);
    }
    virtual void initialize(std::vector<double>) = 0;
    virtual bool condition(std::vector<double>, std::vector<double>) = 0;
};
class KeepTrack_max : public KeepTrack {
public:
    KeepTrack_max(): KeepTrack() {}
    bool condition(std::vector<double> _old, std::vector<double> _new){
        if ( _old[0] < _new[0] )
            return true;
        else
            return false;
    }
    void initialize(std::vector<double> _in){
        memory.push_back(_in);
        current = _in;
    }
    
};

class KeepTrack_min : public KeepTrack {
public:
    KeepTrack_min(): KeepTrack() {}
    bool condition(std::vector<double> _old, std::vector<double> _new){
        if ( _old[0] > _new[0] )
            return true;
        else
            return false;
    }
    void initialize(std::vector<double> _in){
        memory.push_back(_in);
        current=_in;
    }
    
};
/*
KeepTrack_max max;
std::vector<double> x0 = {MIN, state.distance, state.duration}
max.initialize(x0);
...
std::vector<double> x0 = {regis.left_hip.angle, state.distance, state.duration}
max.add(x0);
*/
#endif /* __ROB_HH__ */
