#include <iostream>
#include <fstream>
#include <vector>
#include <boost/algorithm/string.hpp>
#include "State.hh"
#include "str2double.hh"
using namespace std;

State::State(const State &state, double position) :
	repeat(0),
	position(position),
	distance(0.0),
	old_meanSpeed(0.0),
	inSpeed(0.0),inAcceleration(0.0),
	energy(0.0),
	energyW(0.0),
	energyM(0.0),
	nb_fall(0),
	nb_cycle(0),
	duration(0.0),
	muscle_noise(0.001),
	stay_in_loop(true),
	is_in_lift_mode(false),
	is_in_launching_phase(false),
	right_cpg_loaded(false),left_cpg_loaded(false),
	last_force_y(0.0),
	last_force_z(0.0),
	last_force_pos(0.0),
	llast_force_y(0.0),
	llast_force_z(0.0),
	llast_force_pos(0.0),
	falled(false),
	doublestance_duration(0.0),
	doublestance_duration_mean(0.0),
	doublestance_duration_number(0)
{

}

std::map<std::string, double> State::getMap(){
	std::map<std::string, double> fitnesses;
	fitnesses["distance"] = distance;
	fitnesses["energy"] = energy;
	fitnesses["fall"] = nb_fall;
	fitnesses["cycle"] = ((double)nb_cycle);
	fitnesses["angular_knee_velocity_change"] = nb_cycle != 0 ? (double)nb_knee_sign_changed/nb_cycle : 0;
	fitnesses["steadyState_reachedAt"] = steady_after_step;
	
	fitnesses["duration"] = duration;
	fitnesses["muscle_activity_noise"] = muscle_noise;
	fitnesses["last_acceleration"] = meanAcceleration;
	fitnesses["last_speed"] = meanSpeed;
	
	
	fitnesses["doublestance_duration"] = total_doublestance_duration;
	
	//fitnesses["left_right_correlation"] = this->getJointsSimilarities();
	
	
	
	// Fitness correction for false good solution
	if(fitnesses["distance"]/fitnesses["duration"] > 5.0){
		fitnesses["distance"] = 0.1;
	}
	return fitnesses;
	
}