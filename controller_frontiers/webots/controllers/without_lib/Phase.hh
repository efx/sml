#include <vector>
#include <sstream>
#include <fstream>
#include "Experiment.hh"
#include "Interneuron.hh"
#include "CentralClock.hh"



extern CentralClock * centralclock;
/*!
 * \file Phase.hh
 * \author efx
 * \version 0.1
 * \brief Experiment phase description
 */
using namespace std;

std::string ReplaceString(std::string subject, const std::string& search,
                          const std::string& replace) {
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos) {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
    return subject;
}
class Phase{
public:
	string name;
	Experiment *exp;
	int nb_cycle;
	bool restart_state;
	Phase(string name, Experiment *exp, int nb_cycle): name(name), exp(exp), nb_cycle(nb_cycle), restart_state(false){};
	Phase(string name, Experiment *exp, int nb_cycle, string option): name(name), exp(exp), nb_cycle(nb_cycle){
		if(option.find("restart state") != string::npos)
			restart_state = true;
		else
			restart_state = false;
		
	};
	void print(){
		cout << "[Start] " << name << " phase" << endl;
	}
	virtual bool rule(vector<Phase *>::iterator * it) = 0;
	
};
class InitialPhase : public Phase{
public:
	InitialPhase(Experiment *exp): Phase("Initial", exp, 0){};
	bool rule(vector<Phase *>::iterator * it){
		state.is_in_launching_phase = true;
		exp->loadReflexParameters(Settings::str["launching_gate"]);
		exp->parameters->sendTo(exp->body->parameters_geyer);
		this->print();
		if(!exp->body->feedback_right_loaded)
			exp->body->loadFeedback("right");
		if(!exp->body->feedback_left_loaded)
			exp->body->loadFeedback("left");
		exp->color = 0x000000;
		(*it)++;
		return true;
	}
};

class InitialPhaseFinder : public Phase{
public:
	InitialPhaseFinder(Experiment *exp): Phase("Initial", exp, 0){};
	bool rule(vector<Phase *>::iterator * it){
		state.is_in_launching_phase = false;
		exp->loadReflexParameters();
		exp->parameters->sendTo(exp->body->parameters_geyer);
		this->print();
		if(!exp->body->feedback_right_loaded)
			exp->body->loadFeedback("right");
		if(!exp->body->feedback_left_loaded)
			exp->body->loadFeedback("left");
		
		(*it)++;
		return true;
	}
};
class FullReflexPhase : public Phase{
public:
	string parameters;
	FullReflexPhase(Experiment *exp, string parameters, int nb_cycle): Phase("FullReflex", exp, nb_cycle), parameters(parameters){};
	FullReflexPhase(Experiment *exp, int nb_cycle): Phase("FullReflex", exp, nb_cycle), parameters("opti"){};
	FullReflexPhase(Experiment *exp, string parameters, int nb_cycle, string option): Phase("FullReflex", exp, nb_cycle, option), parameters(parameters){};
	FullReflexPhase(Experiment *exp, int nb_cycle, string option,string parameters): Phase("FullReflex", exp, nb_cycle,option), parameters(parameters){};
	FullReflexPhase(Experiment *exp, int nb_cycle, string option): Phase("FullReflex", exp, nb_cycle,option), parameters("opti"){};
	bool rule(vector<Phase *>::iterator * it){
		if(exp->body->left_foot->justTouchTheGround() && state.nb_cycle>nb_cycle){
			if(restart_state)
				state = State(state,exp->body->getFromDef("REGIS")->getPosition()[2]-state.meanSpeed*exp->body->dt);
			if(parameters == "opti"){
				state.is_in_launching_phase = false;
				exp->loadReflexParameters();
			}
			else{
				if(parameters == "noOpti"){
					state.is_in_launching_phase = true;
					exp->loadReflexParametersFromFile("saved");
				}
				else{
					state.is_in_launching_phase = false;
					exp->loadReflexParametersFromFile(parameters);
				}
			}
			//exp->loadCpgParameters();
			//cout << "--> sending reflex parameters to robot" << endl;
			exp->parameters->sendTo(exp->body->parameters_geyer);
			this->print();
			(*it)++;
			
			exp->color = 0x000000;
			return true;
		}
		else
			return false;
	}
};
class SemiReflexPhase : public Phase{
public:
	bool end;
	string parameters;
	SemiReflexPhase(Experiment *exp, std::string parameters, int nb_cycle):Phase("SemiReflex",exp, nb_cycle),end(false),parameters(parameters){};
	SemiReflexPhase(Experiment *exp, int nb_cycle):Phase("SemiReflex",exp, nb_cycle),end(false),parameters("opti"){};
	SemiReflexPhase(Experiment *exp, string parameters, int nb_cycle, string option): Phase("SemiReflex", exp, nb_cycle, option), end(false), parameters(parameters){};
	SemiReflexPhase(Experiment *exp, int nb_cycle, std::string option):Phase("SemiReflex",exp, nb_cycle, option),end(false),parameters("opti"){};
	SemiReflexPhase(Experiment *exp, int nb_cycle, std::string parameters, std::string option):Phase("SemiReflex",exp, nb_cycle, option),end(false),parameters(parameters){};
	bool rule(vector<Phase *>::iterator * it){
		if(exp->body->right_foot->justTouchTheGround() && state.nb_cycle>=nb_cycle && !end)
		{
			
			if(parameters == "opti"){
				state.is_in_launching_phase = false;
				exp->loadCpgParameters();
				state.is_in_launching_phase = false;
			}
			else{
				if(parameters == "noOpti"){
					state.is_in_launching_phase = true;
					exp->loadCpgParametersFromFile("saved");
				}
				else{
					state.is_in_launching_phase = false;
					exp->loadCpgParametersFromFile(parameters);
				}
			}
			//exp->parameters->sendTo(exp->body->parameters_geyer);
			exp->body->loadPartialCpg("right");
			exp->body->loadPartialCpg("left");
			//exp->body->loadPartialCpg("right","cpg");
			//exp->body->loadPartialCpg("left","cpg");
			end = true;
		}
		
		if(end)
		{
			exp->color = 0xffff00;
			this->print();
			(*it)++;
			state.is_in_launching_phase = false;
			return true;
		}
		else
			return false;
		
	}
};
class SemiReflexReplacedPhase : public Phase{
public:
	bool end;
	string parameters;
	SemiReflexReplacedPhase(Experiment *exp, std::string parameters, int nb_cycle):Phase("SemiReflex",exp, nb_cycle),end(false),parameters(parameters){};
	SemiReflexReplacedPhase(Experiment *exp, int nb_cycle):Phase("SemiReflex",exp, nb_cycle),end(false),parameters("opti"){};
	SemiReflexReplacedPhase(Experiment *exp, int nb_cycle, std::string option):Phase("SemiReflex",exp, nb_cycle, option),end(false),parameters("opti"){};
	SemiReflexReplacedPhase(Experiment *exp, int nb_cycle, std::string parameters, std::string option):Phase("SemiReflex",exp, nb_cycle, option),end(false),parameters(parameters){};
	bool rule(vector<Phase *>::iterator * it){
		if(exp->body->right_foot->justTouchTheGround() && state.nb_cycle>=nb_cycle && !end)
		{
			if(restart_state)
				state = State(state,exp->body->getFromDef("REGIS")->getPosition()[2]-state.meanSpeed*exp->body->dt);
			if(parameters == "opti")
				exp->loadReflexParameters();
			else
				exp->loadReflexParametersFromFile("saved");
			exp->loadCpgParameters();
			exp->parameters->sendTo(exp->body->parameters_geyer);
			exp->body->loadPartialCpg("right");
			exp->body->loadPartialCpg("left");
			end = true;
		}
		
		if(end)
		{
			exp->color = 0xffff00;
			this->print();
			(*it)++;
			state.is_in_launching_phase = false;
			return true;
		}
		else
			return false;
		
	}
};


class CpgReflexPhase : public Phase{
public:
	string parameters;
	bool end;
	CpgReflexPhase(Experiment *exp, int nb_cycle):Phase("CpgReflex",exp, nb_cycle),end(false){};
	CpgReflexPhase(Experiment *exp, int nb_cycle, std::string option):Phase("CpgReflex",exp, nb_cycle,option),end(false){};
	bool rule(vector<Phase *>::iterator * it){
		if(exp->body->right_foot->justTouchTheGround() && state.nb_cycle>=nb_cycle && !end){
			if(restart_state)
				state = State(state,exp->body->getFromDef("REGIS")->getPosition()[2]-state.meanSpeed*exp->body->dt);
			exp->loadReflexParameters();
			exp->loadCpgParameters();
			exp->parameters->sendTo(exp->body->parameters_geyer);
			exp->body->loadCpg("right","cpg");
			exp->body->loadCpg("left","cpg");
			end = true;
		}
		
		if(end)
		{
			end = true;
			exp->color = 0xffff00;
			this->print();
			(*it)++;
			return true;
		}
		else
			return false;
	}
};


class SystematicFeedbacksStudyPhase : public Phase{
public:
	bool end;
	string parameters;
	int phase_state_lC_step;
	int phase_state_lC_stepsize_step;
	int phase_state_lA;
	int phase_state_lB;
	int phase_state_lC;
	std::string log_filename;
	std::string connection;
	bool loaded;
	string folder;
	
	
	struct cpgState{
		double amplitude;
		double offset;
		double frequency;
		cpgState(): amplitude(0.0), offset(0.0), frequency(0.0) {}
	};
	struct minStepSize{
		double amplitude;
		double offset;
		double frequency;
		minStepSize(): amplitude(0.1), offset(0.01), frequency(0.1) {}
	};
	
	bool first_increment;
	bool get_state_from_file;
	cpgState current_interneuron_state;
	minStepSize min_step_size;
	std::map<std::string, Entity *>::iterator current_feedback_left;
	std::map<std::string, Entity *>::iterator last_feedback_left;
	std::map<std::string, Entity *>::iterator current_feedback_right;
	
	Connection * current_connection_left;
	Connection * current_connection_right;
	
	
	
	SystematicFeedbacksStudyPhase(Experiment *exp, std::string parameters, int nb_cycle)
		:Phase("SystematicFeedbacksStudy",exp, nb_cycle),end(false),parameters(parameters){init();};
	SystematicFeedbacksStudyPhase(Experiment *exp, int nb_cycle)
		:Phase("SystematicFeedbacksStudy",exp, nb_cycle),end(false),parameters("opti"){init();};
	SystematicFeedbacksStudyPhase(Experiment *exp, int nb_cycle, std::string option)
		:Phase("SystematicFeedbacksStudy",exp, nb_cycle, option),end(false),parameters("opti"){init();};
	SystematicFeedbacksStudyPhase(Experiment *exp, int nb_cycle, std::string parameters, std::string option)
		:Phase("SystematicFeedbacksStudy",exp, nb_cycle, option),end(false),parameters(parameters){init();};
		
	void init(){
		folder = "../../../systematicsearch_data/";
		connection = "feedback";
		loaded = false;
		log_filename = ".serialize_test";
		phase_state_lA = 1;
		phase_state_lB = 0;
		phase_state_lC_step = 0;
		phase_state_lC_stepsize_step = 0;
		phase_state_lC = 0;
		first_increment = true;
		ifstream check(log_filename);
		if (check){
			get_state_from_file = true;
		}
		else
			get_state_from_file = false;
		
	}
	
	 // 	This method should save the current state of the robot
	 //		--> local mean 	frequency
	 // 		--> local mean 	steplength snr
	 // 		--> local mean 	angle of attack
	 // 		--> local mean 	speed
	 // 		--> mean 	step length
	 // 		--> local mean 	step length
	 // 		--> mean 	frequency
	 //		--> min knee angle
	 //		--> maximum foot clearance (maximum foot’s height during the swing phase)
	 //		--> stance/swing feedback replaced
	 //		
	 // 		falled/not_falled	stability criterion
		
	void save_phase_state(){ 
		//TODO
		// save robot_state 
		// save phase_state
		cout << "state saved" << endl;
		ofstream myfile;
		if(oneFeedbackStudy())
			myfile.open (folder+"data_"+ReplaceString(current_feedback_left->first, "left_", "")+".txt", ios::out | ios::app); 
		else
			myfile.open (folder+"data.txt", ios::out | ios::app); 
			
		/*
		 * format
			0:  step A
			1:  step B
			2:  step C
			3:  substep step increment
			4:  substep step increment size
			5:  feedback name
			6:  connection type
			7:  falled 
			8:  falled reason
			9:  stabilized_after
			10: mean_speed steplength
			11: Off
			12: Amp
			13: Freq
		*/
		myfile << 
			phase_state_lA << "\t" <<
			phase_state_lB << "\t" <<
			phase_state_lC << "\t" <<
			phase_state_lC_step << "\t" <<
			phase_state_lC_stepsize_step << "\t" <<
			current_feedback_left->first << "\t" << 
			connection << "\t" <<
			!state.stay_in_loop << "\t" <<
			state.stop_reason << "\t" << 
			state.steady_after_step << "\t" <<
			(state.steplength/state.stepduration) << "\t" <<
			(state.steplength) << "\t" <<
			current_connection_left->offset << "\t" <<
			current_connection_left->amplitude << "\t" <<
			get_parameter_value("freq_change") << "\t";
			
			myfile << endl;
		
	}
	static void serialize_state(SystematicFeedbacksStudyPhase * phase ){
		//TODO
		
		// create and open a character archive for output
		std::ofstream ofs(phase->folder+phase->log_filename);

		// create class instance

		// save data to archive
		ofs << phase->end << endl;
		ofs << phase->parameters << endl;
		ofs << phase->phase_state_lA << endl;
		ofs << phase->phase_state_lB << endl;
		ofs << phase->phase_state_lC << endl;
		ofs << phase->phase_state_lC_step << endl;
		ofs << phase->phase_state_lC_stepsize_step << endl;
		ofs << phase->current_feedback_left->first << endl;
		ofs << phase->current_connection_left->offset << endl;
		ofs << phase->current_connection_left->amplitude << endl;
		ofs << phase->get_parameter_value("freq_change") << endl;
	}
	static string deserialize_state_get(std::ifstream &ifs){
		string line;
		std::getline(ifs, line);
		return line;
	}
	static void deserialize_state(SystematicFeedbacksStudyPhase * phase){
		// create and open an archive for input
		std::ifstream ifs(phase->log_filename);
		
		
		{
		std::istringstream iss(deserialize_state_get(ifs));
		iss >> phase->end;
		}
		{
		std::istringstream iss(deserialize_state_get(ifs));
		iss >> phase->parameters;
		}
		{
		std::istringstream iss(deserialize_state_get(ifs));
		iss >> phase->phase_state_lA;
		}
		{
		std::istringstream iss(deserialize_state_get(ifs));
		iss >> phase->phase_state_lB;
		}
		{
		std::istringstream iss(deserialize_state_get(ifs));
		iss >> phase->phase_state_lC;
		}
		{
		std::istringstream iss(deserialize_state_get(ifs));
		iss >> phase->phase_state_lC_step;
		}
		{
		std::istringstream iss(deserialize_state_get(ifs));
		iss >> phase->phase_state_lC_stepsize_step;
		}
		
		string line;
		{
		std::istringstream iss(deserialize_state_get(ifs));
		iss >> line;
		phase->get_feedback_by_name(line);
		}
		
		{
		std::istringstream iss(deserialize_state_get(ifs));
		iss >> phase->current_interneuron_state.offset;
		}
		{
		std::istringstream iss(deserialize_state_get(ifs));
		iss >> phase->current_interneuron_state.amplitude;
		}
		{
		std::istringstream iss(deserialize_state_get(ifs));
		iss >> phase->current_interneuron_state.frequency;
		
		}
	}
	void save_interneuron_state(){
		/*
		// Relic from the past.
		// If needed should be adapted to get amplitude, offset and frequency from the parameters_controlLayer Parameters
		current_interneuron_state.amplitude = ((CPGInterneuron *)exp->body->cpgs_left[current_feedback_left->first])->amplitude;
		current_interneuron_state.offset = ((CPGInterneuron *)exp->body->cpgs_left[current_feedback_left->first])->offset;
		current_interneuron_state.frequency = centralclock->getFrequency();
		*/
	}
	void load_cpg(string type){
		cerr << "loading interneuron\n\t type: cpg \t" << type << endl;
		if ( type == "osc" ){
			connection = "osc_cpg";
			//default is osc --> empty code
		}
		if ( type == "cst" ){
			connection = "cst_cpg";
			//exp->body->cpgs_left[current_feedback->first]->offset = 0.0;
			((CPGInterneuron *)exp->body->cpgs_left[current_feedback_left->first])->amplitude = 0.0;
			//exp->body->cpgs_right[current_feedback->first]->offset = 0.0;
			((CPGInterneuron *)exp->body->cpgs_right[current_feedback_right->first])->amplitude = 0.0;
		}
		if ( type == "null" ){
			connection = "null_cpg";
			((CPGInterneuron *)exp->body->cpgs_left[current_feedback_left->first])->offset = 0.0;
			((CPGInterneuron *)exp->body->cpgs_left[current_feedback_left->first])->amplitude = 0.0;
			((CPGInterneuron *)exp->body->cpgs_right[current_feedback_right->first])->offset = 0.0;
			((CPGInterneuron *)exp->body->cpgs_right[current_feedback_right->first])->amplitude = 0.0;
		}
		current_connection_left->input = exp->body->cpgs_left[current_feedback_left->first];
		current_connection_right->input = exp->body->cpgs_right[current_feedback_right->first];

	}
	// place feedback as input to connections
	void load_feedback(){
		cerr << "loading interneuron\n\t type: feedback\t" << endl;
		connection = "feedback";
		current_connection_left->input = current_feedback_left->second;
		current_connection_right->input = current_feedback_right->second;
	}
	bool get_next_feedback(){
		current_feedback_left++;
		if(current_feedback_left == last_feedback_left){
			cerr << "last feedback" << endl;
			return false;
		}
		else{
			current_feedback_right = exp->body->feedbacks_right.find(ReplaceString(current_feedback_left->first, "left", "right"));
			current_connection_left = exp->body->connections[current_feedback_left->first];
			current_connection_right = exp->body->connections[current_feedback_right->first];
			cerr << current_feedback_left->first << " and right feedback loaded" << endl;
			return true;
		}
	}
	void get_first_feedback(){
		current_feedback_left = exp->body->feedbacks_left.begin();
		current_feedback_right = exp->body->feedbacks_right.find(ReplaceString(current_feedback_left->first, "left", "right"));
		current_connection_left = exp->body->connections[current_feedback_left->first];
		current_connection_right = exp->body->connections[current_feedback_right->first];
		cout << current_feedback_left->first << " and right feedback loaded" << endl;
	}
	bool get_feedback_by_name(std::string name){
		if (exp->body->feedbacks_left.end() == exp->body->feedbacks_left.find(name)){
			return false;
		}
		else{
			current_feedback_left = exp->body->feedbacks_left.find(name);
			current_feedback_right = exp->body->feedbacks_right.find(ReplaceString(current_feedback_left->first, "left", "right"));
			current_connection_left = exp->body->connections[current_feedback_left->first];
			current_connection_right = exp->body->connections[current_feedback_right->first];
			cout << current_feedback_left->first << " and right feedback loaded" << endl;
			return true;
		}
	}
	
	int increment_step_number(){
		return boost::any_cast<int>(Settings::set["systematic_search_step_number"]);
	}
	int increment_stepsize_step_number(){
		return boost::any_cast<int>(Settings::set["systematic_search_stepsize_step_number"]);
	}
	

	void interneuron_increment(string parameterControlName, double increment){
		exp->body->parameters_controlLayer->set["parameters"][parameterControlName] += increment;
		
	}
	// Get the parameter value from the controlLayer, only for global parameters (not directly associated to one connection
	double get_parameter_value(string parameterControlName){
		return exp->body->parameters_controlLayer->set["parameters"][parameterControlName];
	}

	void interneuron_amplitude_increment(){
		cerr << "\tInterneuron amplitude increment" << endl;
		double increment = current_stepsize()*min_step_size.amplitude;
		current_connection_left->amplitude += increment;
		current_connection_right->amplitude += increment;
		/*
		interneuron_increment("amp_change",increment);
		interneuron_increment("amp_change_bas",increment);
		interneuron_increment("amp_change_reflex",increment);
		*/
	}
	void interneuron_offset_increment(){
		cerr << "\tInterneuron offset increment" << endl;
		double increment = current_stepsize()*min_step_size.offset;
		current_connection_left->offset += increment;
		current_connection_right->offset += increment;
		/*
		interneuron_increment("offset_change",increment);
		interneuron_increment("offset_change_bas",increment);
		interneuron_increment("offset_change_reflex",increment);
		*/

	}
	void interneuron_frequency_increment(){
		cerr << "\tInterneuron frequency increment" << endl;
		double increment = current_stepsize()*min_step_size.frequency;
		interneuron_increment("freq_change",increment);
	}
	
	void reset_connection_state(){
		current_connection_left->reset();
		current_connection_right->reset();
		/*
		 * If you read that again and don't know why it was for just remove it
		 * short story : you were updating the general control parameters that unfortunately apply on several things at the same time like for exemple
		 * 		-> basal muscle activities
		 * 		-> all reflexes
		 * 		-> ...
		 *  HERE YOU ARE INTERESTED IN ONE CONNECTION AT A TIME !
		 * Except for the frequency. Because we hypothesis a global clock frequency so we change it for all cpgs. But since we have only one cpg at a time
		 * --> not a problem !
		interneuron_reset("amp_change");
		interneuron_reset("amp_change_bas");
		interneuron_reset("amp_change_reflex");
		interneuron_reset("offset_change");
		interneuron_reset("offset_change_bas");
		interneuron_reset("offset_change_reflex");
		*/
		interneuron_reset("freq_change");
	}
	void interneuron_reset(string parameterControlName){
		exp->body->parameters_controlLayer->set["parameters"][parameterControlName]
		=  exp->body->parameters_controlLayer->set["default_parameters"][parameterControlName];
	}
	
	void interneuron_amplitude_initialize(double value){
		cerr << "\tInterneuron amplitude initialize.. from file" << endl;
		current_connection_left->amplitude  = value;
		current_connection_right->amplitude = value;
		/*
		interneuron_initialize("amp_change",value);
		interneuron_initialize("amp_change_bas",value);
		interneuron_initialize("amp_change_reflex",value);
		*/
	}
	void interneuron_amplitude_initialize(){
		cerr << "\tInterneuron amplitude initialize" << endl;
		double value = min_step_size.amplitude;
		current_connection_left->amplitude = value;
		current_connection_right->amplitude = value;
	}
	void interneuron_offset_initialize(double value){
		cerr << "\tInterneuron offset initialize.. from file" << endl;
		current_connection_left->offset = value;
		current_connection_right->offset = value;
	}
	void interneuron_offset_initialize(){
		cerr << "\tInterneuron offset initialize" << endl;
		double value = min_step_size.offset;
		current_connection_left->offset = value;
		current_connection_right->offset = value;
	}
	void interneuron_frequency_initialize(double value){
		cerr << "\tInterneuron frequency initialize.. from file" << endl;
		interneuron_initialize("freq_change",value);
	}
	void interneuron_frequency_initialize(){	
		cerr << "\tInterneuron frequency initialize" << endl;
		double value = min_step_size.frequency;
		interneuron_initialize("freq_change",value,1);
	}
	// Initialize the parameterControl to the value passed in parameters
	void interneuron_initialize(string parameterControlName, double value){
		exp->body->parameters_controlLayer->set["parameters"][parameterControlName]
			= value;
	}
	// Initialize the parameterControl to 
	void interneuron_initialize(string parameterControlName, double min, int flag){
		exp->body->parameters_controlLayer->set["parameters"][parameterControlName]
			-= 0.5*increment_step_number()*current_stepsize()*min;
	}
	
	double current_step(){
		return (double)phase_state_lC_step+1;
	}
	double current_stepsize(){
		return  (1+0.5*(double)phase_state_lC_stepsize_step);
	}
	void print_current_interneuron_state(){
		cerr << "(a,o,f) = (" <<
			current_connection_left->amplitude
			<< "," << 
			current_connection_left->offset
			<< "," <<
			exp->body->parameters_controlLayer->set["parameters"]["freq_change"] 
			<< ")" << endl;
	}
	bool rule(vector<Phase *>::iterator * it){
		
		if(exp->body->right_foot->justTouchTheGround() && state.nb_cycle>=nb_cycle && !end)
		{
			nb_cycle = -1;
			// If phase initialization ended
			// 	and first step of the systematic feedback study
			// 	--> call first_step();
			if(!loaded){
				loaded = true;
				first_step();
			}
			// If reached steady state
			// --> reset state
			// --> wait_for_steady_state phase ended
			if(state.steady_state){
				int steady_after_step = state.steady_after_step;
				reset_state();
				state.steady_state = false;
				state.wait_for_steady_state = false;
				state.steady_after_step = steady_after_step;
			}
			// If reached steady state wait five steps
			// After having wait 5 steps we
			// --> save and restart the state
			if(!state.wait_for_steady_state && state.nb_cycle > 5){
				save_state();
				reset_state();
				state.steady_state = false;
				state.wait_for_steady_state = true;
				update();
			}
		}
		
		if(end)
		{
			exp->color = 0xffff00;
			(*it)++;
			state.is_in_launching_phase = false;
			return true;
		}
		else
			return false;
		
	}
	void first_step(){
		// 	--> restart state
		//	--> check if old systematic feedback study present
		// 		if found initialize at the old state
		//		else start at the begining
		if(restart_state){
			state = State(state,exp->body->getFromDef("REGIS")->getPosition()[2]-state.meanSpeed*exp->body->dt);
		}
		this->print();
		ifstream check(log_filename);
		if (check){
			get_state_from_file = true;
			first_increment = false;
			cout << "Systematic Feedback study..\n\told phase state found\n\tloading..." << endl;
			deserialize_state(this);
			interneuron_amplitude_initialize(current_interneuron_state.amplitude);
			interneuron_frequency_initialize(current_interneuron_state.frequency);
			interneuron_offset_initialize(current_interneuron_state.offset);
			update();
		}
		else{
			cout << "Systematic Feedback study..\n\tno old phase state found\n\tstarting new phase..." << endl;
			
			if ( !oneFeedbackStudy() )
				get_first_feedback();
			
			
			last_feedback_left = exp->body->feedbacks_left.end();
		}
		
		while(update()){
			save_state();
			reset_state();
			state.steady_state = false;
			state.wait_for_steady_state = true;
		}
		
	}
	void reset_state(){
		state = State(state,exp->body->getFromDef("REGIS")->getPosition()[2]-state.meanSpeed*exp->body->dt);

	}
	void save_state(){
		state.steady_after_step = state.nb_cycle;
		state.wait_for_steady_state = true;
		save_phase_state();
	}
	void interneuron_test_ended(){
		reset_connection_state();
		load_feedback();
		if(oneFeedbackStudy())
			finalize();
		else if(!get_next_feedback())
			finalize();

	}
	void finalize(){
		end = true;
		phase_state_lA = 100;
	}
	// True if feedback name passed in parameter
	// --> only one feedback tested
	bool oneFeedbackStudy(){
		return get_feedback_by_name("left_"+Settings::str["feedback2study"]);
	}
	bool update(){
		if ( current_feedback_left == last_feedback_left ){
			phase_state_lA++;
			get_first_feedback();
		}
		else{
			update_state();
		}
		return !end;
	}
	void update_state(){
		switch ( phase_state_lA ){
			case 0: 
				replacement_study();
				break;
			case 1:
				replacement_and_increment_study();
				break;
			default:
				finalize();
				break;
		}
	}
	void replacement_study(){
		switch ( phase_state_lB ){
			case 0:
				load_cpg("null");
				break;
			case 1:
				load_cpg("cst");// cst model
				break;
			case 2: 
				load_cpg("osc");// osc model
				break;
			
			default:
				phase_state_lB=-1;
				interneuron_test_ended();
				
		}
		phase_state_lB++;
	}
	void replacement_and_increment_study(){
		switch ( phase_state_lB ){
			case 0:
				// cst model
				load_cpg("cst");// cst model
				switch ( phase_state_lC ){
					case 0: // Amplitude incrementation
						if(first_increment)
							interneuron_amplitude_initialize();
						else
							interneuron_amplitude_increment();
						break;
					case 1: // Offset incrementation
						if(first_increment)
							interneuron_offset_initialize();
						else
							interneuron_offset_increment();
						break;
					default:
						phase_state_lC=0;
						phase_state_lB++;
						phase_state_lC_step = -1;
				}
				
				break;
			case 1:
				// osc model
				load_cpg("osc");// osc model
				switch ( phase_state_lC ){
					case 0: // Amplitude incrementation
						if(first_increment)
							interneuron_amplitude_initialize();
						else
							interneuron_amplitude_increment();
						break;
					case 1: // Offset incrementation
						if(first_increment)
							interneuron_offset_initialize();
						else
							interneuron_offset_increment();
						break;
					case 2: // Offset incrementation
						if(first_increment)
							interneuron_frequency_initialize();
						else
							interneuron_frequency_increment();
						break;
					default:
						phase_state_lC=0;
						phase_state_lB++;
						phase_state_lC_step = -1;
				}
				
				break;
			case 2:
				load_feedback();
				switch ( phase_state_lC ){
					case 0: // Amplitude incrementation
						if(first_increment)
							interneuron_amplitude_initialize();
						else
							interneuron_amplitude_increment();
						break;
					case 1: // Offset incrementation
						if(first_increment)
							interneuron_offset_initialize();
						else
							interneuron_offset_increment();
						break;
					default:
						phase_state_lC=0;
						phase_state_lB++;
						phase_state_lC_step = -1;
				}
				
				break;
			default:
				phase_state_lB=0;
				interneuron_test_ended();
		}
		
		// first level (from inside) incrementation control
		// --> we increment until we reach the increment_step_number() threshold
		// --> we then 
		//	reset the first level incrementation control
		//	increment the second level incrementation control (the incrementation step size control)
		//	set the first_increment flag to true (so that the next variable to increment
		// 						gets initialized properly ( mean - 0.5*amplitude )
		if (phase_state_lC_step > increment_step_number()){
			phase_state_lC_step = 0;
			phase_state_lC_stepsize_step++;
			first_increment = true;
			reset_connection_state();
		}
		else 
			phase_state_lC_step++;
		
		// second level (from inside) incrementation control
		// --> we increment until we reach the increment_stepsize_step_number() threshold
		// --> we then 
		//	reset the second level incrementation control
		//	increment the phase_C state
		//	reset the interneuron state to default
		// 	
		if (phase_state_lC_stepsize_step > increment_stepsize_step_number()){
			phase_state_lC++;
			reset_connection_state();
			phase_state_lC_stepsize_step = 0;
		}
	}
};
