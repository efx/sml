#include "Rob.hh"
#include "config.hh"
#include "Interneuron.hh"
#include "Motoneuron.hh"
#include <webots/Node.hpp>
#include <boost/xpressive/xpressive.hpp>
extern State state;
extern double debug;

using namespace std;
using namespace webots;
using namespace boost::xpressive;

typedef std::map<std::string, std::map<std::string, Muscle>>::iterator it_muscle;

//constructor
Rob::Rob(Parameters * parameters_INSenLayer, Parameters * parameters_controlLayer) : time_step(TIME_STEP),parameters_INSenLayer(parameters_INSenLayer), parameters_controlLayer(parameters_controlLayer){
	/**
	 * 
	 * miscellaneous
	 * 
	 */
	keyboardEnable(100);
	
	parameterControlVec.push_back("amp_change_reflex");
	parameterControlVec.push_back("offset_change_reflex");
	parameterControlVec.push_back("amp_change");
	parameterControlVec.push_back("offset_change");
	parameterControlVec.push_back("amp_change_bas");
	parameterControlVec.push_back("offset_change_bas");
	parameterControlVec.push_back("freq_change");
	//parameterControlVec.push_back("trunk_ref");
	parameterControlVec.push_back("stance_end");
	//parameterControlVec.push_back("swing_end");
	
	parameterControl["amp_change_reflex"]="A sen";
	parameterControl["offset_change_reflex"]="O sen";
	parameterControl["amp_change"]="A cpg";
	parameterControl["offset_change"]="O cpg";
	parameterControl["amp_change_bas"]="A bas";
	parameterControl["offset_change_bas"]="O bas";
	parameterControl["freq_change"]="Freq";
	parameterControl["stance_end"]="STend";
	//parameterControl["swing_end"]="SWend";
	//parameterControl["trunk_ref"]="Trunk ref";
	
	control = 0;
	//parameterControlValue = ;
	counter = 0;
	//Constant initialisation
	initialise_constant();
	// Robot initial position
	initialise_position();
	//Parameters initialisation
	initialise_parameters(parameters_INSenLayer);
	//Communication initialisation
	initialise_communication();
	/**
	 * 
	 * intialise model features
	 * 
	 */
	
	/** 
	 *  1/Joints initialisation 
	 */
	initialise_joints();
	debug == true ?	cout << "[ok] : joints initialisation (Rob.cc)" << endl:true;
	/** 
	 *  2/Muscles initialisation, act on joints
	 */
	initialise_muscles();
	debug == true ?	cout << "[ok] : muscles initialisation (Rob.cc)" << endl:true;
	/** 
	 *  3a/Sensors initialisation (ground, foot, muscles)
	 */
	initialise_sensors();
	debug == true ?	cout << "[ok] : sensors initialisation (Rob.cc)" << endl:true;
	/** 
	 *  3b/Motoneurons initialisation
	 */
	initialise_motoneurons();
	debug == true ?	cout << "[ok] : motoneurons initialisation (Rob.cc)" << endl:true;
	/** 
	 *  4/Feedbacks initialisation
	 */
	initialise_feedbacks(parameters_controlLayer);
	debug == true ?	cout << "[ok] : feedbacks initialisation (Rob.cc)" << endl:true;
	/** 
	 *  4/CPGs initialisation
	 */
	initialise_cpgs(parameters_controlLayer);
	debug == true ?	cout << "[ok] : cpgs initialisation (Rob.cc)" << endl:true;
	//show_everything();
	super::step(time_step);
	
}
//--------------------------------------------------------------
int Rob::step(){

	
	compute_angles(dt); debug == true ? cout << "[ok] : compute angle (Rob.cc)" << endl:true;
	
	compute_muscles_state(dt); debug == true ? cout << "[ok] : compute angle (Rob.cc)" << endl:true;
	apply_torque_to_joints(); debug == true ? cout << "[ok] : compute torque (Rob.cc)" << endl:true;
	listen_sensors_state(); debug == true ? cout << "[ok] : compute sensors (Rob.cc)" << endl:true;
	update_sensory_neuron_state(); debug == true ? cout << "[ok] : compute sensory interneuron state (Rob.cc)" << endl:true;
	update_cpgs_neuron_state(); debug == true ? cout << "[ok] : compute cpg interneuron state (Rob.cc)" << endl:true;
	update_interneuron_network(); debug == true ? cout << "[ok] : compute interneuron state (Rob.cc)" << endl:true;
	//compute_motoneurones_stimulation(); debug == true ? cout << "[ok] : compute motoneurones stimulation (Rob.cc)" << endl:true;
	compute_muscles_stimulation(dt); debug == true ? cout << "[ok] : compute muscles stimulation (Rob.cc)" << endl:true;
	/** -------------- listening for packets, updating robots state and sending packets ---- */
	listen_(); debug == true ? cout << "[ok] : listening (Rob.cc)" << endl:true;
	update_state(); debug == true ? cout << "[ok] : update state (Rob.cc)" << endl:true;
	check_if_end();
	emit_(); debug == true ? cout << "[ok] : emitting (Rob.cc)" << endl:true;
	
	if(fixedViewPoint)
		fixedYAxis(1.2);
	
	
	return super::step(time_step);
}

//compute l_MTC
void Rob::compute_muscles_state(double dt){
	for(auto&  kv : muscles){
		kv.second->step(dt);

		state.energyW +=  kv.second->d_energyW;
		state.energyM +=  kv.second->d_energyM;
		state.energy   +=  kv.second->d_energy; 
		
		state.denergy = kv.second->d_energy;
		
		
		kv.second->ComputeFMTC();
		kv.second->ApplyForce();
	}
	//this->compute_muscles_contribution_to_joints();
}
//compute all the joint angles
void Rob::compute_angles(double dt){
	for(auto&  kv : joints){
		kv.second->ComputeAngle(dt);
	}
	trunk->ComputeAngle(dt);
}


void Rob::apply_torque_to_joints(){
	for(auto&  kv : joints){
		kv.second->ComputeTorqueSoftLimit();
		kv.second->ComputeTorque();
	}
}

void Rob::listen_sensors_state(){
	right_foot->updateState(getTime());
	left_foot->updateState(getTime());
	if(right_foot->getState() == "stance" && right_foot->getLastState() == "swing" && left_foot->getState() == "stance")
		finishing_stance = "left";
	if(left_foot->getState() == "stance" && left_foot->getLastState() == "swing" && right_foot->getState() == "stance"){
		finishing_stance = "right";
	}
	
	if(right_foot->getState() == "swing" && right_foot->getLastState() == "stance")
		trunk->theta_to = trunk->theta;
	
	if(left_foot->getState() == "swing" && left_foot->getLastState() == "stance")
		trunk->theta_to = trunk->theta;
}
//void 
void Rob::update_cpgs_neuron_state(){
	for(auto& kv: cpgs){
		string name = kv.first;
		// side ?
		debug == true ? cout << "try loading loading cpg " << name << "  (Rob.cc).." <<endl:true;
		bool left = name.find("left")!=string::npos ? true : false;
		// name 
		left ? name.erase (name.begin(), name.begin()+5) : name.erase (name.begin(), name.begin()+6);
		
		if((left && state.left_up_efx_parameters) || (!left && state.right_up_efx_parameters)){
			double o=0.0;
			double a=1.0;
			if( ( (CPGInterneuron *) kv.second)->amplitude == 0.0){
				o = parameters_controlLayer->set["parameters"]["offset_change_bas"];
				a = parameters_controlLayer->set["parameters"]["amp_change_bas"];
			}
			else{
				o = parameters_controlLayer->set["parameters"]["offset_change"];
				a = parameters_controlLayer->set["parameters"]["amp_change"];
			}
				
			(kv.second)->apply(o,a);
		}
		else
			(kv.second)->apply();
		debug == true ? cout << " ----> [ok] "<<endl :true;
	}
}

void Rob::update_sensory_neuron_state(){
	setHDistToCM();
	setVDistToCM();
	for(auto& kv: feedbacks){
		string name = kv.first;
		// side ?
		bool left = name.find("left")!=string::npos ? true : false;
		// name 
		left ? name.erase (name.begin(), name.begin()+5) : name.erase (name.begin(), name.begin()+6);
		// exists as cpg ?
		bool cpg = find(modelized_sensory_interneurons.begin(),modelized_sensory_interneurons.end(),name) != modelized_sensory_interneurons.end();
		
		if((left && state.left_up_efx_parameters) || (!left && state.right_up_efx_parameters)){
			double o=0.0;
			double a=1.0;
			if(!cpg){
				
				//if(((CPGInterneuron *)cpgs["left_"+name])->amplitude == 0.0){
				if(false){ //WARNING
					o = parameters_controlLayer->set["parameters"]["offset_change_bas"];
					a = parameters_controlLayer->set["parameters"]["amp_change_bas"];
				}
				else{
					o = parameters_controlLayer->set["parameters"]["offset_change_reflex"];
					a = parameters_controlLayer->set["parameters"]["amp_change_reflex"];
				}
			}
			else{
				if(((CPGInterneuron *)cpgs["left_"+name])->amplitude == 0.0){
					o = parameters_controlLayer->set["parameters"]["offset_change_bas"];
					a = parameters_controlLayer->set["parameters"]["amp_change_bas"];
				}
				else{
					
					o = parameters_controlLayer->set["parameters"]["offset_change"];
					a = parameters_controlLayer->set["parameters"]["amp_change"];
				}
			}
			(kv.second)->apply(o,a);
		}
		else
			(kv.second)->apply();

	}
}

void Rob::update_interneuron_network(){
	for(auto& kv: motoneurons)
		kv.second->input = 0;
	for(auto& kv: connections){
		// Apply gate
		if(kv.second->isGateOpen(this)){ //if(isGateOpen((Interneuron *)kv.second->input)){
			kv.second->step(); // not working
			//kv.second->output->add(kv.second->input->get());
		}
		
	}
}
void Rob::setHDistToCM(){
	setHDistToCM("left");
	setHDistToCM("right");
}
void Rob::setHDistToCM(string side){
	Joint * hip;
	Joint * knee;
	double hip_knee = 0.5;
	double knee_ankle = 0.5;
	double foot_length = 0.16;
	if(side == "right"){
		hip = joints["HIP_RIGHT"];
		knee = joints["KNEE_RIGHT"];
	}
	else{
		hip = joints["HIP_LEFT"];
		knee = joints["KNEE_LEFT"];
	}
	double hdisttocm = 
	(
		hip_knee*sin(hip->getAngle())+
		knee_ankle*(
			cos(knee->getAngle())*sin(hip->getAngle()+trunk->theta)+
			sin(knee->getAngle())*cos(hip->getAngle()+trunk->theta)
		)
	)/foot_length;
	

	if(side == "right"){
		//if(hdisttocm != hdisttocm_right)
		//cout << "Right HDIST is : " << hdisttocm << "\t\t" << parameters_controlLayer->set["parameters"]["stance_end"]+parameters_geyer["stance_end"] << " " << getLimbState("right") << endl;
		hdisttocm_right = hdisttocm;
		
	}
	else{
		hdisttocm_left = hdisttocm;
	}
}

void Rob::setVDistToCM(){
	setVDistToCM("left");
	setVDistToCM("right");
}
void Rob::setVDistToCM(string side){
	Joint * hip;
	Joint * knee;

	bool global = true;
	double foot_length = 0.16;
	double vdisttocm=0.0;
	if ( global ){
		double ankle_pos;
		if(side == "right"){
			ankle_pos = this->getFromDef("ANKLE_RIGHT")->getPosition()[1];
		}
		else{
			ankle_pos = this->getFromDef("ANKLE_LEFT")->getPosition()[1];
		}
		
		vdisttocm = (this->getFromDef("REGIS")->getPosition()[1] - ankle_pos);
	}
		
	if ( ! global ) {
		double hip_knee = 0.5;
		double knee_ankle = 0.5;
		
		if(side == "right"){
			hip = joints["HIP_RIGHT"];
			knee = joints["KNEE_RIGHT"];
		}
		else{
			hip = joints["HIP_LEFT"];
			knee = joints["KNEE_LEFT"];
		}
		
		vdisttocm = 
		(
			hip_knee*cos(hip->getAngle())+
			knee_ankle*(
				sin(knee->getAngle())*cos(hip->getAngle()+trunk->theta)+
				cos(knee->getAngle())*sin(hip->getAngle()+trunk->theta)
			)
		)/foot_length;
	}
	if(side == "left")
		vdisttocm_left = vdisttocm;
	else
		vdisttocm_right = vdisttocm;
}

string Rob::getLimbState(string side){
	Foot * foot;
	Foot * cfoot;
	double hdist;
	if(side == "right"){
		foot = right_foot;
		cfoot = left_foot;
		hdist = hdisttocm_right;
	}
	else{
		foot = left_foot;
		cfoot = right_foot;
		hdist = hdisttocm_left;
	}
	if (side == "left")
		hdist = hdisttocm_left;
	else
		hdist = hdisttocm_right;
	string limb_state;
	if(foot->inStance()){
		if((cfoot->inStance() && finishing_stance==side))
			limb_state = "stance_end";
		else
			limb_state = "stance_firstpart";
		//if(!state.is_in_launching_phase)
		//	if(hdist > parameters_geyer["stance_end"]+parameters_controlLayer->set["parameters"]["stance_end"])
		// limb_state = "stance_end";
	}
	else{
		if(hdist < parameters_geyer["swing_end"]+parameters_controlLayer->set["parameters"]["swing_end"])
			limb_state = "swing_end";
		else
			limb_state = "swing_firstpart";
	}
	
	//if(side == "left")
	//	cout << limb_state << endl;
	return limb_state;
}
bool Rob::isGateOpen(Interneuron * interneuron){
	
	bool add=false;
	/*** FEEDBACK ALWAYS ACTIVE */
	if(interneuron->activeDuring() == "cycle") add=true;
	/*** FEEDBACK ACTIVE DURING STANCE / SWING */
	if(getLimbState(interneuron->getOutputEntity()->side).find(interneuron->activeDuring()) != string::npos) add=true;
	/*** FEEDBACK ANTI OVER EXTENSION */
	if(interneuron->getOutputEntity()->side == "left" && interneuron->activeDuring() == "angleoffset" && joints[this->name+"KNEE_LEFT"]->angle > phi_knee_off && joints[this->name+"KNEE_LEFT"]->d_angle > 0.0) add=true;
	if(interneuron->getOutputEntity()->side == "right" && interneuron->activeDuring() == "angleoffset" && joints[this->name+"KNEE_RIGHT"]->angle > phi_knee_off && joints[this->name+"KNEE_RIGHT"]->d_angle > 0.0) add=true;
	
	if(add){ return true; }
	else return false;
}
void Rob::applyGateNeuron(Interneuron * interneuron){
	//change : was 
	if(isGateOpen(interneuron)) interneuron->getOutputEntity()->add(interneuron->get()); 
	//interneuron->getOutputEntity()->add(interneuron->get());
}
void Rob::compute_muscles_stimulation(double dt){
	for(auto& kv: muscles){
		sregex rex = sregex::compile( "([^_]*)_([^_]*)" );
		smatch mat;
		if (regex_match (kv.first, mat, rex))
			kv.second->stim = parameters_geyer[mat[2]+"_activitybasal"]+motoneurons[kv.first]->get(dt);
	}
	if(right_foot->inStance() && left_foot->inStance()){
		//cout << "double support" << endl;
		if(finishing_stance == "right"){ //double support and right finish the stance
			muscles["right_glu"]->stim -= parameters_controlLayer->set["parameters"]["amp_change_reflex"]*parameters_geyer["deltas"];
			muscles["right_hf"]->stim += parameters_controlLayer->set["parameters"]["amp_change_reflex"]*parameters_geyer["deltas"]; 
		}
		if(finishing_stance == "left"){ //double support and left finish the stance
			muscles["left_glu"]->stim -= parameters_controlLayer->set["parameters"]["amp_change_reflex"]*parameters_geyer["deltas"];
			muscles["left_hf"]->stim += parameters_controlLayer->set["parameters"]["amp_change_reflex"]*parameters_geyer["deltas"];
		}
	}
	int i=-1;
	int stimulation_scheme = 1;
	if(stimulation_scheme!=1)
		for(auto& kv: motoneurons)
			muscles[kv.first]->stim = 0.0;
	switch(stimulation_scheme){
		case 1:
			for(auto& kv: muscles){
				if (kv.second->stim<0.0)      kv.second->stim = 0.0;
				else if (kv.second->stim>1.0) kv.second->stim = 1.0;
			}
			break;
		case 2:
			for(auto& kv: motoneurons){
				i+=1;
				muscles[kv.first]->stim = 0.0;
				
				if(state.duration < 8.0*counter){
					
					if(counter == i){
						cout << "*";
						cout << kv.first << ",stim : " << muscles[kv.first]->A << endl;
						if(state.duration > 8.0*(counter)-4.0)
							muscles[kv.first]->stim = 1.0;
					}
					
				}
				else{
					cout << "MUSCLE " << counter+1 << endl;
					counter += 1;
				}
			}
			break;
		case 3:
			if(state.duration < 0.2){
				muscles["right_hf"]->stim = 1;
			}
			break;
	}
}
void Rob::reinitializeHipServoPosition(){
	Node * servoNode;
	Field * transField;
	Field * rotField;
	const double INITIAL_TRANS[3] = { 0.08, -0.4, 0 };
	const double INITIAL_ROT[4] = { 1, 0, 0, -0.1 };
	// Get HIP LEFT SERVOS
	servoNode = this->getFromDef(this->name+"HIP_LEFT");
	transField = servoNode->getField("translation");
	rotField = servoNode->getField("rotation");
	transField->setSFVec3f(INITIAL_TRANS);
	rotField->setSFRotation(INITIAL_ROT);
	// Get HIP RIGHT SERVOS
	servoNode = this->getFromDef("RIGHT_LEFT");
	transField = servoNode->getField("translation");
	rotField = servoNode->getField("rotation");
	
	transField->setSFVec3f(INITIAL_TRANS);
	rotField->setSFRotation(INITIAL_ROT);
	this->simulationPhysicsReset();
}
void Rob::setHipRot(string limb, double rot){
	if(limb == "left" || limb == "Left" || limb == "LEFT")
	{
		Node * servoNode = this->getFromDef(this->name+"HIP_LEFT");
		Field * transField = servoNode->getField("translation");
		Field * rotField = servoNode->getField("rotation");
		// reset the robot
		const double INITIAL_TRANS[3] = { 0.08, -0.4, 0 };
		const double INITIAL_ROT[4] = { 1, 0, 0, rot };
		transField->setSFVec3f(INITIAL_TRANS);
		rotField->setSFRotation(INITIAL_ROT);
	}	
	else if(limb == "right" || limb == "Right" || limb == "RIGHT")
	{
		Node * servoNode = this->getFromDef(this->name+"HIP_RIGHT");
		Field * transField = servoNode->getField("translation");
		Field * rotField = servoNode->getField("rotation");
		// reset the robot
		const double INITIAL_TRANS[3] = { 0.08, -0.4, 0 };
		const double INITIAL_ROT[4] = { 1, 0, 0, rot };
		transField->setSFVec3f(INITIAL_TRANS);
		rotField->setSFRotation(INITIAL_ROT);
	}
	this->simulationPhysicsReset();
}
void Rob::fixedYAxis(double Y){
	Node * viewpoint;
	Field * viewpoint_pos;
	viewpoint = this->getRoot()->getField("children")->getMFNode(1);
	viewpoint_pos = viewpoint->getField("position");
	
	
	double * newViewPointPos = new double[3];
	double * actualViewPointPos = new double[3];
	
	actualViewPointPos = (double *)viewpoint_pos->getSFVec3f();
	newViewPointPos[0] = actualViewPointPos[0];
	newViewPointPos[1] = Y;
	newViewPointPos[2] = actualViewPointPos[2];
	
	//if(viewpoint->getField("follow")->getSFString() == "")
	viewpoint_pos->setSFVec3f(newViewPointPos);
}
void Rob::check_if_end(){
		if(state.distance > 10.0 && state.meanSpeed < 0.1)
		state.stay_in_loop = false;
		state.stop_reason = 5;
	// -----------------   will we get out of the loop ? --------------------

	if(state.nb_fall > boost::any_cast<int>(Settings::set["max_fall_time"])){
		state.stay_in_loop = false;
		state.stop_reason = 1;
		//this->reinitializeRobotPosition();
	}
	/*
	 * Reason :
	 *  1 : falled
	 *  2 : height (max 3.0)
	 *  3 : vertical distance from foot to cm (min 0.6)
	 *  4 : stabilized confidence level to low (min -8.0)
	 *  5 : too slow (min 0.1 m/s)
	 *  0 : other
	 */
	
	if(state.stabilized_confidence < -8.0){
		state.stay_in_loop = false;
		state.stop_reason = 4;
	}
	
	
	if ( vdisttocm_left < 0.6 ){
		state.stay_in_loop = false;
		state.stop_reason = 3;
	}
	
	//if(state.height > 3.0){
	//	state.stop_reason = 2;
	//	state.stay_in_loop = false;
	//}
	
	
	if(boost::any_cast<int>(Settings::set["backward"])==1 && state.position-state.initial_position > 1.0){
		state.stay_in_loop = false;
	}
	
	if(boost::any_cast<int>(Settings::set["backward"])==0 && state.position-state.initial_position < -1.0){
		state.stay_in_loop = false;
	}

	
	switch(boost::any_cast<int>(Settings::set["optimization_scheme"])){
		case 0:
			if(state.distance > boost::any_cast<double>(Settings::set["distance_max"]))
				state.stay_in_loop = false; 
			if(state.duration > boost::any_cast<double>(Settings::set["duration_max"]))
				state.stay_in_loop = false;
			break;
		case 1:
			if(state.energy > boost::any_cast<double>(Settings::set["energylimit"]))
				state.stay_in_loop = false;
			break;
		case 2:
			if(state.nb_cycle > 80)
				state.stay_in_loop = false;
			break;
	}
}
void Rob::update_state_energy(){
	//penatly of energy consumption if knee is hyperextended (to simulate pain)
	double lower_angle_limit = PI; //if knee extend more than 179°
	double penalty = 50000.0; // [J/(rad * s)]
	
	if(joints[this->name+"KNEE_RIGHT"]->angle > lower_angle_limit){
		state.energy_overextension   += penalty * dt * (joints[this->name+"KNEE_RIGHT"]->angle - lower_angle_limit);
		//cout<<"knee right hyperextension: " << joints[this->name+"KNEE_RIGHT"]->angle*180/PI << endl;
	}
	if(joints[this->name+"KNEE_LEFT"]->angle  > lower_angle_limit){
        state.energy_overextension   += penalty * dt * (joints[this->name+"KNEE_LEFT"]->angle - lower_angle_limit);
		//cout<<"knee left hyperextension"<< joints[this->name+"KNEE_LEFT"]->angle*180/PI << endl;
	}
}
// Gets updated at each time step
void Rob::update_state_global(){
	update_state_energy();
	
	state.height = this->getFromDef("REGIS")->getPosition()[1];
	state.old_position = state.position;
	state.position = this->getFromDef("REGIS")->getPosition()[2];
	
	
	double dx = state.position - state.old_position;
	// Estimate acceleration
	if(abs(state.duration-round(state.duration)) <= 0.001)
		state.old_meanSpeed = state.meanSpeed;
	double dv = (state.meanSpeed-state.old_meanSpeed);
	state.distance += dx;
	state.duration += dt;
	state.inAcceleration = dv;
	state.inSpeed = dx/dt;
	state.meanSpeed += 0.0025 * (state.inSpeed - state.meanSpeed);
	state.meanAcceleration += 0.0025 * (state.inAcceleration - state.meanAcceleration);
	

	//double t = state.duration;
	//	cout << state.muscle_noise << endl;
	// state.muscle_noise = 0.04/(1.0+exp(-0.4*(state.duration-10.0)));
	//if(boost::any_cast<int>(Settings::set["muscle_activation_noise"]) == 1 && state.nb_cycle%2==0 && !state.is_in_launching_phase){
          //state.muscle_noise = 0.4/(1.0+exp(-0.4*(state.duration-10.0)));
	//  state.muscle_noise = (boost::any_cast<double>(Settings::set["muscle_activation_noise_max"]))/(1.0+exp(-0.358*(state.distance-25.0)));
    //    }
    /*
	 *  enables online update of parameters after some steps
	 */
	
	int step_change = boost::any_cast<int>(Settings::set["speed_step_switch"]);
	if(!state.left_up_efx_parameters && state.nb_cycle>=step_change && left_foot->justTouchTheGround())
	{
	//	cout << "left foot update parameters" << endl;
		state.left_up_efx_parameters=true;
	}
	if(!state.right_up_efx_parameters && state.nb_cycle>=step_change && right_foot->justTouchTheGround())
	{
	//	cout << "right foot update parameters" << endl;
		state.right_up_efx_parameters=true;
	}
	
}
// This method updates:
// --> cycle length duration, distance,... (steplength should be replaced by cycle here)
// --> doublestance duration
// --> mean speed
// --> stabilization criterion
		
void Rob::update_state_touchGround(){
	state.nb_cycle++;
	//cout << "Double stance duration: " << state.doublestance_duration << endl;
	state.total_doublestance_duration += state.doublestance_duration;
	state.doublestance_duration_mean += state.doublestance_duration;
	state.doublestance_duration_number += 1;
	
	state.doublestance_duration = 0.0;
	state.stepduration_old = state.stepduration;
	state.stepduration = state.duration - state.stepdurationold_position;
	state.stepdurationold_position = state.duration;
	state.steplength_old = state.steplength;
	state.steplength = state.position - state.steplengthold_position;
	state.steplengthold_position = state.position;

}

void Rob::check_if_stabilized(){
	double v = state.steplength/state.stepduration;
	double v_old = state.steplength_old/state.stepduration_old;
	
	double v_mean = state.distance/state.duration;
	
	double changesPercentage = abs((v - v_old)/v_mean);
	double threshold = 1/100.0;
	double stabilized_threshold = 3.0;
	cout << "Confidence level: " << state.stabilized_confidence << endl;
	cout << "changesPercentage: " << changesPercentage << endl;
	if ( !state.steady_state ){
		// maybe try to implement better smooth threshold :TODO:
		
		if ( changesPercentage < threshold ){
			state.stabilized_confidence+=1.0;
			if(state.steady_after_step == -1)
				state.steady_after_step = state.nb_cycle;
		}
		else if (changesPercentage < 5*threshold){
			state.stabilized_confidence+=1.0-2*changesPercentage/(5*threshold);
		}
		else{
			state.steady_after_step = -1;
			state.stabilized_confidence-=1.0;
		}
		if (state.stabilized_confidence > stabilized_threshold){
			state.stabilized_confidence = 0.0;
			cout << "You stabilized after " << state.nb_cycle-state.steady_after_step << " cycle(s)" <<  endl;
			state.steady_state = true;
			state.steady_after_step = -1;
		}
	}
}
void Rob::update_state(){
	
	if( right_foot->justTouchTheGround()){
		update_state_touchGround();
		if ( state.wait_for_steady_state ){
			check_if_stabilized();
		}
	}
	
	if( 
		right_foot->justTouchTheGround()
			|| 
		left_foot->justTouchTheGround() 
	)
	{
		//:TODO: creating this kinematic class to extract
		// 	 important information at specfic point in the 
		//	 cycle
		//state.kinematics["swing/stance"].update();
	}
	if( 
		right_foot->justGetOffGround() 
			|| 
		left_foot->justGetOffGround() 
	)
	{
		//state.kinematics["stance/swing"].update();
	}
	
	if( left_foot->justTouchTheGround()){
		
	}
	
	if( right_foot->inStance() && left_foot->inStance())
		state.doublestance_duration += dt;
	
	
	
	
	update_state_global();
	
	// extract max foot height
	

	
	
	
	
}


void Rob::listen_(){
	bool detect_new_fall = false;
	while (receiver->getQueueLength()){
		message_PtoW = (PtoW *)receiver->getData();
		detect_new_fall = message_PtoW->detect_new_fall;
		if(Settings::str["extract"] == "force"){
			state.llast_force_y = state.last_force_y;
			state.llast_force_z = state.last_force_z;
			state.llast_force_pos = state.last_force_pos;
			state.last_force_y = message_PtoW->last_force_y; 
			state.last_force_z = message_PtoW->last_force_z;
			state.last_force_pos = message_PtoW->last_force_pos;
		}
		state.is_in_lift_mode = message_PtoW->is_in_lift_mode;
		if(detect_new_fall == 1){
			state.nb_fall ++;
			state.falled = true;
		}
		receiver->nextPacket();
	}
}
void Rob::emit_(){
	//------------  we send a message to the physic plugin -------------
	message_WtoP.backward = boost::any_cast<int>(Settings::set["backward"]);
	if(Settings::str["extract"] == "force" || Settings::str["extract"] == "rp_eval"){
		message_WtoP.force_amplitude = boost::any_cast<double>(Settings::set["force"]);
	}
	emitter->send(&message_WtoP, sizeof(message_WtoP));
	double *pos = new double[20];
	int i=0;
	for(auto&  kv : muscles){
		//cout << kv.second->A << endl;
		pos[i] = kv.second->A;
		i++;
	}
	pos[14] = state.meanSpeed;
	pos[15] = state.meanAcceleration;
	pos[16] = left_foot->getFeltForce();
	pos[17] = left_foot->getFeltForce();
	pos[18] = right_foot->getRealFeltForce();
	pos[19] = right_foot->getRealFeltForce();
	emitter_matlab->send(&pos[16], 4*sizeof(double));
//	emitter_analyzer->send(val, 2*sizeof(double));
}

void Rob::plotTextInWindow(int color){
	int black = 0x000000; // color of the text in the 3d windows
	//int red = 0xFF0000; // color of the text in the 3d windows
	//int green = 0x00FF00; // color of the text in the 3d windows
	//int blue = 0x0000FF; // color of the text in the 3d windows
	
	for(auto kv : parameters_controlLayer->set["parameters"])
		parametersColor[kv.first] = 0x000000;
	
	char label[64];
	// '1' -> 49
	
	// <- 14
	// ^  15
	// -> 16
	// |  17
	int currentKey = keyboardGetKey();
	if(currentKey > 48 && currentKey < 57){
		control = currentKey-48;
	}
	if(currentKey == 315 && control >= 0)
		control--;
	if(currentKey == 317 && control < (int)parameterControlVec.size())
		control++;
	if(currentKey == 70) fixedViewPoint = !fixedViewPoint;
	
	//if(currentKey != 0)
	//cout << "Key pressed : " << currentKey << endl;
	double step = 0.01;
	if(control >= 1 && control <= (int)parameterControlVec.size()){
		parameterControlName = parameterControlVec[control-1];
		parameterControlValue = &parameters_controlLayer->set["parameters"][parameterControlName];
	}
	else{
		control = 0;
		parameterControlName="";
	}
	if(currentKey==316 && control != -1)
		*parameterControlValue += step;
	if(currentKey==314 && control != -1)
		*parameterControlValue -= step;
	
	/*
	parameters_controlLayer->set["parameters"]["amp_change_reflex"] = 1.5;
	if(state.nb_cycle < 5)
		parameters_controlLayer->set["parameters"]["amp_change"] = 1.0;
	else if(state.position-state.initial_position < 17.5)
		parameters_controlLayer->set["parameters"]["amp_change"] = 0.8;
	else if(state.position-state.initial_position < 22.5)
		parameters_controlLayer->set["parameters"]["amp_change"] += 0.0001;
	*/
	if((int)(state.duration*1000) % 10 == 0){
	sprintf(label, "v=%.2f [m/s]\na=%.1lf [m/s2]\ncycle=%d cycle", state.meanSpeed, state.meanAcceleration,state.nb_cycle);
	this->setLabel(1, label, 0.01, 0.01, 0.08, color, 0.0);
	
	if(true)
	{
		sprintf(label, "d=%.1f [m]\nh=%.1f [m]", 
		state.position-state.initial_position , state.height);
		this->setLabel(2, label, 0.01, 0.15, 0.08, color, 0.0);
	}
	else
	{
		sprintf(label, "%.1f [m], %.1f [m] \n%.0f [J] ", 
		state.position-state.initial_position , state.height, state.energy);
		this->setLabel(2, label, 0.01, 0.07, 0.08, color, 0.0);
	}
	double start = 0.3;
	
	int i=3;
	for(auto &kv : parameterControlVec){
		sprintf(label, "%s, %.2f", parameterControl[kv].c_str(),parameters_controlLayer->set["parameters"][kv]);
		if(kv== parameterControlName)
			this->setLabel(i, label, 0.01, start, 0.08, black, 0.0);
		else
			this->setLabel(i, label, 0.01, start, 0.08, color, 0.0);
		start+=0.04;
		i++;
	}
	}
	/*
	if(control != 0){
		sprintf(label, "%s, %.2f", parameterControl[control].c_str(),*parameterControlValue);
		//sprintf(label, "%s, %.1f", parameterControl[3].c_str(),parameters_controlLayer->set["parameters"]["amp_change"]);
		this->setLabel(3, label, 0.01, 0.47, 0.08, black, 0.0);
	}
	else{
		sprintf(label, "control off");
		this->setLabel(3, label, 0.01, 0.47, 0.08, color, 0.0);
	}*/
}
