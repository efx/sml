#include "Optimization.hh"

#define __OPTIMIZATION_HH__

#ifdef OPTIMIZATION
optimization::Webots fake = optimization::Webots::Instance();
optimizer tryGetOpti(){
	return fake;
}
#else 
FakeOpti FakeOpti::fake;
optimizer tryGetOpti(){

	return FakeOpti::Instance();
}
#endif
