#include "Motoneuron.hh"
#include <fstream>
#include <iostream>
using namespace std;

Motoneuron::Motoneuron():Entity("right"),input(0.0),tau(0.01){}
Motoneuron::Motoneuron(string side):Entity(side),input(0.0),tau(0.01){}
Motoneuron::Motoneuron(double state):Entity(state){}
Motoneuron::Motoneuron(double state, double input):Entity(state),input(input){}

double Motoneuron::get(double dt){
	double dA = (input-state) * dt/tau;
	state += dA;
	return state;
} // to change, add a differential equation
void Motoneuron::set(double value){this->state = value;}
void Motoneuron::add(double value){this->input += value;}