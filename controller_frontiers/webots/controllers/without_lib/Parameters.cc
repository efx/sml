#include "Parameters.hh"
#include <fstream>
#include <iostream>
using namespace std;
using namespace optimization::messages::task;
void Parameters::init(std::map<std::string, std::map<std::string, double>> parameters)
{
	for (auto &kv : parameters["parameters"] )
	{
		this->set["default_parameters"][kv.first] = kv.second;
		this->set["parameters"][kv.first] = kv.second;
		this->set["min"][kv.first] = 0.0;
		this->set["max"][kv.first] = 1.0;
	}
	if(parameters.size() != 1){
	for (auto &kv : parameters )
		for (auto &kv2 : kv.second )
			this->set[kv.first][kv2.first] = kv2.second;
	}
}
void Parameters::loadParam_fromfile(string file)
{
	std::vector<std::string> strs; // the vector containing the key->value pair
	ifstream myfile (file);
	string line ; // the string containing the line
	if (myfile.is_open()){
		while( std::getline( myfile, line ) )
		{
			boost::split(strs, line, boost::is_any_of("\t "));
			
			if(strs.size() == 2){
			//cout << strs[0] << " " << convertToDouble(strs[1]) << endl;
			this->set["parameters"][strs[0]] = convertToDouble(strs[1]);
			}
		}
		myfile.close();
	}
	else{
	cout<<"ERROR: unable to open file '" << file << "'. In:Parameters.cc, constructor(string, string)"<<endl;
	}
}
void Parameters::loadRange_fromfile(string file)
{
	std::vector<std::string> strs;
	ifstream myfile (file);
	string line ;
	if (myfile.is_open()){
		while( std::getline( myfile, line ) )
		{
			boost::split(strs, line, boost::is_any_of("\t "));
			this->set["min"][strs[0]] = convertToDouble(strs[1]);
			this->set["max"][strs[0]] = convertToDouble(strs[2]);
			//cout << this->set["min"][strs[0]] << "-" << this->set["max"][strs[0]] << endl;
		}
		myfile.close();
	}
	else{
	cout<<"ERROR: unable to open file '" << file << "'. In:Parameters.cc, constructor(string, file)"<<endl;
	}
}
void Parameters::loadParam_fromopti(){
	optimization::Webots &opti = optimization::Webots::Instance();
	if (opti){
		Task::Parameter param;
		for(auto&& kv : this->set["parameters"]) {
			//cout << kv.first << endl;
			if(opti.Parameter(kv.first, param))
				this->set["parameters"][kv.first] = param.value();
			else
				cout << kv.first << " not found " << endl;
		}
	}
}
//rescale the parameters using the min range
void Parameters::rescale(map<std::string, double> &rescaled_set)
{
	for(auto& kv : this->set["parameters"]) {
		auto it = this->set["min"].find(kv.first);
		if(it != this->set["min"].end())
		{
			rescaled_set[kv.first] = ( this->set["max"][kv.first] - this->set["min"][kv.first] ) * kv.second + this->set["min"][kv.first] ;
			//cout << "key:" << kv->first << "," << rescaled_set[kv->first] << endl;
			//rescaled_set[kv.first] = ((double)round(rescaled_set[kv.first]*1000))/1000.0;
		}
		else
		{
			rescaled_set[kv.first] = kv.second;
		}
	}
}
void Parameters::sendTo(std::map<std::string, double> &set2){
	if(boost::any_cast<int>(Settings::set["rescale_parameters"]) == 1 )
		this->rescale(set2);
	else
		for(auto& kv : this->set["parameters"]) {
			set2[kv.first] = kv.second;
		}
}
/*
void Parameters::sendTo(std::map<std::string, double> &rescaled_set, std::string side){
	if(boost::any_cast<int>(Settings::set["rescale_parameters"]) == 1 )
		this->rescale(set2);
	else
		for(auto& kv : this->set["parameters"]) {
			set2[kv.first] = kv.second;
		}
}*/
void Parameters::show(){
	for(auto& kv : this->set["parameters"]) {
		cout << "key:" << kv.first << "," << kv.second << endl;
	}
}
