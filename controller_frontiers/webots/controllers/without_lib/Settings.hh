#ifndef __SETTINGS_HH__
#define __SETTINGS_HH__

#include <string>
#include <vector>
#include <iostream>
#include <math.h>

#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <map>
//#include <unordered_map>
#include <optimization/webots.hh> //to include the optimizer framework
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/any.hpp>

//#include <tinyxml/tinyxml.h>
//#include <tinyxml/tinystr.h>

typedef std::map<std::string, double>::iterator it_map;

class Settings
{
public:
	static std::map<std::string, boost::any> set;
	static std::map<std::string, std::string> str;
	static std::string jobName;
	static std::string config;
	Settings();
	Settings(std::string str);
	void init(std::string str);
	std::string load_settings(std::string setting_name, std::string setting_value);
	boost::any load_settings(std::string setting_name, boost::any setting_value, std::string setting_type);
};
#endif /* __SETTINGS_HH__ */
