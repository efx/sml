#include "Muscle.hh"
#include <fstream>
#include <iostream>
#include <boost/any.hpp>
#include "Parameters.hh"
#include "Settings.hh"

#include <efx/rungekutta.h>


#include <boost/random/normal_distribution.hpp>
#include <boost/random.hpp>
using namespace std;

//constructor
typedef boost::mt19937 RandomGenerator;
static RandomGenerator rng(static_cast<unsigned> (time(0)));

// Gaussian
typedef boost::normal_distribution<double> NormalDistribution;
typedef boost::variate_generator<RandomGenerator&, \
NormalDistribution> GaussianGenerator;


static NormalDistribution nd_amplitude(0.0, 1.0);
static GaussianGenerator gauss(rng, nd_amplitude);
 
vector<double> dadt(double x, vector<double> y, vector<double> param, vector<double> input){
	vector<double> dydt(y);
	vector<double> in(input);
	//double c1 = 3.3;
	//double c2 = 16.7;
	//dydt.at(0) = 5*(c1*in[0]+c2)*(in[0]-y[0]);
	dydt.at(0) = 100*(in[0]-y[0]);
	return dydt;
}
vector<double> dL_CEdt(double x, vector<double> y, vector<double> param, vector<double> input){
	vector<double> dydt(y);
	vector<double> in(input);
	//double c1 = 3.3;
	//double c2 = 16.7;
	//dydt.at(0) = 5*(c1*in[0]+c2)*(in[0]-y[0]);
	dydt.at(0) = in[0];
	return dydt;
}

Muscle::Muscle(SIDE::Side side, std::string name, double l_slack, double l_opt, double v_max, double mass, double pennation, bool act_clockwise, Joint * joint0, double r0, double angle_ref0, double angle_max0, double typeI_fiber) :
// Constant
side(side),
name(name),
E_ref(0.04),
c(log(0.05)), 
w(0.56), 
N(1.5),
K(5.0),
tau_act(0.01), 
F_per_m2(250000), 
density(1060),
noise(0.0),
// Specific variable
act_clockwise(act_clockwise), joint0(joint0), angle_ref0(angle_ref0), angle_max0(angle_max0), r0(r0), l_slack(l_slack), l_opt(l_opt), v_max(v_max*l_opt),mass(mass),F_max(F_per_m2 * mass*pennation/(density*l_opt)), pennation(pennation), typeI_fiber(typeI_fiber),
// Muscle variables
l_SE(0.0), l_CE(0.0), v_CE(0.0), F_SE(0.0), F_PE(0.0), F_BE(0.0), F_MF(0.0), F_MTC(0.0), f_v(0.0), f_l(0.0),
// Energy muscle variables
d_energyW(0.0), d_energyM(0.0), d_energyA(0.0), d_energyS(0.0),d_energy(0.0), cst_energyM(100.0)
{
	stim = 0.01;
	nu = 0.01;
	a.push_back(stim);
	A = a[0];
	initialise_muscle_length();
}

//destructor
Muscle::~Muscle()
{}






//--------------METHODES-------------------------------------------------------





//compute l_SE and l_CE for a given l_MTC at steady state with v_CE=0.0 and A=0.0
void Muscle::initialise_muscle_length(){
	ComputeMuscleLength();
	if(l_MTC < (l_slack + l_opt)){
		l_CE = l_opt;
		l_SE = l_MTC - l_CE;
	}
	else{
		if (l_opt*w + E_ref*l_slack != 0.0)
			l_SE = l_slack * ( (l_opt*w + E_ref*(l_MTC-l_opt)) / (l_opt*w + E_ref*l_slack) );
		else
			l_SE = l_slack;
		l_CE = l_MTC - l_SE;
	}
	l_ce.push_back(l_CE);
}

//compute the new muscle state for one time step
void Muscle::step(double dt){
	/**
	 * 	TYPE OF MUSCLE
	 * 
	 * 	0 : Geyer model, adapted by Steeve
	 * 	1 : Geyer model as described in H.Geyer 2010
	 * 	2 : H. van der Kooij model
	 */
	/** 	Muscle simulation step
	 * 
	 *  	ALL MODELS ARE HILL MASED MODEL
	 * 
	 * 
	 * 
	 * 
	 */
	double numerator;
	double denominator;
	double val;
	double exposant;
	/**	Neural Input	*/
	// Compute stimulation
	//dA = (stim-A) * dt/tau_act;
	vector<double> input;
	input.push_back(stim);
	for(int i=0; i<10; i++)
	{
		
		a=rungeKutta4(state.duration+dt/10*i,dt/10,a,input,(*dadt));

	}
	A=a[0];
	// Add noise
	
	if(boost::any_cast<int>(Settings::set["muscle_activation_noise"]) == 1 && !state.is_in_launching_phase)
	//if(boost::any_cast<int>(Settings::set["muscle_activation_noise"]) == 1)
	{
		noise = (rnd()-0.5)/sqrt(0.0833) * A * A * nu * 20;
		A += noise;
		//if (state.nb_cycle % 10 == 0 && state.nb_cycle != 0){
		//	nu+=0.1/(state.distance/(state.nb_cycle+0.01)*1000);
		//}
	}
	
	A = (A < 0.01) ? 0.01 : A;
	A = (A > 1.0) ? 1.0 : A;
	
	int type = boost::any_cast<int>(Settings::set["muscle_model"]);
	switch(type){
		case 0:
		/** passive element forces */
			numerator 	= (l_SE-l_slack);
			denominator 	= (l_slack*E_ref);
			if(denominator == 0.0)
				denominator = l_slack;
			F_SE = (l_SE > l_slack 		? 	F_max * ( numerator/denominator ) * ( numerator/denominator ) : 0.0);
			numerator	= (l_CE-l_opt);
			denominator	= (l_opt*w);
			if(denominator == 0.0)
				denominator = l_opt;
			// A VOIRE 
		//	F_PE = (l_CE > l_opt 		? 	F_max * ( numerator/denominator ) * ( numerator/denominator ) : 0.0);
			F_PE = (l_CE > l_opt 		? 	F_max * ( numerator/denominator ) * ( numerator/denominator )  : 0.0);
			numerator 	= (l_CE-l_opt*(1.0-w));
			denominator 	= (l_opt*sqrt(w/2.0));
			if(denominator == 0.0)
				denominator = l_opt;
			F_BE = (l_CE <= l_opt*(1.0-w) 	? 	F_max * ( numerator/denominator ) * ( numerator/denominator ) : 0.0);
		/** active element force */
			F_MF = F_SE - F_PE + F_BE;
			//F_MF = (F_MF<0.0) ? 0.0 : F_MF;
		/** length-force curve */
			numerator = (l_CE-l_opt);
			denominator = (l_opt*w);
			if(denominator == 0.0)
				denominator = l_opt;
			val = fabs(numerator/denominator);
			exposant = c * val * val * val;
			f_l = exp( exposant );
			if(f_l<0.001) f_l=0.001;
			//if(f_l<0.001) f_l=0.001;
		/** velocity-force curve */
			if ( F_max * A * f_l == 0.0)
				f_v = F_MF/F_max;
			else
				f_v = F_MF/(F_max*A*f_l);
			f_v = (f_v > 1.5) ? 1.5 : f_v;
			f_v = (f_v < 0.0) ? 0.0 : f_v;
			break;
		case 1:
		/** passive element forces */
			numerator 	= (l_SE-l_slack);
			denominator 	= (l_slack*E_ref);
			F_SE = (l_SE > l_slack 		? 	F_max * ( numerator/denominator ) * ( numerator/denominator ) : 0.0);
			numerator	= (l_CE-l_opt);
			denominator	= (l_opt*w);
			if(denominator == 0.0)
				denominator = l_opt;
			double F_PE_star;
			F_PE_star = (l_CE > l_opt 	? 	F_max * ( numerator/denominator ) * ( numerator/denominator ) : 0.0);
			numerator 	= (l_CE-l_opt*(1.0-w));
			denominator 	= (l_opt*w/2.0);
			if(denominator == 0.0)
				denominator = l_opt;
			F_BE = (l_CE <= l_opt*(1.0-w) 	? 	F_max * ( numerator/denominator ) * ( numerator/denominator ) : 0.0);
		/** length-force curve */
			numerator = (l_CE-l_opt);
			denominator = (l_opt*w);
			if(denominator == 0.0)
				denominator = l_opt;
			val = fabs(numerator/denominator);
			exposant = c * val * val * val;
			f_l = exp( exposant );
			if(f_l<0.001) f_l=0.001;
		/** velocity-force curve */
			if(F_max*A*f_l+F_PE_star == 0.0)
				f_v = 0.0;
			else
				f_v = (F_SE+F_BE)/((F_max*A*f_l)+F_PE_star); 
			f_v = (f_v > 1.5) ? 1.5 : f_v;
			f_v = (f_v < 0.0) ? 0.0 : f_v;
		/** active element force */
			F_PE = f_v*F_PE_star;
			F_MF = F_SE - F_PE + F_BE;
			F_MF = ((F_MF>0.0) ? F_MF : 0.0);
			break;
	}

	/**
	* -------------- Update muscle state
	* 
	*/
	// active element speed
	v_CE = (f_v < 1.0) ? v_max * (1.0-f_v)/(1.0+f_v*K) : v_max * (f_v-1.0)/( 7.56*K*(f_v-N)+1.0-N);
	input[0] = -v_CE;
	l_ce=rungeKutta4(state.duration,dt,l_ce,input,(*dL_CEdt));
	l_CE = l_ce[0];
	//l_CE = (l_CE > 0.0) ? l_CE + v_CE : 0.0;
	// muscle length
	ComputeMuscleLength();
	// passive element length
	l_SE = l_MTC - l_CE;
	// active element activity
	
	/** 
	 * ------------- Energy used by this muscle during this time step 
	 */
	
	
	d_energyA = mass*fA();
	d_energyM = mass*g()*fM();
	
	d_energyS = -v_CE > 0.0 ? 0.25*F_MTC*(-v_CE) : 0.0;
	d_energyW = -v_CE > 0.0 ? F_MF*(-v_CE) : 0.0;
	
	d_energy = dt*( d_energyA + d_energyM + d_energyS + d_energyW );
	
}
double Muscle::fA(){
	return 40 * typeI_fiber * sin( PI/2 * stim) + 133*(1 - typeI_fiber)*(1-cos(PI/2* stim));
}
double Muscle::fM(){
	return 74 * typeI_fiber * sin( PI/2 * A) + 111*(1 - typeI_fiber)*(1-cos(PI/2* stim));
}
double Muscle::g(){
	double l = l_CE/l_opt;
	if(l >= 0 && l <= 0.5)
		return 0.5;
	else if(l > 0.5 && l <= 1.0)
		return l;
	else if(l > 1.0 && l <= 1.5)
		return -2*l+3;
	else
		return 0;
}
void Muscle::ComputeFMTC(){
	F_MTC = F_SE;
}

void Muscle::ComputeMuscleLength()
{
	// here is how to compute the muscle length
	//l = l_slack + l_opt + pennation * r_0 * ( sin(phi-phi_max) - sin(phi_ref-phi_max) )
	l_MTC = l_slack + l_opt + pennation * r0 * joint0->angleTF(angle_ref0, angle_max0);
}
void Muscle::ApplyForce()
{
	double flag = 1.0;
	if(act_clockwise == false)
		flag = -1.0;
	joint0->AddForce(flag*abs(r0)*F_MTC*joint0->angleTFprime(angle_max0));
}
// generate a random number in the range [0;1]
double Muscle::rnd(){
	//return gauss();
	return (double)rand()/RAND_MAX;
}
/*
 * POSTURAL MUSCLE
 * 
 */
PosturalMuscle::PosturalMuscle(SIDE::Side side, std::string name, double l_slack, double l_opt, double v_max, double mass, double pennation, bool act_clockwise, Joint * joint0, double r0, double angle_ref0, double angle_max0, Joint * joint1, double r1, double angle_ref1, double angle_max1, double typeI_fiber) : Muscle(side, name, l_slack, l_opt, v_max, mass, pennation, act_clockwise, joint0, r0, angle_ref0, angle_max0,typeI_fiber), joint1(joint1), angle_ref1(angle_ref1), angle_max1(angle_max1), r1(r1)
{
}
void PosturalMuscle::ComputeMuscleLength()
{
	l_MTC = l_slack + l_opt 
	+ pennation * r0 * joint0->angleTF(angle_ref0, angle_max0)
	+ pennation * r1 * joint1->angleTF(angle_ref1, angle_max1);
}
void PosturalMuscle::ApplyForce()
{
	double flag = 1.0;
	if(act_clockwise == false)
		flag = -1.0;
	
	joint0->AddForce(flag*abs(r0)*F_MTC*joint0->angleTFprime(angle_max0));
	joint1->AddForce(flag*abs(r1)*F_MTC*joint1->angleTFprime(angle_max1));
}
