#ifndef __OPTIMIZATION_HH__
#define __OPTIMIZATION_HH__

#ifdef OPTIMIZATION

#include <optimization/webots.hh> //to include the optimizer framework
typedef optimization::Webots& optimizer;
typedef optimization::messages::task::Task::Parameter optimizerParameters;
#else

typedef bool optimizerParameters;
#include <map>
#include <string>
class FakeOpti;
typedef FakeOpti& optimizer;


class FakeOpti{
private:
public:
	static FakeOpti fake;
	static optimizer Instance(){
		return fake;
	}
	void Respond(std::map<std::string, double> fitnesses){
	}
	bool Setting(std::string, std::string){
		return false;
	}
	bool Parameter(std::string a, bool b){
		return false;
	}
	FakeOpti(){
	}
	operator int() const
	{ 
		return 0; 
		
	}
};


#endif

optimizer tryGetOpti();
#endif