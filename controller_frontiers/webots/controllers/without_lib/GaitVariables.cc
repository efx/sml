#include "GaitVariables.hh"
//#include <unordered_map>
#include "config.hh"
#include <codecogs/statistics/moments/mean.h>
#include <codecogs/statistics/moments/variance.h>
#include <cmath>

using namespace std;

//==========================================================================================
// Constructors

GaitVariable::GaitVariable(double _current,double _default):
_current(_current),_default(_default){}

GaitVariableDecorator::GaitVariableDecorator(GaitVariable& _base,double _current,double _default):
GaitVariable(_current,_default),_base(_base){}

GV_update_max::GV_update_max(GaitVariable& _base, double _current,double _default):
GaitVariableDecorator(_base,_current,_default){}
GV_update_min::GV_update_min(GaitVariable& _base, double _current,double _default):
GaitVariableDecorator(_base,_current,_default){}
GV_update_add::GV_update_add(GaitVariable& _base, double _current,double _default):
GaitVariableDecorator(_base,_current,_default){}
GV_update_sub::GV_update_sub(GaitVariable& _base, double _current,double _default):
GaitVariableDecorator(_base,_current,_default){}

GV_endCondition_leftStanceSwingTransition::GV_endCondition_leftStanceSwingTransition(GaitVariable& _base, double _current,double _default):
GaitVariableDecorator(_base,_current,_default){}
GV_endCondition_leftSwingStanceTransition::GV_endCondition_leftSwingStanceTransition(GaitVariable& _base, double _current,double _default):
GaitVariableDecorator(_base,_current,_default){}
GV_endCondition_rightStanceSwingTransition::GV_endCondition_rightStanceSwingTransition(GaitVariable& _base, double _current,double _default):
GaitVariableDecorator(_base,_current,_default){}
GV_endCondition_rightSwingStanceTransition::GV_endCondition_rightSwingStanceTransition(GaitVariable& _base, double _current,double _default):
GaitVariableDecorator(_base,_current,_default){}

GV_updateCondition_left::GV_updateCondition_left(GaitVariable& _base, double _current,double _default):
GaitVariableDecorator(_base,_current,_default){}
GV_updateCondition_right::GV_updateCondition_right(GaitVariable& _base, double _current,double _default):
GaitVariableDecorator(_base,_current,_default){}
GV_updateCondition_stance::GV_updateCondition_stance(GaitVariable& _base, double _current,double _default):
GaitVariableDecorator(_base,_current,_default){}
GV_updateCondition_swing::GV_updateCondition_swing(GaitVariable& _base, double _current,double _default):
GaitVariableDecorator(_base,_current,_default){}

//==========================================================================================
// Destructors
GaitVariable::~GaitVariable(){}

GaitVariableDecorator::~GaitVariableDecorator(){}

GV_update_max::~GV_update_max(){}
GV_update_min::~GV_update_min(){}
GV_update_add::~GV_update_add(){}
GV_update_sub::~GV_update_sub(){}

GV_endCondition_leftStanceSwingTransition::~GV_endCondition_leftStanceSwingTransition(){}
GV_endCondition_leftSwingStanceTransition::~GV_endCondition_leftSwingStanceTransition(){}
GV_endCondition_rightStanceSwingTransition::~GV_endCondition_rightStanceSwingTransition(){}
GV_endCondition_rightSwingStanceTransition::~GV_endCondition_rightSwingStanceTransition(){}

GV_updateCondition_left::~GV_updateCondition_left(){}
GV_updateCondition_right::~GV_updateCondition_right(){}
GV_updateCondition_stance::~GV_updateCondition_stance(){}
GV_updateCondition_swing::~GV_updateCondition_swing(){}

//==========================================================================================
// Methods
double GaitVariable::getStd(){
	if(saved_data.size() > 2)
		return sqrt(Stats::Moments::variance<double>(saved_data.size()-1,&saved_data[0]));
	else
		return 0.0;
}

double GaitVariable::getMean(){
	if(saved_data.size() > 1)
		return Stats::Moments::mean<double>(saved_data.size()-1,&saved_data[0]);
	else
		return saved_data[0];
}

/*! \brief When to save the GaitVariable
 *         Could be at stance/swing transition at swing/stance transition,...
 *
 *  Detailed description starts here.
 */
bool    GaitVariable::endCondition() const 
{
        return false;
}

/*! \brief How to update the value
 *         
 *
 *  Detailed description starts here.
 */
bool    GaitVariable::updateCondition() const 
{
        return false;
}

void    GaitVariable::step(double _val){
        if(updateCondition())
            update(_val);
}

void    GaitVariable::update(double _val)
{
        _current = _val;
}

void    GaitVariable::save()
{
        saved_data.push_back(_current);
        _current = _default;
}


void  GV_update_max::update(double _val){
        if(_val > _current)
            _current = _val;
}

void  GV_update_min::update(double _val){
        if(_val < _current)
            _current = _val;
}

void  GV_update_add::update(double _val){
        _current += _val;
}
void  GV_update_sub::update(double _val){
        _current -= _val;
}

bool GV_updateCondition_left::updateCondition(){
    return false;
}
bool GV_updateCondition_right::updateCondition(){
    return false;
}
bool GV_updateCondition_stance::updateCondition(){
    return false;
}
bool GV_updateCondition_swing::updateCondition(){
    return false;
}



bool GV_endCondition_leftStanceSwingTransition::end_condition(){
    return false;
}
bool GV_endCondition_leftSwingStanceTransition::end_condition(){
    return false;
}
bool GV_endCondition_rightStanceSwingTransition::end_condition(){
    return false;
}
bool GV_endCondition_rightSwingStanceTransition::end_condition(){
    return false;
}



BaseAliment::~BaseAliment()
{

}

Aliment::~Aliment()
{

}

DecorateurGout::~DecorateurGout()
{

}
Decorateur::~Decorateur()
{

}


BaseAliment::BaseAliment(double prix,const std::string& name):
m_prix(prix),m_name(name)
{

}

Aliment::Aliment(double prix,const std::string& name):
BaseAliment(prix,name)
{

}

Decorateur::Decorateur(BaseAliment& base,double prix,const std::string& name):
BaseAliment(prix,name),m_base(base)
{

}

DecorateurGout::DecorateurGout(BaseAliment& base,double prix,const std::string& name):
Decorateur(base,prix,name)
{

}

DecorateurPerissable::DecorateurPerissable(BaseAliment& base,const std::string& limite):
Decorateur(base),m_limite(limite)
{

}

double  BaseAliment::GetPrix() const 
{
        return m_prix;
}
std::string BaseAliment::GetName() const 
{
        return m_name;
}

void BaseAliment::Afficher(std::ostream & flux) const
{        
        flux<<GetName() << " :" << GetPrix();
}    


double DecorateurGout::GetPrix() const 
{
        return (m_base.GetPrix()+m_prix);
}

std::string DecorateurGout::GetName() const 
{
        return (m_base.GetName()+" "+m_name);
}

void DecorateurPerissable::Afficher(std::ostream & flux) const
{
                m_base.Afficher(flux);
                flux<<" "<<m_limite;
}

std::ostream & operator << (std::ostream & flux,const BaseAliment& t)
{

        t.Afficher(flux);
        return flux;
}
