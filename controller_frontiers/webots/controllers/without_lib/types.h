#ifndef __AIRI_TYPES_H__
#define __AIRI_TYPES_H__

#include <string>
#include <map>


namespace EVENT_TYPE{
	typedef enum
	{
		UNIQUE 		= 1,
		LOOP 		= -1,
	} EventType;
}

namespace LIMB_STATE{
	typedef enum
	{
		STANCE_START 	= 0,
		STANCE_END 	= 1,
		SWING_START 	= 2,
		SWING_END 	= 3
	} LimbState;
}

namespace ACTIVE_DURING{
	typedef enum
	{
		WHOLE_CYCLE 	= -1,
		STANCE 		= 1,
		STANCE_START 	= 11,
		STANCE_END 	= 12,
		SWING 		= 2,
		SWING_START 	= 21,
		SWING_END 	= 23,
		ANGLE_OFFSET 	= 3,
	} ActiveDuring;
}

namespace SIDE{
	typedef enum
	{
		LEFT 		= 0,
		RIGHT 		= 1,
		ALL 		= 2
	} Side;
}
namespace SENSORS{
	typedef enum
	{
		LEFT_FOOT 	= 0,
		RIGHT_FOOT 	= 1
	} SensorsName;
}

namespace MUSCLES{
	const int NUMBER = 7;
	typedef enum
	{
		LEFT_HF 	= 0,
		LEFT_GLU 	= 1,
		LEFT_VAS 	= 2,
		LEFT_HAM 	= 3,
		LEFT_GAS 	= 4,
		LEFT_SOL 	= 5,
		LEFT_TA 	= 6,
		RIGHT_HF 	= 7,
		RIGHT_GLU 	= 8,
		RIGHT_VAS 	= 9,
		RIGHT_HAM 	= 10,
		RIGHT_GAS 	= 11,
		RIGHT_SOL 	= 12,
		RIGHT_TA 	= 13,
	} Muscles;
	/*
	std::map<MUSCLES::Muscles,std::string> Names ={
		{ MUSCLES::LEFT_HF, "LEFT_HF" },
		{ MUSCLES::LEFT_GLU, "LEFT_GLU" },
		{ MUSCLES::LEFT_VAS, "LEFT_VAS" },
		{ MUSCLES::LEFT_HAM, "LEFT_HAM" },
		{ MUSCLES::LEFT_GAS, "LEFT_GAS" },
		{ MUSCLES::LEFT_SOL, "LEFT_SOL" },
		{ MUSCLES::LEFT_TA, "LEFT_TA" },
		{ MUSCLES::RIGHT_HF, "RIGHT_HF" },
		{ MUSCLES::RIGHT_GLU, "RIGHT_GLU" },
		{ MUSCLES::RIGHT_VAS, "RIGHT_VAS" },
		{ MUSCLES::RIGHT_HAM, "RIGHT_HAM" },
		{ MUSCLES::RIGHT_GAS, "RIGHT_GAS" },
		{ MUSCLES::RIGHT_SOL, "RIGHT_SOL" },
		{ MUSCLES::RIGHT_TA, "RIGHT_TA" },
	};*/

}
namespace JOINTS{
	const int NUMBER = 6;
	typedef enum
	{
		HIP_LEFT 	= 0,
		KNEE_LEFT 	= 1,
		ANKLE_LEFT 	= 2,
		HIP_RIGHT 	= 3,
		KNEE_RIGHT 	= 4,
		ANKLE_RIGHT 	= 5
	} Joints;
}


namespace SEGMENTS{
	const int NUMBER = 9;
	typedef enum
	{
		TRUNK		= 0,
		HIP_LEFT 	= 1,
		KNEE_LEFT 	= 2,
		ANKLE_LEFT 	= 3,
		HIP_RIGHT 	= 4,
		KNEE_RIGHT 	= 5,
		ANKLE_RIGHT 	= 6,
		HIP_LEFT_ROLL	= 7,
		HIP_RIGHT_ROLL 	= 8
	} Segments;
}

#endif