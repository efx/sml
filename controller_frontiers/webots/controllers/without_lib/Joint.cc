#include "Joint.hh"
#include "State.hh"
extern State state;
using namespace std;

Joint::Joint(std::string jointname, Servo & servo, int time_step,double reference_angle, double angle_min, double angle_max, Supervisor * Regis) : 
 Servo(servo), jointname(jointname),
 dt(double(time_step)/1000.0), reference_angle(reference_angle), angle(reference_angle), d_angle(0.0),
 angle_min(angle_min), angle_max(angle_max), angle_ref(0.0),
 torque(0.0), torque_soft_limit(0.0), soft_limit_stiffness(17.19)
{
	double direction = Regis->getFromDef(this->getName())->getField("rotation")->getSFRotation()[0];
	double alpha     = Regis->getFromDef(this->getName())->getField("rotation")->getSFRotation()[3];	
	initial_phi = direction * alpha;
	enablePosition(time_step);
	//cout << jointname << " " << initial_phi << endl;
	//position = new double[3];
	//position = (double *)Regis->getFromDef(jointname)->getPosition();
}

double Joint::getAngle()
{
//	if(getName().find("KNEE_LEFT")!= string::npos || getName().find("KNEE_RIGHT")!= string::npos)
//		return (this->getPosition() + initial_phi);
//	else
//cout << this->getName() << ": " << this->getPosition() << " " << this->getPosition() + initial_phi << endl;;
		return (this->getPosition() + initial_phi);
}
/**
 * 	the angle_max corresponds to the angle at which the 
 * 	force of the muscle applied to the joints is maximum.
 **/
double Joint::angleTF(double angle_ref, double angle_max)
{
	if(this->getName().find("HIP_RIGHT")!=string::npos || this->getName().find("HIP_LEFT")!= string::npos)
		return angle - angle_ref;
	// H. Geyer angle transfer function
	else if(boost::any_cast<int>(Settings::set["angle_transfer_function"]) == 0)
	{
		return sin(angle - angle_max) - sin(angle_ref - angle_max);
	}
	else
	{
		double alpha = cos(angle_max);
		return (sqrt(-2*alpha*cos(angle)+alpha*alpha+1)-sqrt(-2*alpha*cos(angle_ref)+alpha*alpha+1))/alpha;
	}
}

void Joint::ComputeAngle()
{
	ComputeAngle(dt);
}

void Joint::ComputeAngle(double dt)
{
	double cangle;
	if(getName().find("KNEE_LEFT")!= string::npos || getName().find("KNEE_RIGHT")!= string::npos)
		cangle = reference_angle - initial_phi - this->getPosition();
	else
		cangle = reference_angle + initial_phi + this->getPosition();
	double d_angle_old = d_angle;
	d_angle = (cangle - angle)/dt;
	
	if(getName() == "KNEE_LEFT"){
		if ( (d_angle > 0) - (d_angle < 0) != (d_angle_old > 0) - (d_angle_old < 0)){
			state.nb_knee_sign_changed++;
		}
	}
	angle = cangle;
	torque = 0.0;
}
/**
 * 	the angle_max corresponds to the angle at which the 
 * 	force of the muscle applied to the joints is maximum.
 **/
double Joint::angleTFprime(double angle_max){
// H. Geyer angle transfer function
	if(this->getName().find("HIP_RIGHT")!= string::npos || this->getName().find("HIP_LEFT")!= string::npos)
		return 1.0;
	else if(boost::any_cast<int>(Settings::set["angle_transfer_function"]) == 0)
	{
		return cos(angle-angle_max);
	}
	else
	{
		double alpha = cos(angle_max);
		return sin(angle)/sqrt(1+alpha*alpha-2*alpha*cos(angle));
	}
}
void Joint::AddForce(double force){
	torque += force;
}
int sign(double val){
	if(val < 0.0)
		return -1;
	else
		return 1;
}
void Joint::ComputeTorqueSoftLimit(){
	double flag = 1.0;
	//if(getName().find("KNEE")== string::npos) flag = -1.0;
	//Soft lower limit torque
	//if(getName() == "KNEE_LEFT")
	//	cout << angle*180/3.14 << endl;
	if ( angle < angle_min && d_angle/0.0175 < 1.0){
		torque_soft_limit = flag * soft_limit_stiffness * ( angle - angle_min ) * ( 1.0 - d_angle / 0.0175 );
		//cout << state.duration << ", lower " << getName() << endl;
	}
	//Soft upper limit torque
	else if ( angle > angle_max && d_angle/0.0175 > -1.0 ){ // it was  > 1.0 
		torque_soft_limit = flag * soft_limit_stiffness * ( angle - angle_max ) * ( 1.0 + d_angle / 0.0175 );
		//cout << state.duration << ", upper, " << getName() << endl;
	}
	else 
		torque_soft_limit = 0.0;
}
