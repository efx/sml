#include <stdio.h>
#include "Rob.hh"
#include "State.hh"
#include "Experiment.hh"
#include "CentralClock.hh"
#include "config.hh"
#include <string>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>




using namespace std;

CentralClock * centralclock;
State state;// = new State();
double debug=false;
std::map<std::string, boost::any> Settings::set;
std::map<std::string, std::string> Settings::str;
void loadGeyerParametersName(std::map<std::string, std::map<std::string, double> >& param);
void loadControlParametersName(std::map<std::string, std::map<std::string, double> >& param);
int main(int argc, char* argv[])
{
	//if(argc == 2)
	//	Settings settings(argv[1]);
	//else
		Settings settings;
		
	if(boost::any_cast<int>(Settings::set["log"]) == 2){
		ofstream out("../../../log/webots_log.txt", ios::app);
		streambuf *coutbuf = cout.rdbuf(); //save old buf
		cout.rdbuf(out.rdbuf()); //redirect std::cout to out.txt!
	}
	cout << "--------------------------- " << endl;
	srand(time(0));
	
	
	std::map<std::string, std::map<std::string, double> > geyer;
	//std::map<std::string, std::map<std::string, double> > efx;
	std::map<std::string, std::map<std::string, double> > control;
	loadGeyerParametersName(geyer);
	loadControlParametersName(control);
	Parameters *Geyer = new Parameters(geyer);
	Parameters *Control = new Parameters(control);
	
	centralclock = new CentralClock(Geyer->set["parameters"]["freq"]);
	//CentralClock centralclock_main(0.85);
	// Create experiment, send parameters
	//Rob Regis(time_step, &this->parameters, &this->parameters_cpg);
	Rob *Regis = new Rob(Geyer, Control);
	Experiment exp(Regis, Geyer, Control);
	
	if(argc == 3)
		exp.change_number = convertToInt(argv[2])-1;
	// Run the experiment
	exp.run();
	
	return 0;
}
void loadControlParametersName(std::map<std::string, std::map<std::string, double> >& param){
	param["parameters"]["offset_change"] = 0.0;
	param["parameters"]["offset_change_bas"] = 0.0;
	param["parameters"]["offset_change_reflex"] = 0.0;
	param["parameters"]["amp_change"] = 1.0;
	param["parameters"]["amp_change_bas"] = 1.0;
	param["parameters"]["amp_change_reflex"] = 1.0;
	param["parameters"]["trunk_ref"] = 0.0;
	
	param["parameters"]["freq_change"] = 1.0;
	param["parameters"]["stance_end"] = 0.0;
	param["parameters"]["swing_end"] = 0.0;
}
void loadGeyerParametersName(std::map<std::string, std::map<std::string, double> >& param){
	// Default parameters
	param["parameters"]["solsol_wf"] = 0.65341611611853989139;
	param["parameters"]["tata_wl"] = 0.332814881307563315;
	param["parameters"]["solta_wf"] = 1.1526980944100497783;
	param["parameters"]["gasgas_wf"] = 1.1068077891810712554;
	param["parameters"]["vasvas_wf"] = 0.77993426421509415292;
	param["parameters"]["hamham_wf"] = 0.49029746580493743791;
	param["parameters"]["gluglu_wf"] = 0.076462083734209956853;
	param["parameters"]["hfhf_wl"] = 0.50585588988083418638;
	param["parameters"]["hamhf_wl"] = 0.82778009455857337606;
	param["parameters"]["ta_bl"] = 0.20741854578256185837;
	param["parameters"]["hf_bl"] = 0.48522419143032552435;
	param["parameters"]["ham_bl"] = 0.48103142880817645333;
	param["parameters"]["kphiknee"] = 0.3612329212386057864;
	param["parameters"]["kbodyweight"] = 0.43017472946576890136;
	param["parameters"]["kp"] = 0.50226761669972064261;
	param["parameters"]["kd"] = 1.1592334201551834916;
	param["parameters"]["deltas"] = 0.73559614103666370877;
	param["parameters"]["sol_activitybasal"] = 0.7310296678150665084;
	param["parameters"]["ta_activitybasal"] = 0.60467744570980996865;
	param["parameters"]["gas_activitybasal"] = 0.31046097011931184095;
	param["parameters"]["vas_activitybasal"] = 0.69071424442607254335;
	param["parameters"]["ham_activitybasal"] = 0.53213646478239429172;
	param["parameters"]["glu_activitybasal"] = 1.1263660725436519527;
	param["parameters"]["hf_activitybasal"] = 0.32943183288119209928;
	param["parameters"]["klean"] = 0.17090550802838316846;
	param["parameters"]["kref"] = 0.105;
	
	param["parameters"]["stance_end"] = 1.0;
	param["parameters"]["swing_end"] = -1.0;
	param["parameters"]["freq"] = 1.0;
}
