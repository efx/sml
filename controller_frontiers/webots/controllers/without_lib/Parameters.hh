#ifndef __PARAMETERS_HH__
#define __PARAMETERS_HH__

/*!
 * \file Parameters.hh
 * \brief Parameters set management
 * \author efx
 * \version 0.1
 */

#include <string>
#include <vector>
#include <iostream>
#include <math.h>

#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <map>
//#include <unordered_map>
#include <optimization/webots.hh> //to include the optimizer framework
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/any.hpp>
#include "str2double.hh"

#include "Settings.hh"

typedef std::map<std::string, double>::iterator it_map;

class Parameters
{
	private:
	void init(); // load default controller range
	void init(std::string mode); // load default range / optimization parameters
	void init(std::map<std::string, std::map<std::string, double> > set);
	public:
	//constante
	std::map<std::string, std::map<std::string, double> > set;

//------------METHODES----------------------------------------

	//constructors
	Parameters(){}
	//Parameters(std::string mode, std::string file);
	Parameters(std::map<std::string, std::map<std::string, double> > set){this->init(set);}
	void loadParam_fromopti();
	void loadParam_fromfile(std::string file);
	void loadRange_fromfile(std::string file);
	//destructor
	~Parameters(){};
	//function
	void rescale(std::map<std::string, double> &rescaled_set);
	void sendTo(std::map<std::string, double> &set);
	void sendTo(std::map<std::string, double> &set2, std::string);			
	void show();
};
#endif /* __PARAMETERS_HH__ */
