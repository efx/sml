#ifndef __MUSCLE_HH__
#define __MUSCLE_HH__

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <math.h>
#include "types.h"
#include "Joint.hh"
#include "State.hh"

extern State state;


class Muscle
{
public:
	SIDE::Side side;
	std::string name;
	//constante
	double nu;
	double E_ref;	//reference strain
	double c;
	double w;
	double N;
	double K;
	double tau_act;	//time cst of the derivative of activation
	double F_per_m2; //force per m2 of muscle physiological cross sectional area
	double density;	//density of muscle;
	double noise; // noise applied to the muscle;
	bool act_clockwise;
	Joint * joint0; // joint on which the muscle is attached
	double angle_ref0; // reference angle at which muscle length = l_slack + l_opt
	double angle_max0;
	double  r0; // one of the length between joint and muscle attachement
	//muscle specific
	double l_slack; //rest length of the serie elastic
	double l_opt;
	double v_max;
	double mass;
	double F_max;
	double pennation;
	double typeI_fiber;
	//variable
	double l_SE;	//length of the serie elastic
	double l_CE;	//length of the contractil element
	double d_l_CE;	 //delta length of the contractil element 
	double v_CE;	//speed of the contractile element
	double l_MTC; //length of the muscle-tendon complex
	double F_SE; //force serie elastic
	double F_PE; //force high parallel elastic
	double F_BE;
	double F_MF; //muscle fibers force
	double F_MTC;
	double f_v;
	double f_l;
	double dA;	 //dA/dt derivative of activation
	std::vector<double> a;		//activation
	double A;
	std::vector<double> l_ce;
	double stim; //stimulation of the muscle
	
	//double r0;
	
	//double steps;
	//double mean_force;
	
	double d_energyW; //energy used by the muscle in a single time step
	double d_energyM;
	double d_energyA;
	double d_energyS;
	double d_energy;
	double cst_energyM;
	

	
	//------------METHODES----------------------------------------
	
	
	//constructor
	Muscle(){};
	//Muscle(double& l_slack, double& l_opt, double& v_max, double& mass, double& pennation, Joint * joint0, double& r0);
	Muscle(SIDE::Side side, std::string name, double l_slack, double l_opt, double v_max, double mass, double pennation, bool act_clockwise, Joint * joint0, double r0, double angle_ref0, double angle_max0,double typeI_fiber);
	//destructor
	~Muscle();
	
	//function
	double fA();
	double fM();
	double g();
	void initialise_muscle_length();//compute l_SE and l_CE for a given l_MTC at steady state
	void step(double dt);//compute the new muscle state for one time step
	virtual void ComputeMuscleLength();
	void ComputeFMTC();
	virtual void ApplyForce();
	//virtual std::vector<double> getForce();
	double rnd();
};

class PosturalMuscle : public Muscle
{
public:
	Joint * joint1;
	double angle_ref1; // reference angle at which muscle length = l_slack + l_opt
	double angle_max1;
	double r1;
	PosturalMuscle(SIDE::Side side, std::string name, double  l_slack, double l_opt, double v_max, double mass, double pennation, bool act_clockwise, Joint * joint0, double r0, double angle_ref0, double angle_max0, Joint * joint1, double r1, double angle_ref1, double angle_max1,double typeI_fiber);
	void ComputeMuscleLength();
	void ApplyForce();
	std::vector<double> getForce();
};
#endif /* __MUSCLE_HH__ */
