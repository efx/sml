#ifndef __Rob_HH__
#define __Rob_HH__


#include <string>
#include <math.h>
#include <vector>
#include <fstream>
#include <map>

//#include <boost/algorithm/string.hpp>


#include <webots/Servo.hpp>
#include <webots/Supervisor.hpp>
#include <webots/TouchSensor.hpp>

#include "Muscle.hh"
#include "Joint.hh"
#include "Parameters.hh"
#include "Settings.hh"
#include "State.hh"
#include "Sensor.hh"

class Interneuron;
class Motoneuron;
class Entity;
class Connection;

#define DEBUG 0


typedef struct{
  int detect_new_fall;  //if we detect a new fall
  int is_in_lift_mode; 
  double last_force_y;
  double last_force_z;
  double last_force_pos;
} PtoW; //from Physic plugin to Webots

typedef struct{
//  int reinitialisation;
  int backward;
  double force_amplitude;
} WtoP; //from Webots to Physic plug in

typedef struct{
  double muscle_signal;
  double muscle_activity;
} WtoM; //from Webots to Raw supervisor

class Rob: public webots::Supervisor
{
private:
	bool flag;
	bool fixedViewPoint;
	int counter;
	void initialise_constant();
	void initialise_joints();
	void initialise_position();
	void initialise_sensors();
	void initialise_communication();
	void initialise_parameters( Parameters * parameters);
	void initialise_muscles();
	void initialise_motoneurons();
	void initialise_feedbacks(Parameters * parameters);
	void initialise_feedbacks(Parameters * parameters, string side);
	void initialise_cpgs( Parameters * parameters);
	std::string name;
	std::string name2;
	
	double hdisttocm_left;
	double hdisttocm_right;
	double vdisttocm_left;
	double vdisttocm_right;
public:
	std::vector<std::string> parameterControlVec;
	std::map<std::string, std::string> parameterControl;
	int control;
	double* parameterControlValue;
	std::string parameterControlName;
	std::map<std::string, int> parametersColor;
	bool feedback_right_loaded;
	bool feedback_left_loaded;
	bool cpg_right_loaded;
	bool cpg_left_loaded;
	void loadFeedback(std::string side);
	void loadPassiveFeedback();
	void loadCpg(std::string side);
	void loadCpg(std::string prefix,std::string prefix2);
	void loadPartialCpg(std::string side);
	void loadPartialCpg(std::string prefix, std::string prefix2);
	void loadPassiveCpg();
	typedef Supervisor super;
	int step();
	//CpgNetwork *network;
	//std::map<std::string, CpgProperty *> networkParameters;
	std::map<std::string, double> parameters_geyer;
	
	int time_step;
	Parameters * parameters_INSenLayer;
	Parameters * parameters_controlLayer;
	double dt;
	double phi_knee_off;
	double soft_limit_stiffness;
	
	//Receiver
	webots::Receiver* receiver;
	PtoW *message_PtoW;
	webots::Emitter* emitter;
	WtoP message_WtoP;
	//Emitter
	webots::Emitter* emitter_matlab;
	webots::Emitter* emitter_analyzer;
	WtoM message_WtoM;
	
	/**
	 * ROBOT POSITION
	 * 
	 */
	std::map<std::string, const double * > initial_rotation;
	std::map<std::string, const double * > initial_translation;
	void set_position(std::string field_name);
	void save_field(std::string field_name);

	/**
	 *  JOINTS
	 * 
	 */
	std::vector<webots::Servo *> servo;
	std::map<std::string, Joint *> joints;
	double theta_trunk_ref;
	Trunk * trunk;
	//std::map<int, double> mean_force;
	//double count;

	double zero_length_offset;
	double one_gain;
	
	/**
	 *  MUSCLES
	 * 
	 */
	std::map<std::string, Muscle *> muscles;
	/**
	 *  MOTONEURONS 
	 */
	std::map<std::string, Motoneuron *> motoneurons;
	/**
	 *  SENSORS
	 */
	std::map<std::string, Sensor *> sensors;
	Foot * left_foot;
	Foot * right_foot;	
	std::string finishing_stance;
	/**
	 *   FEEDBACKS SIGNALS
	 */
	std::vector<std::string> modelized_sensory_interneurons;
	std::vector<std::string> modelized_sensory_interneurons_right;
	std::vector<std::string> modelized_sensory_interneurons_left;
	std::map<std::string, Entity *> feedbacks; // feedbacks interneurones
	std::map<std::string, Entity *> feedbacks_right; // feedbacks interneurones
	std::map<std::string, Entity *> feedbacks_left; // feedbacks interneurones
	std::map<std::string, Entity *> cpgs; // cpgs interneurones
	std::map<std::string, Entity *> cpgs_right; // right limb cpgs interneurones
	std::map<std::string, Entity *> cpgs_left; // left limb cpgs interneurones
	std::map<std::string, Entity *> interneurones; // currently active interneurones
	std::map<Entity *, std::string> feedbacks_name;
	std::map<Entity *, std::string> cpgs_name;
	std::map<Entity *, std::string> interneurones_name;
	
	std::map<std::string, Connection *> connections;
	

  //-----------------------------------
	
	//constructor
	Rob(Parameters * parameters);
	Rob(Parameters * parameters, Parameters * parameters_controlLayer);
	//destructor
	//~Rob();
	
	//function
	double compute_tf(double phi, double phi_max, double phi_ref);
	double compute_ir(double phi,double phi_max);
	
	void setParameters(std::vector<double> parameters);
	void listen_();
	void emit_();
	/**
	 * STATE UPDATE
	 */
	void update_state_touchGround();
	void check_if_stabilized();
	void check_if_end();
	void update_state_energy();
	void update_state_global();
	void update_state();
	void fixedYAxis(double Y);
	/** 0/ */
	void compute_angles(double dt);
	/** 1/ muscles */
	void compute_muscles_state(double dt);
	/** 2/ joints */
	void apply_torque_to_joints();
	/** 3/ sensors */
	void listen_sensors_state();
	/** 4/ neuron layer 1 */
	void update_sensory_neuron_state();
	void update_cpgs_neuron_state();
	void update_interneuron_network();
	void applyGateNeuron(Interneuron * interneuron);
	bool isGateOpen(Interneuron * interneuron);
	void setHDistToCM(std::string side);
	void setHDistToCM();
	void setVDistToCM(std::string side);
	void setVDistToCM();
	std::string getLimbState(std::string side);
	void compute_motoneurones_stimulation();
	/** 5/ muscle input */
	void compute_muscles_stimulation(double dt);

	
	void setRobotPosition();
	void reinitializeRobotPosition();
	void reinitializeHipServoPosition();
	void setHipRot(std::string limb, double rot);
	
	void print_muscle_states();
	void plotTextInWindow(int color);
};
/*
namespace KINEMATIC_DATA_FORMAT{
	typedef enum
	{
		COM_SAGITAL_ANGLE 	= 0,
		L_HIP_SAGITAL_ANGLE 	= 1,
		L_KNEE_SAGITAL_ANGLE 	= 2,
		L_ANKLE_SAGITAL_ANGLE	= 3,
		R_HIP_SAGITAL_ANGLE 	= 4,
		R_KNEE_SAGITAL_ANGLE 	= 5,
		R_ANKLE_SAGITAL_ANGLE	= 6,
		COM_X			= 7,
		COM_Y			= 8,
		L_HIP_SAGITAL_X 	= 9,
		L_HIP_SAGITAL_Y 	= 10,
		L_KNEE_SAGITAL_X 	= 11,
		L_KNEE_SAGITAL_Y 	= 12,
		L_ANKLE_SAGITAL_X	= 13,
		L_ANKLE_SAGITAL_Y	= 14,
		R_HIP_SAGITAL_X 	= 15,
		R_HIP_SAGITAL_Y 	= 16,
		R_KNEE_SAGITAL_X 	= 17,
		R_KNEE_SAGITAL_Y 	= 18,
		R_ANKLE_SAGITAL_X	= 19,
		R_ANKLE_SAGITAL_Y	= 20,
	} Format;
}
class KinematicData{
public:
	int count;
	std::vector<double> current;
	std::vector<double> total;
	KinematicData()
		{
			count = 0;
			for(int i=0;i<size;i++){
				current.push_back(0.0);
				total.push_back(0.0);
			}
			
		}
	void update(){
		get_current();
		add_current();
		cout++
	}
	void get_current(){
		
	}
	void add_current(){
		for (i=0;i<current.size();i++){
			total[i]+=current[i];
		}
	}
	void get_mean(std::vector<double> out){
		for (i=0;i<total.size();i++){
			if (count != 0){
				out[i]=total[i]/(double)count;
			}
			else
				out[i]=0.0;
		}
	}
};
*/
const double MIN = -100000.0;
const double MAX =  100000.0;
class KeepTrack{
public:
	std::vector<double> current;
	std::vector< std::vector<double> > memory;
	int max_size;
	KeepTrack(){
		max_size = 2;
	}
	void add(std::vector<double> one){
		if(one.size() == memory[0].size()){
			if ((int)memory.size() < max_size)
				memory.push_back(one);
			else{
				memory.erase(memory.begin());
				memory.push_back(one);
			}
			
		}
		if(condition(current,one))
			current.swap(one);
	}
	virtual void initialize(std::vector<double>) = 0;
	virtual bool condition(std::vector<double>, std::vector<double>) = 0;
};
class KeepTrack_max : public KeepTrack {
public:
	KeepTrack_max(): KeepTrack() {}
	bool condition(std::vector<double> _old, std::vector<double> _new){
		if ( _old[0] < _new[0] )
			return true;
		else
			return false;
	}
	void initialize(std::vector<double> _in){
		memory.push_back(_in);
		current = _in;
	}
	
};

class KeepTrack_min : public KeepTrack {
public:
	KeepTrack_min(): KeepTrack() {}
	bool condition(std::vector<double> _old, std::vector<double> _new){
		if ( _old[0] > _new[0] )
			return true;
		else
			return false;
	}
	void initialize(std::vector<double> _in){
		memory.push_back(_in);
		current=_in;
	}
	
};
/*
KeepTrack_max max;
std::vector<double> x0 = {MIN, state.distance, state.duration}
max.initialize(x0);
...
std::vector<double> x0 = {regis.left_hip.angle, state.distance, state.duration}
max.add(x0);
*/
#endif /* __ROB_HH__ */
