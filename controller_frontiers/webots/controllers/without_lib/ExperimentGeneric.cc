#include "Experiment.hh"
//#include <unordered_map>
#include "config.hh"
#include "Entity.hh"
#include <codecogs/statistics/moments/correlation.h>
#include <codecogs/maths/interpolation/linear.h>
#include <codecogs/statistics/moments/snr.h>

#include <cmath>
using namespace std;
#ifdef Optimization
using namespace optimization::messages::task;
#endif
extern State state;
extern bool debug;

double human_knee[51] = {3.74, 5.96, 8.33, 10.88, 13.39, 15.31, 16.20, 16.20, 15.75, 15.07, 14.16, 13.10, 12.04, 11.10, 10.28, 9.54, 8.93, 8.47, 8.23, 8.21, 8.36, 8.71, 9.33, 10.25, 11.53, 13.21, 15.47, 18.54, 22.74, 28.32, 35.05, 42.28, 49.12, 54.89, 59.14, 61.71, 62.55, 61.77, 59.52, 55.96, 51.26, 45.57, 39.10, 31.99, 24.44, 16.91, 10.13, 4.98, 2.11, 1.73, 3.21};

double human_hip[51] = {-15.73, -14.69, -13.52, -12.49, -11.60, -10.55, -9.14, -7.54, -6.06, -4.77, -3.50, -2.19, -0.94, 0.16, 1.20, 2.27, 3.34, 4.35, 5.24, 6.07, 6.89, 7.69, 8.43, 9.12, 9.77, 10.39, 10.93, 11.27, 11.05, 9.95, 7.96, 5.27, 2.23, -0.86, -3.80, -6.45, -8.84, -11.04, -13.06, -14.80, -16.20, -17.27, -18.07, -18.53, -18.55, -18.17, -17.55, -16.88, -16.30, -15.90, -15.53};

double human_ankle[51] = {0.57, 2.83, 5.27, 6.46, 5.91, 4.18, 2.19, 0.46, -0.96, -2.13, -3.07, -3.79, -4.35, -4.84, -5.34, -5.84, -6.29, -6.71, -7.16, -7.66, -8.11, -8.43, -8.56, -8.46, -8.03, -7.14, -5.55, -3.06, 0.34, 4.47, 8.91, 13.12, 16.27, 17.55, 16.64, 14.00, 10.48, 6.91, 3.81, 1.34, -0.51, -1.59, -1.72, -1.11, -0.25, 0.34, 0.45, 0.19, -0.14, -0.04, 0.89};
double human_hip_torque[51] = {0.249, 0.600, 0.556, 0.416, 0.359, 0.305, 0.245, 0.159, 0.084, 0.000, -0.064, -0.092, -0.098, -0.092, -0.085, -0.088, -0.100, -0.130, -0.168, -0.199, -0.231, -0.269, -0.312, -0.364, -0.401, -0.404, -0.356, -0.262, -0.251, -0.310, -0.344, -0.295, -0.228, -0.169, -0.126, -0.089, -0.069, -0.057, -0.044, -0.026, -0.009, 0.008, 0.029, 0.060, 0.106, 0.170, 0.242, 0.296, 0.301, 0.237, 0.118};
double human_knee_torque[51] = {-0.196, -0.281, -0.090, 0.173, 0.362, 0.508, 0.593, 0.615, 0.556, 0.469, 0.362, 0.244, 0.141, 0.052, -0.019, -0.070, -0.114, -0.149, -0.181, -0.217, -0.247, -0.269, -0.270, -0.237, -0.171, -0.087, -0.004, 0.054, 0.116, 0.157, 0.156, 0.114, 0.080, 0.066, 0.064, 0.053, 0.037, 0.020, 0.004, -0.009, -0.023, -0.040, -0.059, -0.082, -0.114, -0.158, -0.211, -0.253, -0.263, -0.224, -0.147};
double human_ankle_torque[51] = {-0.009, -0.034, -0.064, -0.051, 0.028, 0.143, 0.260, 0.368, 0.469, 0.545, 0.601, 0.650, 0.692, 0.736, 0.780, 0.825, 0.881, 0.951, 1.037, 1.144, 1.260, 1.388, 1.513, 1.608, 1.628, 1.565, 1.388, 1.073, 0.690, 0.335, 0.102, -0.001, -0.028, -0.023, -0.019, -0.015, -0.012, -0.010, -0.010, -0.010, -0.011, -0.012, -0.013, -0.013, -0.011, -0.006, 0.001, 0.007, 0.011, 0.010, 0.004};


/*
 * 
 * void filter(int,double *,double *,int,double *,double *);
 * 
 */



void filter(int ord, double *a, double *b, int np, double *x, double *y)
{
	int i,j;
	y[0]=b[0]*x[0];
	for (i=1;i<ord+1;i++)
	{
		y[i]=0.0;
		for (j=0;j<i+1;j++)
			y[i]=y[i]+b[j]*x[i-j];
		for (j=0;j<i;j++)
			y[i]=y[i]-a[j+1]*y[i-j-1];
	}
	/* end of initial part */
	for (i=ord+1;i<np+1;i++)
	{
		y[i]=0.0;
		for (j=0;j<ord+1;j++)
			y[i]=y[i]+b[j]*x[i-j];
		for (j=0;j<ord;j++)
			y[i]=y[i]-a[j+1]*y[i-j-1];
	}
} /* end of filter */
void filtfilt(int ord, double *a, double *b, int np, double *x, double *y)
{
	int i;
	filter(ord,a,b,np,x,y);
	/* reverse the series for FILTFILT */
	for (i=0;i<np;i++) x[i]=y[np-i-1];
	/* do FILTER again */
	filter(ord,a,b,np,x,y);
	/* reverse the series back */
	for (i=0;i<np;i++) x[i]=y[np-i-1];
	for (i=0;i<np;i++) y[i]=x[i];
	/* NOW y=filtfilt(b,a,x); boundary handling not included*/
}
void smooth(double * x, int np){
	double y[np];
	int ord=10;
	double a[ord];
	double b[ord];
	for(int i=0;i<ord;i++) a[i] = 0.1;
	for(int i=0;i<ord;i++) b[i] = 1.0;
	
	filter(ord, a, b, np, x, y);
	
	for(int i=0;i<np;i++){
		x[i] = y[i];
	}
	
}



//--------------------- METHODES ----------------------------


void Experiment::initiation(){
	cout << "--------------" << endl;
	writeRawHeader();

	//fitness_file = new ofstream();
	
	parameterControlVec.push_back("amp_change_reflex");
	parameterControlVec.push_back("offset_change_reflex");
	parameterControlVec.push_back("amp_change");
	parameterControlVec.push_back("offset_change");
	parameterControlVec.push_back("amp_change_bas");
	parameterControlVec.push_back("offset_change_bas");
	parameterControlVec.push_back("freq_change");
	parameterControlVec.push_back("stance_end");
	
	parameters_cpg_file = new ofstream();
	parameters_cpg_file_track = new ofstream();
	parameters_cpg_file_associated_state = new ofstream();
	if(boost::any_cast<int>(Settings::set["log"]) == 1){
		if(!fexists("../../../log/parameters_cpg.txt"))
		{
			parameters_cpg_file->open("../../../log/parameters_cpg.txt");
			parameters_cpg_file_associated_state->open("../../../log/parameters_cpg_associated_state.txt");
			parameters_cpg_file_track->open("../../../log/parameters_cpg_track.txt");
			for(auto &parameters: parameterControlVec){
				*parameters_cpg_file << parameters << " ";
				*parameters_cpg_file_track << parameters << " ";
			}
			*parameters_cpg_file << endl;
			*parameters_cpg_file_track << "distance" << endl;
			*parameters_cpg_file_associated_state << "mean_speed steplength steplength_var doublestance_duration spent_energy distance falled" << endl;
		}
		else{
			parameters_cpg_file->open("../../../log/parameters_cpg.txt", ios::app);
			parameters_cpg_file_associated_state->open("../../../log/parameters_cpg_associated_state.txt", ios::app);
			parameters_cpg_file_track->open("../../../log/parameters_cpg_track.txt",ios::app);
		}
	}
	
	
	for(auto &parameters: parameters_cpg->set["parameters"]){
		lastparameters_cpg_value[parameters.first] = parameters.second;
	}
}
void Experiment::termination(){
	cout << "--> trial ended" << endl;
	
	/** --------------  close raw files -------- */
	if(boost::any_cast<int>(Settings::set["save_for_matlab"]) == 1){
		for(auto& kv : raw_files) kv.second->close();
	}
	if(boost::any_cast<int>(Settings::set["log"]) == 1){
		parameters_cpg_file->close();
		parameters_cpg_file_track->close();
		parameters_cpg_file_associated_state->close();
	}


	if(boost::any_cast<int>(Settings::set["extract_video"]) == 1)
		body->stopMovie();
	else{
		this->sendFitness();
	}
	// Evaluation
	if(Settings::str["extract"] != "off"){
		saveForEvaluation(Settings::str["extract"]);
	}
}
void Experiment::saveForEvaluation(string what){
	//not compatible with number_of_repeat !=1
	
	ofstream *file;
	file = new ofstream();
	file->open("../../../evaluation/data/" + Settings::str["extractoutput"], ios::app);
	*file << Settings::str["experimenttype"] << "\t" << Settings::str["modeltype"] << "\t" << Settings::str["worldname"] << "\t" ;
	if(what == "base"){
		*file << 
			fitnesses["distance"] << "\t" << 
			fitnesses["human_hip_correlation"] << "\t" << 
			fitnesses["human_knee_correlation"] << "\t" << 
			fitnesses["human_ankle_correlation"] << "\t" << 
			fitnesses["energy"];
	}
	else if(what == "force"){
		if(fitnesses["falled"])
			*file << fitnesses["distance"] << "\t" << fitnesses["llast_force_y"] << "\t" << fitnesses["llast_force_z"] << "\t" << fitnesses["llast_force_pos"] << "\t" << !fitnesses["falled"];
		else
			*file << fitnesses["distance"] << "\t" << fitnesses["last_force_y"] << "\t" << fitnesses["last_force_z"] << "\t" << fitnesses["last_force_pos"] << "\t" << !fitnesses["falled"];
	}
	else if(what == "distance"){
		*file << fitnesses["distance"] << "\t" << !fitnesses["falled"];
	}	
	else if(what == "rp_eval"){
		*file << boost::any_cast<double>(Settings::set["force"]) << "\t" << !fitnesses["falled"];
	}	
	else if(what == "wg_eval"){
	}
	*file << endl;
	file->close();
	
	
	/*
	 *	extract = base
	 *	>> cat extractoutput
	 *	experimenttype modeltype 	corr hip	corr knee	corr ankle	energy	distance
	 *	
	 *	extract = force
	 *	>> cat extractoutput
	 *	experimenttype modeltype 	distance 	last force amp	last force pos		last force angle
	 *	
	 *	extract = distance
	 *	>> cat extractoutput
	 *	experimenttype modeltype	distance 
	 *	
	 *	extract = rp_eval
	 *	experimenttype modeltype	force 	passed (yes/no)
	 *	
	 *	extract = wg_eval
	 *	experimenttype modeltype	slope	passed (yes /no)
	 */
}

void Experiment::sendFitness(){
	fitnesses = state.getMap();
	
	fitnesses["trunk_mean_angle"] = Stats::Moments::mean<double>(data_joint_angles["trunk"].size()-1,&data_joint_angles["trunk"][0]);
	fitnesses["human_ankle_correlation"] = 0.0; //this->getJointsCorrelationWithHuman("ANKLE_LEFT");
	fitnesses["human_knee_correlation"] = 0.0; //this->getJointsCorrelationWithHuman("KNEE_LEFT");
	fitnesses["human_hip_correlation"] = 0.0; //this->getJointsCorrelationWithHuman("HIP_LEFT");
	
	fitnesses["ankle_torque_sum"] = (this->getTorqueSum("ANKLE_LEFT") + this->getTorqueSum("ANKLE_RIGHT"))/fitnesses["duration"]/1000.0;
	fitnesses["knee_torque_sum"] = (this->getTorqueSum("KNEE_LEFT") + this->getTorqueSum("KNEE_RIGHT"))/fitnesses["duration"]/1000.0;
	fitnesses["hip_torque_sum"] = (this->getTorqueSum("HIP_LEFT") + this->getTorqueSum("HIP_RIGHT"))/fitnesses["duration"]/1000.0;
	
	fitnesses["ankle_SNR"] = 0.0; //this->getSNRWithHuman("ANKLE_LEFT");
	fitnesses["knee_SNR"] = 0.0; //this->getSNRWithHuman("KNEE_LEFT");
	fitnesses["hip_SNR"] = 0.0; //this->getSNRWithHuman("HIP_LEFT");
	if(data_steplength.size()-1 > 15){
		fitnesses["steplengthSNR"] = Stats::Moments::snr<double>(data_steplength.size()-1,&data_steplength[1]);
		
		//double left_mean = Stats::Moments::mean<double>(data_steplength_left.size()-1,&data_steplength_left[1]);
		//double right_mean = Stats::Moments::mean<double>(data_steplength_right.size()-1,&data_steplength_right[1]);
		//double left_var = sqrt(Stats::Moments::variance<double>(data_steplength_left.size()-1,&data_steplength_left[1]));
		//double right_var = sqrt(Stats::Moments::variance<double>(data_steplength_right.size()-1,&data_steplength_right[1]));
		//state.steplengthSNR_leftright = log(fabs(1./(left_mean/left_var-right_mean/right_var)));
		//cout << "steplengthSNR_leftright " << state.steplengthSNR_leftright << endl;
		//cout << "steplengthSNR " << state.steplengthSNR << endl;
	}
	else{
		fitnesses["steplengthSNR"] = 0.0;
		//state.steplengthSNR_leftright = 0.0;
	}
	
	
	if(this->getMode() == "optimization")
	{
		optimizer &opti = tryGetOpti();
		for(auto &kv : fitnesses){
			cout << kv.first << " " << kv.second << endl;
		}
		opti.Respond(fitnesses);
	        if (opti && !opti.Setting("optiextractor"))
	        {
        	        body->simulationQuit(EXIT_SUCCESS);
	        }
	}
	else
		body->simulationQuit(EXIT_SUCCESS);

}


/**
 * Load parameters from launching gate if the file is not found the launching gate at 1.3 m/s is used
 * 
 */
void Experiment::loadReflexParameters(string name){
	cout << "External parameter loading :" << name << endl;
    cout << "\t>>" << Settings::str["config"]+"launching_gate/" + name.c_str() + ".txt" << endl;
	this->parameters->loadParam_fromfile(Settings::str["config"]+"launching_gate/" + name.c_str() + ".txt");
}
/**
 * Load parameters from 
 * - the optimization framework if present 
 * - and from the file parameters.txt if not
 * 
 */
void Experiment::loadCpgParameters(){
	cout << "External parameter loading" << endl;
	if(this->getMode() == "optimization")
		this->parameters_cpg->loadParam_fromopti();	
	else
		this->parameters_cpg->loadParam_fromfile(Settings::str["config"]+"cpg_gate/cpg_parameters.txt");
}
void Experiment::loadCpgParametersFromFile(){
	this->parameters_cpg->loadParam_fromfile(Settings::str["config"]+"cpg_gate/cpg_parameters.txt");
}
void Experiment::loadCpgParametersFromFile(std::string prefix){
	this->parameters_cpg->loadParam_fromfile(Settings::str["config"]+"cpg_gate/"+prefix+".txt");
}
void Experiment::loadReflexParameters(){
	cout << "External parameter loading" << endl;
	if(this->getMode() != "optimization")
		this->parameters->loadParam_fromfile(Settings::str["config"]+"fullReflex_gate/default.txt");
	else
		this->parameters->loadParam_fromopti();
}

void Experiment::loadReflexParametersFromFile(){
	this->parameters->loadParam_fromfile(Settings::str["config"]+"fullReflex_gate/default.txt");
}
void Experiment::loadReflexParametersFromFile(std::string prefix){
	this->parameters->loadParam_fromfile(Settings::str["config"]+"fullReflex_gate/"+prefix+".txt");
}
/** 
 * return the mode of the experiment
 */
std::string Experiment::trySetMode(string mode){
	if(mode == "optimization"){
		cerr << "Trying to get the optimizer... ";
		optimizer &opti = tryGetOpti();
		if (opti){
			cerr << "Found !" << endl;
			this->mode = mode;
		}
		else{
			cerr << "Note Found ! entering normal mode" << endl;
			this->mode = "normal";
		}
	}
	else
		this->mode = mode;
	return this->mode;
}
//return the mode of the experiment
string Experiment::getMode(){ return this->mode; }


void Experiment::addRawData()
{
	if(!state.is_in_launching_phase && last_phase){
		data_time.push_back(data_time.back()+body->dt);
		if(body->left_foot->justTouchTheGround())
		{
			data_steplength.push_back( state.position - laststeppos );
			//cout << state.position - laststeppos << endl;
			laststeppos = state.position;
		}
		
		
		data_joint_angles["trunk"].push_back(body->trunk->theta);
		for(auto& kv : body->joints){
			data_joint_angles[kv.first].push_back(kv.second->getAngle()*180/PI);
			//if(kv.first.find("KNEE") != string::npos)
			data_joint_torques[kv.first].push_back(1./80.*(kv.second->torque_soft_limit));
		}
		if(body->left_foot->justTouchTheGround()){
			//cout << "left " << body->left_foot->distance_last_step << endl;
			data_leftTO.push_back(data_joint_angles["HIP_LEFT"].size()-1);
			data_steplength_left.push_back(body->left_foot->duration_last_step);
		}
		if(body->right_foot->justTouchTheGround()){
			//cout << "right " << body->right_foot->distance_last_step << endl;
			data_rightTO.push_back(data_joint_angles["HIP_LEFT"].size()-1);
			data_steplength_right.push_back(body->right_foot->duration_last_step);
		}
	}
}
int Experiment::changeVectorSize(vector<double>& out, vector<double>& x, vector<double>& y, long start, long end, int N_out)
{
	long N = end-start;
	
	
	//smooth(&y[start], N+100);
	
	//2 Create the interpolant
	if((int)x.size() > end+200){
		Maths::Interpolation::Linear A(N+100, &x[start], &y[start]);
		double xx = data_time[start];
		double step = (data_time[end]-data_time[start+1]) / (N_out-1);
		for (int i = 0; i < N_out; ++i, xx += step)
			out.push_back(A.getValue(xx));
		return 1;
	}
	else{
		for (int i = 0; i < N_out; ++i)
			out.push_back(0.0);
		return 0;
	}
}
double Experiment::getSNRWithHuman(std::string joint)
{
	// number of point of the human data
	int N_out = 51;
	double snr=-10.0;
	if( data_leftTO.size() <= 15 || data_rightTO.size() <= 15)
		return snr;
	double total_snr = 0.0;
	
	unsigned int start = 15;
	unsigned int i = start;
	int n = 0;
	while( i<=data_leftTO.size()-1){
		long start = data_leftTO[i-1];
		long end = data_leftTO[i]-1;
		vector<double> robot_angle;
		if(end-start > 100){
			double * human_angle = new double[N_out];
			if(changeVectorSize(robot_angle, data_time, data_joint_angles[joint], start, end, N_out) == 1){
				if( joint == "ANKLE_LEFT" || joint == "ANKLE_RIGHT" ) 	human_angle = human_ankle;
				if( joint == "KNEE_LEFT"  || joint == "KNEE_RIGHT" )	human_angle = human_knee;
				if( joint == "HIP_LEFT"   || joint == "HIP_RIGHT" )	human_angle = human_hip;
				total_snr += Stats::Moments::snr(N_out, human_angle, &robot_angle[0]);
				n += 1;
			}
		}
		i++;
	}
	//1 Extract the cycle begin and end;
	
	//2 Extract the subpart of interest
	//changeVectorSize(robot_angle, data_time, data_joint_torques[joint], start, end, N_out);
	
	//cout << endl;
	//for(int i=0;i<N_out;i++)
	//    cout << robot_angle[i] << " ";
	//cout << endl;
	return total_snr/((double)(i-15));
}
/*
 * cout << joint << " : " << snr << endl ;
 * cout << joint << " = [ ";
 * for ( auto &it : robot_angle )
 *	cout << it << " " ;
 * cout << "]; " << endl;
 * cout << "h" << joint << " = [ ";
 * for (int i=0; i<N_out; i++ )
 *	cout <<  human_angle[i] << " ";
 * cout << "]; " << endl;
 */

double Experiment::getTorqueSum(std::string joint){
	std::vector<double> & torque = data_joint_torques[joint];
	double total_torque = 0.0;
	for(auto &kv: torque){
		total_torque += abs(kv);
	}
	return total_torque;
}
double Experiment::getJointsCorrelationWithHuman(std::string joint)
{
	// number of point of the human data
	int N_out = 51;
	double corr=0.0;
	if( data_leftTO.size() <= 15 || data_rightTO.size() <= 15)
		return corr;
	double total_corr = 0.0;
	
	unsigned int start = 15;
	unsigned int i = start;
	int n = 0;
	while( i<=data_leftTO.size()-1){
		//1 Extract the cycle begin and end;
		long start = data_leftTO[i-1];
		long end = data_leftTO[i]-1;
		vector<double> robot_angle;
		//2 Extract the subpart of interest
		if(end-start > 100){
			if(changeVectorSize(robot_angle, data_time, data_joint_angles[joint], start, end, N_out) == 1){
				//3 Compute correlation
				if( joint == "ANKLE_LEFT" || joint == "ANKLE_RIGHT" ){
					if (false){
						for(auto &it : robot_angle)
							cout << it << endl;
						cout << endl << endl;
						for(int i=start; i<=end; i++)
							cout << data_joint_angles[joint][i] << endl;
						cout << endl << endl;
						for(int i=0; i<51; i++)
							cout << human_ankle[i] << endl;
						cout << endl << endl;
						cout << fabs(Stats::Moments::correlation<double>(N_out, &robot_angle[0] , human_ankle)) << endl;
					}
					corr = fabs(Stats::Moments::correlation<double>(N_out, &robot_angle[0] , human_ankle));
				}
				if( joint == "KNEE_LEFT"  || joint == "KNEE_RIGHT" )
					corr = fabs(Stats::Moments::correlation<double>(N_out, &robot_angle[0] , human_knee));
				if( joint == "HIP_LEFT"   || joint == "HIP_RIGHT" )
					corr = fabs(Stats::Moments::correlation<double>(N_out, &robot_angle[0] , human_hip));
				total_corr += corr;
				n += 1;
			}
		}
		i++;
	}
	if(n!=0)
		return total_corr/((double)(n));
	else
		return 0.0;
}

// generate a random number in the range [0;1]
double Experiment::rnd(){ return (double)rand()/RAND_MAX; }
/**
 * 
 * Raw file management
 *
 *
 * For a given experiment (run) the 
 * name of the files will have the same id (<name_of_the_file>_id)
 * 
 * 
 */
void Experiment::createRawFiles(){
	int i=1;
	string files[] = {
		"grf",
		"motoneurons_activity",
		"muscles_force",
		"muscles_activity",
		"joints_force",
		"joints_angle",
		"joints_position_y",
		"joints_position_z",
		"distance",
		"footfall",
		"energy",
		"sensors_activity",
		"feedbacks",
		"cpgs",
		"interneurons",
	};
	string out = boost::lexical_cast<std::string>(i);
	while(fexists("../../../raw_files/"+files[0]+out) || fexists("../../../raw_files/"+files[1]+out)){
		i++;
		out = boost::lexical_cast<std::string>(i);
	}
	std::ofstream *file;
	for (auto & it : files){
		file = new ofstream();
		raw_files[it] = file;
		raw_files[it]->open("../../../raw_files/"+it+out);
	}
}
void Experiment::writeRawHeader()
{
	if(boost::any_cast<int>(Settings::set["save_for_matlab"]) == 1){
		//matlab_motoneurons_activity << "%" << Settings::jobName << endl;
		//matlab_joints_force << "%" << Settings::jobName << endl;
		for(auto& kv : this->body->motoneurons)
		{
			*raw_files["muscles_force"] << kv.second << " ";
			*raw_files["motoneurons_activity"] << kv.second << " " ;
			*raw_files["muscles_activity"] << kv.second << " " ;
		}
		for(auto& kv :this->body->joints)
		{
			*raw_files["joints_force"] << kv.second->getName() << " ";
			*raw_files["joints_angle"] << kv.second->getName() << " ";
			*raw_files["joints_position_y"] << kv.second->getName() << " ";
			*raw_files["joints_position_z"] << kv.second->getName() << " ";
		}
		for(auto& kv :this->body->sensors)
		{
			*raw_files["sensors_activity"] << kv.first << " ";
		}
		for(auto& kv :this->body->feedbacks)
		{
			*raw_files["feedbacks"] << kv.first << " ";
		}
		for(auto& kv :this->body->cpgs)
		{
			*raw_files["cpgs"] << kv.first << " ";
		}
		for(auto& kv :this->body->feedbacks)
		{
			*raw_files["interneurons"] << kv.first << " ";
		}
		
		*raw_files["distance"] << "distance";
		*raw_files["footfall"] << "left right";
		*raw_files["energy"] << "energy";
		*raw_files["grf"] << "grf_X" << " " << "grf_Y";
		for(auto &kv : raw_files)
			*kv.second << endl;
	}
}
void Experiment::writeRawContent()
{
	if(boost::any_cast<int>(Settings::set["save_for_matlab"]) == 1 && last_phase)
	{
		for(auto& kv :this->body->muscles)
		{
			*raw_files["muscles_force"] << kv.second->F_MTC << " ";
			*raw_files["motoneurons_activity"] << ((Entity *)this->body->motoneurons[kv.first])->get() << " " ;
			*raw_files["muscles_activity"] << kv.second->A << " " ;
		}
		debug == true ? cout << "--writing muscles" << endl:true;
		for(auto& kv :this->body->joints)
		{
			*raw_files["joints_force"] << kv.second->torque + kv.second->torque_soft_limit << " ";
			*raw_files["joints_angle"] << kv.second->getAngle() << " ";
			*raw_files["joints_position_z"] <<this->body->getFromDef(kv.first)->getPosition()[2]-this->body->getFromDef("REGIS")->getPosition()[2] << " ";
			*raw_files["joints_position_y"] <<this->body->getFromDef(kv.first)->getPosition()[1]-this->body->getFromDef("REGIS")->getPosition()[1] << " ";
		}
		debug == true ? cout << "--writing joints" << endl:true;
		for(auto& kv :this->body->sensors)
		{
			*raw_files["sensors_activity"] << kv.second->get() << " ";
		}
		for(auto& kv :this->body->cpgs)
		{
			//if(kv.first == "right_vas__mff_stance" && !state.is_in_launching_phase && !this->body->cpg_loaded){
				//	*raw_files["feedbacks"] <<this->body->cpgs["right_vas__mff_stance"]->get() << " ";
				//}
				//else
				*raw_files["cpgs"] << kv.second->get() << " ";
		}
		for(auto& kv :this->body->feedbacks)
		{
			//if(kv.first == "right_vas__mff_stance" && !state.is_in_launching_phase && !this->body->cpg_loaded){
				//	*raw_files["feedbacks"] <<this->body->cpgs["right_vas__mff_stance"]->get() << " ";
				//}
				//else
				*raw_files["feedbacks"] << kv.second->get() << " ";
		}
		for(auto& kv :this->body->connections)
		{
			*raw_files["interneurons"] << kv.second->input->get() << " ";
		}
		debug == true ? cout << "--writing sensors" << endl:true;
		*raw_files["distance"] << state.position-state.initial_position;
		*raw_files["footfall"] <<this->body->left_foot->inStance() << " " <<this->body->right_foot->inStance();
		*raw_files["energy"] << state.energy;
		*raw_files["grf"] <<this->body->left_foot->getXForce() << " " <<this->body->left_foot->getYForce();
		for(auto &kv : raw_files)
			*kv.second << endl;
	}
}
