#ifndef __GAITVARIABLES_HH__
#define __GAITVARIABLES_HH__

/*!
 * \file GaitVariables.hh
 * \brief Class & Decorators used to extract gait variables online
 * \author efx
 * \version 0.1
 * 
 * The classes and decorators are inspired from the c++ tutorial on 
 * design patterns for c++. More specifically we use the notion of decorators
 * http://come-david.developpez.com/tutoriels/dps/?page=Decorateur
 * 
 */

#include <string>
#include <iostream>
#include <vector>



//==========================================================================================
class GaitVariable
{
 protected:
    //les données de la classe de base
    std::vector<double> saved_data;
    double _current;
    double _default;
    
    virtual ~GaitVariable()=0;
    GaitVariable(double _current=0,double _default=0);
        
 public:

    virtual bool      endCondition() const;
    virtual bool      updateCondition() const;
    
    virtual void      update(double _val);

    void              step(double _val);
    void   save();
    double getStd();
    double getMean();
};

//==========================================================================================
//Decorator generic class
class GaitVariableDecorator : public GaitVariable
{
        protected:
        GaitVariable& _base;
        
        GaitVariableDecorator(GaitVariable& _base,double _current=0,double _default=0);
        virtual ~GaitVariableDecorator() =0;
public:
        GaitVariable* GetBase() const {return &_base;};
};

//==========================================================================================
//Decorators for update mechanism
class GV_update_max : public GaitVariableDecorator
{
 public:

     void      update(double _val);
    
    GV_update_max(GaitVariable& base,double _current=0,double _default=0);
    ~GV_update_max();
};

class GV_update_min : public GaitVariableDecorator
{
 public:

     void      update(double _val);
    
    GV_update_min(GaitVariable& base,double _current=0,double _default=0);
    ~GV_update_min();
};

class GV_update_add : public GaitVariableDecorator
{
 public:

     void      update(double _val);
    
    GV_update_add(GaitVariable& base,double _current=0,double _default=0);
    ~GV_update_add();
};

class GV_update_sub : public GaitVariableDecorator
{
 public:

     void      update(double _val);
    
    GV_update_sub(GaitVariable& base,double _current=0,double _default=0);
    ~GV_update_sub();
};

//==========================================================================================
//Decorators for update_condition
class GV_updateCondition_stance : public GaitVariableDecorator
{
 public:

    bool      updateCondition();
    
    GV_updateCondition_stance(GaitVariable& base,double _current=0,double _default=0);
    ~GV_updateCondition_stance();
};
class GV_updateCondition_swing : public GaitVariableDecorator
{
 public:

    bool      updateCondition();
    
    GV_updateCondition_swing(GaitVariable& base,double _current=0,double _default=0);
    ~GV_updateCondition_swing();
};
class GV_updateCondition_left : public GaitVariableDecorator
{
 public:

    bool      updateCondition();
    
    GV_updateCondition_left(GaitVariable& base,double _current=0,double _default=0);
    ~GV_updateCondition_left();
};
class GV_updateCondition_right : public GaitVariableDecorator
{
 public:

    bool      updateCondition();
    
    GV_updateCondition_right(GaitVariable& base,double _current=0,double _default=0);
    ~GV_updateCondition_right();
};

//==========================================================================================
//Decorators for end_condition
class  GV_endCondition_leftStanceSwingTransition : public GaitVariableDecorator
{
 public:

    bool      end_condition();
    
    GV_endCondition_leftStanceSwingTransition(GaitVariable& base,double _current=0,double _default=0);
    ~ GV_endCondition_leftStanceSwingTransition();
};

class  GV_endCondition_rightStanceSwingTransition : public GaitVariableDecorator
{
 public:

    bool      end_condition();
    
    GV_endCondition_rightStanceSwingTransition(GaitVariable& base,double _current=0,double _default=0);
    ~ GV_endCondition_rightStanceSwingTransition();
};

class  GV_endCondition_leftSwingStanceTransition : public GaitVariableDecorator
{
 public:

    bool      end_condition();
    
    GV_endCondition_leftSwingStanceTransition(GaitVariable& base,double _current=0,double _default=0);
    ~ GV_endCondition_leftSwingStanceTransition();
};

class  GV_endCondition_rightSwingStanceTransition : public GaitVariableDecorator
{
 public:

    bool      end_condition();
    
    GV_endCondition_rightSwingStanceTransition(GaitVariable& base,double _current=0,double _default=0);
    ~ GV_endCondition_rightSwingStanceTransition();
};


//==========================================================================================
class BaseAliment 
{
 protected:
    //les données de la classe de base
    double m_prix;
    std::string m_name;

        virtual ~BaseAliment()=0;
        BaseAliment(double prix=0,const std::string& name="");
        
 public:

    virtual double      GetPrix() const;
    virtual std::string GetName() const; 

    virtual void Afficher(std::ostream & flux) const;
};

class Aliment : public BaseAliment
{

public:
    Aliment(double prix=0,const std::string& name="");
    virtual ~Aliment();
};
//==========================================================================================
//l'aliment à proprement parler. ici trivial.Il pourrait être substitué par BaseAliment
class Decorateur : public BaseAliment
{
        protected:
        BaseAliment& m_base;
        
        Decorateur(BaseAliment& base,double prix=0,const std::string& name="");
        virtual ~Decorateur() =0;
public:
        BaseAliment* GetBase() const {return &m_base;};
};

//un premier décorateur permettant d'ajouter du goût à un aliment 
//Ce décorateur modifie les fonctions déjà présentes
class DecorateurGout : public Decorateur
{
 public:

     double      GetPrix() const;
     std::string GetName() const; 
    
    DecorateurGout(BaseAliment& base,double prix,const std::string& name);
    ~DecorateurGout();
};

//un deuxième décorateur permettant d'ajouter une date limite à un aliment 
//Ce décorateur ajoute une nouvelle fonction.
class DecorateurPerissable : public Decorateur
{
        std::string m_limite;

        public:
        std::string GetLimite() const {return (m_limite) ;}        

        void Afficher(std::ostream & flux) const;

        DecorateurPerissable(BaseAliment& base,const std::string& limite);
        ~DecorateurPerissable(){}
};

#endif