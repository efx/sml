#ifndef __JOINT_HH__
#define __JOINT_HH__

/*!
 * \file Joint.hh
 * \brief Joint definition
 * \author efx
 * \version 0.1
 */
#include <string>
#include <math.h>
#include <map>
#include <webots/Servo.hpp>
#include <webots/Supervisor.hpp>
#include <boost/any.hpp>

#include "Parameters.hh"
#include "Settings.hh"

#define PI 3.14159265

using namespace webots;
//class Servo;
class Joint: public Servo {
public:
	Joint(std::string jointname, Servo & servo, int time_step,double reference_angle, double angle_min, double angle_max, Supervisor * Regis);
	virtual ~Joint() {}
	//---------------------------
	std::string jointname;
	//double position;
	//double dposition;
	double dt;
	double reference_angle;
	double initial_phi;
	double angle;
	double d_angle;
	double angle_min;
	double angle_max;
	double angle_ref;
	double torque;
	double torque_soft_limit;
	double soft_limit_stiffness;
	//---------------------------
	void AddForce(double force);
	void ComputeAngle();
	void ComputeAngle(double dt);
	
	double angleTF(double angle_ref, double angle_max);
	double angleTFprime(double angle_max);
	void ComputeTorqueSoftLimit();
	double getAngle();
	void ComputeTorque();
	
};

#endif /* __JOINT_HH__ */
