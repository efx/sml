#include "Entity.hh"


void FirstOrderConnection::step(){
	vector<double> in;
	in.push_back(amplitude*weight*this->input->get()+offset);
	
	vector<double> y;
	y.push_back(value);
	
	y=rungeKutta4(state.duration,dt,y,in,(Derivatives *)this);
	value = y[0];
	output->add(value);
}

vector<double> FirstOrderConnection::dydt(double x, vector<double> y, vector<double> param, vector<double> input){
	vector<double> dadt(y);
	//dadt.at(0) = 100*(input[0]-y[0]);
	dadt.at(0) = 100*(input[0]-y[0]);
	return dadt;
}
