#ifndef __EXPERIMENT_HH__
#define __EXPERIMENT_HH__

#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <fstream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <map>
#include <cmath>
#include <boost/lexical_cast.hpp>
#include "State.hh"
#include "Parameters.hh"
#include "Settings.hh"
#include "Rob.hh"

#include "Optimization.hh" //to include or not the optimizer framework


class Phase;

class LinearInterpolant{
public:
	std::string folder;
	std::map<std::string,double> * parameters;
	int parameter_number;
	LinearInterpolant(std::string folder, std::map<std::string,double> * parameters, int parameter_number): folder(folder), parameters(parameters), parameter_number(parameter_number){
	}
	LinearInterpolant(){
		folder = "cpg";
		parameter_number=0;
	}
	
};


class parametersControl{
public:
	std::map<std::string,std::map<std::string,double>> set;
	static void loadFromFile(std::string file, std::map<std::string,std::vector<double>>& set){
		std::vector<std::string> strs1; // the vector containing the key->value pair
		std::vector<std::string> strs2; // the vector containing the key->value pair
		ifstream myfile (file);
		string line ; // the string containing the line
		if (myfile.is_open()){
			while( std::getline( myfile, line ) )
			{
				
				boost::split(strs1, line, boost::is_any_of("\t"));
					for(auto it: strs1){
						boost::split(strs2, it, boost::is_any_of(":"));
						//cout << it << endl;
						if(strs2.size()==2){
							set[strs2[0]].push_back(convertToDouble(strs2[1]));
						}
					}
			}
			myfile.close();
		}
		else{
			cout<<"ERROR: unable to open file '" << file << "'. In:Parameters.cc, constructor(string, string)"<<endl;
		}
	}
	parametersControl(std::map<std::string,double> max, std::map<std::string,double> step){
		init(max,step);
	}
	parametersControl(std::string file,  Parameters * parameters){
		std::vector<std::string> strs; // the vector containing the key->value pair
		ifstream myfile (file);
		string line ; // the string containing the line
		if (myfile.is_open()){
			while( std::getline( myfile, line ) )
			{
				boost::split(strs, line, boost::is_any_of("\t "));
				
				//cout << strs[0] << " " << convertToDouble(strs[1]) << endl;
				this->set["max"][strs[0]] = convertToDouble(strs[1]);
				this->set["step"][strs[0]] = convertToDouble(strs[2]);
			}
			if(strs.size()>3){
				parameters->set["parameters"][strs[0]] = convertToDouble(strs[3]);
			}
			myfile.close();
		}
		else{
			cout<<"ERROR: unable to open file '" << file << "'. In:Parameters.cc, constructor(string, string)"<<endl;
		}
	}
	
	void init(std::map<std::string,double> max, std::map<std::string,double> step)
	{
		for(auto &kv : max){
			set["max"][kv.first] = kv.second;
		}
		for(auto &kv : step){
			set["step"][kv.first] = kv.second;
		}
		
	}
	
};

class Experiment
{
public:
	map<string, double> fitnesses;
	LinearInterpolant *linearInterpolant;
	Rob *body;
	std::vector<Phase *> phases;
	bool last_phase;
	std::vector<Phase *>::iterator phase;
	std::ofstream *fitness_file;
	std::ofstream *parameters_cpg_file;
	std::ofstream *parameters_cpg_file_track;
	std::ofstream *parameters_cpg_file_associated_state;
	std::vector<std::string> parameterControlVec;
	std::map<std::string,std::vector<double>> parameters_cpg_control_file;
	double max_wait;
	double waited;
	double spent_energy;
	bool changed;
	bool first_saved;
	double changed_at_dist;
	int changed_at_nbsl;
	int time_step;
	int change_number;
	// fitness
	bool last_trial();
	void sendFitness();
	int number_of_trials();
	void initiation();
	void termination();
	int RepeatNumber();
	bool loadingEnded;
	
	int color;
	// ---------------------------------------------------
	//std::vector phases;
	int                     nb_parameters;
	double energylimit;  //energy we provide to the robot at each trial
	std::string mode;
	Parameters * parameters;
	Parameters * parameters_cpg;
	std::vector<double> parameter_MIN;
	std::vector<double> parameter_MAX;
	std::vector<double> parameter_robot;
	
	//  std::vector<std::string> raw_files;
	std::map<std::string, std::ofstream*> raw_files;
	std::vector<std::string> raw_filesname;
	std::map<std::string, double> lastparameters_cpg_value;
	
	std::map<std::string, std::vector<double>> data_joint_angles;
	std::map<std::string, std::vector<double>> data_joint_torques;
	std::vector<double> data_leftTO;
	std::vector<double> data_rightTO;
	std::vector<double> data_time;
	std::vector<double> data_steplength;
	std::vector<double> data_steplength_left;
	std::vector<double> data_steplength_right;
	
	parametersControl *parameters_cpg_control;
	double laststeppos;
	//----------------- METHODES -----------------------
	
	
	//Experiment();
	Experiment(Rob * body,Parameters * set);
	Experiment(Rob * body,Parameters * set1,Parameters * set2);
	void init();
	std::string    trySetMode(std::string mode);
	std::string  getMode();
	void    run();//the main of this class
	void loadReflexParameters();
	void loadReflexParameters(std::string speed);
	void loadReflexParametersFromFile();
	void loadGammaFibersParameters();
	void loadGammaFibersParametersFromFile();
	void loadReflexParametersFromFile(std::string prefix);
	void loadCpgParameters();
	void loadCpgParametersFromFile();
	void loadCpgParametersFromFile(std::string prefix);
	void createRawFiles();
	void writeRawHeader();
	void writeRawContent();
	
	void addRawData();
	int changeVectorSize(std::vector<double>& out, std::vector<double>& x, std::vector<double>& y, long start, long end, int N_out);
	double getJointsCorrelationWithHuman(std::string joint);
	double getTorqueSum(std::string joint);
	double getSNRWithHuman(std::string joint);
	State one_trial( std::string mode); //a single trial that return a fitness
	void step();
	double  rnd();             //rnd number generator [0;1]
	
	void saveForEvaluation(std::string what);
};


#endif /* __GRADIENT_HH__ */