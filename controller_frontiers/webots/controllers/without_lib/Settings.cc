#include "Settings.hh"
#include <fstream>
#include <iostream>
#include "Optimization.hh"
using namespace std;
#ifdef Optimization
using namespace optimization::messages::task;
#endif
//constructor
std::string Settings::load_settings(std::string setting_name, std::string setting_value){
	string IN_;
	stringstream s;
	optimizer &opti = tryGetOpti();

	string temp = setting_value;
	cout << setting_name << endl;

	if(opti){if ( opti.Setting(setting_name,IN_) ){
		s << IN_;
		s >> temp;
	}}
	return temp;
}
boost::any Settings::load_settings(std::string setting_name, boost::any setting_value, std::string setting_type){
	string IN_;
	stringstream s;
	//optimizer &opti = tryGetOpti();
	optimizer &opti = tryGetOpti();

	boost::any OUT_;

	//Double
	if(setting_type == "double")
	{
		double temp = boost::any_cast<double>(setting_value);
		if(opti){if ( opti.Setting(setting_name,IN_) ){
			s << IN_;
			s >> temp;
		}}
		OUT_ = temp;
	}
	else
	{
		int temp = boost::any_cast<int>(setting_value);
		if(opti){if ( opti.Setting(setting_name,IN_) ){
			s << IN_;
			s >> temp;
		}}
		OUT_ = temp;
	}
	
	//boost::any OUT_ = setting_value;
	
	return OUT_;
}
//settings
std::string Settings::jobName = "no opti";
Settings::Settings(){
    optimizer &opti = tryGetOpti();
	/**
	 * 
	 * Settings loaded from the optimization framework
	 * 
	 * */
	if(opti){
		this->set["number_of_repeat"] = load_settings("number_of_repeat", 1, "int");
		this->set["muscle_activation_noise_max"] = load_settings("muscle_activation_noise_max", 0.2, "double");
		this->set["randomized_initial_condition"] = load_settings("randomized_initial_condition", 0, "int");
		this->set["max_fall_time"] = load_settings("max_fall_time", 5, "int");
		this->set["number_of_cycles"] = load_settings("number_of_cycles", 50, "int"); // only for optimization_scheme 0
		this->set["energylimit"] = load_settings("energylimit", 5000.0, "double"); // only for optimization scheme 1
		this->set["angle_transfer_function"] = load_settings("angle_transfer_function", 0, "int"); // 0: geyer's one, 1: florin's one
		this->set["optimization_scheme"] = load_settings("optimization_scheme", 0, "int"); //  0: number of step, 1: energy
		this->set["theta|trunkref"] = load_settings("theta|trunkref", 0.105, "double"); //  0: number of step, 1: energy
		this->set["backward"] = load_settings("backward", 0, "int"); //  0: distance, 1: energy
		this->set["save_for_matlab"] = load_settings("save_for_matlab", 0, "int");
		this->set["rescale_parameters"] = load_settings("rescale_parameters", 0, "int");
		this->set["muscle_activation_noise"] = load_settings("muscle_activation_noise", 0, "int");
		this->set["distance_max"] = load_settings("distance_max", 35.0, "double");
		this->set["duration_max"] = load_settings("duration_max", 32.0, "double");
		this->set["parameter_loading_scheme"] = load_settings("parameter_loading_scheme", 1, "int");
		this->set["muscle_model"] = load_settings("muscle_model", 1, "int");
		this->set["amp_change"] = load_settings("amp_change", 1.0, "double");
		this->set["freq_change"] = load_settings("freq_change", 1.0, "double");
		this->set["speed_step_switch"] = load_settings("speed_step_switch", 7, "int");
		this->set["log"] = load_settings("log", 0, "int");
		this->set["extract_video"] = load_settings("extract_video", 0, "int");
		this->set["automatic_systematic_search"] = load_settings("automatic_systematic_search", 0, "int");
		this->set["systematic_search_step_number"] = load_settings("systematic_search_step_number", 10, "int");
		this->set["systematic_search_stepsize_step_number"] = load_settings("systematic_search_stepsize_step_number", 5, "int");
		
		//this->config = load_settings("config", "conf");
		//this->config = "../../"+this->config+"/";
		this->str["config"] = "../../"+load_settings("config", "conf")+"/";
		this->str["systematic_control_rule_file"] = load_settings("systematic_control_rule_file","systematic_control_rule.txt");
		this->str["cpg"] = load_settings("cpg","active");
		this->str["experiment"] = load_settings("experiment", "fullReflex");
		this->str["launching_gate"] = load_settings("launching_gate", "1.3");
		this->str["modeltype"] = load_settings("modeltype","M1");
		this->str["experimenttype"] = load_settings("experimenttype","flat");
		this->str["extract"] = load_settings("extract","off");
		this->str["extractoutput"] = load_settings("extractoutput","output.txt");
		this->str["worldname"] = load_settings("worldname","base.wbt");
		this->str["feedback2study"] = load_settings("feedback2study","");
		this->set["force"] = load_settings("force",0.0,"double");
		this->str["video_name"] = load_settings("video_name", "video");
		this->jobName = "opti";
	}
	/**
	 * 
	 * Settings loaded from the settings file (used by webots without the optimization framework)
	 * 
	 * */
	else{
		init("settings.xml");
	}

}
Settings::Settings(std::string str){
	init(str);
}
void Settings::init(std::string str){
	cout << "loading settings from file" << endl;
	using boost::property_tree::ptree;
	ptree pt;
	read_xml("../../conf/settings/"+str, pt);
	this->set["number_of_repeat"] = pt.get("settings.number_of_repeat", 1);
	this->set["muscle_activation_noise_max"] = pt.get("settings.muscle_activation_noise_max", 0.2);
	this->set["randomized_initial_condition"] = pt.get("settings.randomized_initial_condition", 0);
	this->set["max_fall_time"] = pt.get("settings.max_fall_time", 5);
	this->set["number_of_cycles"] = pt.get("settings.number_of_cycles", 50); // only for optimization_scheme 0
	this->set["energylimit"] = pt.get("settings.energylimit", 5000.0); // only for optimization scheme 1
	this->set["angle_transfer_function"] = pt.get("settings.angle_transfer_function", 0); // 0: geyer's one, 1: florin's one
	this->set["optimization_scheme"] = pt.get("settings.optimization_scheme", 0); //  0: number of step, 1: energy
	this->set["theta|trunkref"] = pt.get("settings.theta|trunkref", 0.105); //  0: number of step, 1: energy
	this->set["backward"] = pt.get("settings.backward", 0); //  0: number of step, 1: energy
	this->set["save_for_matlab"] = pt.get("settings.save_for_matlab", 0);
	this->set["rescale_parameters"] = pt.get("settings.rescale_parameters", 0);
	this->set["muscle_activation_noise"] = pt.get("settings.muscle_activation_noise", 0);
	this->set["distance_max"] = pt.get("settings.distance_max", 40.0);
	this->set["duration_max"] = pt.get("settings.duration_max", 32.0);
	this->set["parameter_loading_scheme"] = pt.get("settings.parameter_loading_scheme", 1);
	this->set["muscle_model"] = pt.get("settings.muscle_model", 1);
	this->set["amp_change"] = pt.get("settings.amp_change", 1.0);
	this->set["freq_change"] = pt.get("settings.freq_change", 1.0);
	this->set["speed_step_switch"] = pt.get("settings.speed_step_switch", 7);
	this->set["log"] = pt.get("settings.log", 0);
	this->set["systematic_search_step_number"] = pt.get("settings.systematic_search_step_number", 10);
	this->set["systematic_search_stepsize_step_number"] = pt.get("settings.systematic_search_stepsize_step_number", 5);
	
	this->jobName = pt.get("settings.job_name", "no opti");
	

	this->str["config"] = "../../"+pt.get("settings.config", "conf")+"/";
	this->str["systematic_control_rule_file"] = pt.get("settings.systematic_control_rule_file","systematic_control_rule.txt");
	this->str["cpg"] = pt.get("settings.cpg","passive");

	this->str["experiment"] = pt.get("settings.experiment","fullReflex");
	this->str["launching_gate"] = pt.get("settings.launching_gate", "1.3");
	
	this->str["modeltype"] = pt.get("settings.modeltype","M1");
	this->str["experimenttype"] = pt.get("settings.experimenttype","flat");
	this->str["extract"] = pt.get("settings.extract","off");

	this->str["extractoutput"] = pt.get("settings.extractoutput","output.txt");
	this->str["worldname"] = pt.get("settings.worldname","base.wbt");
	
	this->str["feedback2study"] = pt.get("settings.feedback2study","");
	
	this->set["force"] = pt.get("settings.force",0.0);
	
	this->set["extract_video"] = pt.get("settings.extract_video", 0);
	this->set["automatic_systematic_search"] = pt.get("settings.automatic_systematic_search", 0);
	this->str["video_name"] = pt.get("settings.video_name", "video");

	
	
}
