#include "types.h"
#include <array>

static std::array<std::string,PARAMETERS_GEYER::NUMBER> PARAMETERS_GEYER_NAMES = {
"solsol_wf",
"tata_wl",
"tata_wl_st",
"tata_wl_sw",
"solta_wf",
"gasgas_wf",
"vasvas_wf",
"hamham_wf",
"gluglu_wf",
"hfhf_wl",
"hamhf_wl",
"ta_bl",
"ta_bl_sw",
"ta_bl_st",
"hf_bl",
"ham_bl",
"kphiknee",
"kphiknee_off",
"kbodyweight",
"kp",
"kd",
"deltas",
"sol_activitybasal",
"ta_activitybasal",
"gas_activitybasal",
"vas_activitybasal",
"ham_activitybasal",
"glu_activitybasal",
"hf_activitybasal",

"kp_ham",
"kd_ham",
"kref_ham",

"kp_glu",
"kd_glu",
"kref_glu",

"kp_hf",
"kd_hf",
"kref_hf",

"hipkp_hf",
"hipkd_hf",

"hipkp_glu",
"hipkd_glu",

"kref_hip",

"kneekp_vas",
"kneekd_vas",
"kref_knee",

"sol_swing_activitybasal",
"ta_swing_activitybasal",
"gas_swing_activitybasal",
"vas_swing_activitybasal",
"ham_swing_activitybasal",
"glu_swing_activitybasal",
"hf_swing_activitybasal",

"sol_stance_activitybasal",
"ta_stance_activitybasal",
"gas_stance_activitybasal",
"vas_stance_activitybasal",
"ham_stance_activitybasal",
"glu_stance_activitybasal",
"hf_stance_activitybasal",

"klean",
"theta|trunkref",

"stance_end",
"swing_end",

"deltas_vas",
"deltas_glu",
"deltas_hf",

"freq"
}