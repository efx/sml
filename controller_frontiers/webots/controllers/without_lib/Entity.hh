#ifndef __ENTITY_HH__
#define __ENTITY_HH__

/*!
 * \file Entity.hh
 * \brief Entity definiton (base class of all neurons) and the different connection type
 * \author efx
 * \version 0.1
 */

#include <vector>
#include "RungeKutta.hh"
#include <string>
#include "Rob.hh"
#include "State.hh"
using namespace std;
extern State state;

class Entity{
protected:
	double state;
public:
	string side;
	string active_during;
	Entity():state(0.0),side("left_right"),active_during("cycle"){};
	Entity(double state):state(state),side("left_right"),active_during("cycle"){};
	Entity(string side):state(0.0),side(side),active_during("cycle"){};
	Entity(string side, double state):state(state),side(side),active_during("cycle"){};
	Entity(string side, string active_during):state(0.0),side(side),active_during(active_during){};
	Entity(string side, string active_during, double state):state(state),side(side),active_during(active_during){};
	virtual double get(){return state;}
	virtual void set(double state ){this->state = state;}
	virtual void add(double state )=0;
	virtual void apply() = 0;
	virtual void apply(double o, double a) = 0;
	virtual string activeDuring(){ return active_during;}
};


class Connection{
public:
	Entity * input;
	Entity * output;
	double dt;
	double weight;
	double value;
	double amplitude;
	double offset;
	Connection(Entity * input, Entity * output): input(input), output(output), dt(0.001),weight(1.0),amplitude(1.0),offset(0.0){value = output->get();};
	
	virtual void step() = 0;
	virtual double getWeight(){
		return weight;
	}
	virtual void setWeight(double weight){
		this->weight=weight;
	}
	virtual bool isGateOpen(Rob * body){
		return body->isGateOpen((Interneuron *)input);
	}
	void reset(){
		weight = 1.0;
		amplitude = 1.0;
		offset = 0.0;
	}
};


class NoDelayConnection : public Connection{
public:
	NoDelayConnection( Entity * input, Entity * output): Connection(input,output){};
	void step(){
		//output->add(weight*input->get());
		output->add(amplitude*weight*input->get()+offset);
	}
};

class FirstOrderConnection : public Connection, public Derivatives{
private:
	double value;

public:
	FirstOrderConnection( Entity * input, Entity * output): Connection(input,output){};
	void step();
	vector<double> dydt(double x, vector<double> y, vector<double> param, vector<double> input);
};

#endif