#ifndef __State_HH__
#define __State_HH__
#include<map>
//---------------------------------------------------------

/*!
 * \file State.hh
 * \brief Robot states
 * \author efx
 * \version 0.1
 */

class State {
public:
	State() : 
		repeat(0), 
		nb_knee_sign_changed(0),
		position(0.0), 
		old_position(0.0), 
		height(0.0), 
		distance(0.0), 
		meanSpeed(0.0), 
		old_meanSpeed(0.0), 
		inSpeed(0.0),
		meanAcceleration(0.0), 
		inAcceleration(0.0), 
		energy(0.0), 
		denergy(0.0), 
		energyW(0.0), 
		energyM(0.0), 
		energy_overextension(0.0),
		nb_fall(0.0), 
		nb_cycle(0),
		steplengthold_position(0.0),
		steplength(0.0),
		steplength_old(0.0),
		step_to_steady_state(0),
		duration(0.0),
		stepdurationold_position(0.0),
		stepduration(0.0),
		stepduration_old(0.0),
		initial_position(0.0),
		muscle_noise(0.001), 
		stay_in_loop(true), 
		is_in_lift_mode(false),
		is_in_launching_phase(true),
		right_cpg_loaded(false),
		left_cpg_loaded(false),
		right_up_efx_parameters(false),
		left_up_efx_parameters(false),
		steady_state(false),
		stop_reason(0),
		wait_for_steady_state(false),
		stabilized_confidence(0.0),
		steady_after_step(0),
		doublestance_duration(0.0),
		total_doublestance_duration(0.0) {}
	State(double d, double e, int n_f, int n_c, double du) : distance(d), energy(e), nb_fall(n_f), nb_cycle(n_c), duration(du) {}
	//State & operator+=( const State &other );
	State(const State &state, double position);
	State operator+ (State const& other);
	int	repeat;
	int	nb_knee_sign_changed;
	double	position;
	double	old_position;
	double	height;
	double	distance;
	double  meanSpeed;
	double	old_meanSpeed;
	double  inSpeed;
	double	meanAcceleration;
	double  inAcceleration;
	double	energy; // total energy
	double 	denergy;
	double	energyW;
	double	energyM;
    double energy_overextension;
	int	nb_fall;
	int	nb_cycle;
	double  steplengthold_position;
	double steplength;
	double steplength_old;
	int 	step_to_steady_state;
	double	duration;
	double 	stepdurationold_position;
	double 	stepduration;
	double 	stepduration_old;
	double	initial_position;
	double	muscle_noise;
	bool	stay_in_loop;
	bool	is_in_lift_mode;
	bool 	is_in_launching_phase;
	bool	right_cpg_loaded;
	bool	left_cpg_loaded;
	bool 	right_up_efx_parameters;
	bool	left_up_efx_parameters;
	
	bool	steady_state;
	int 	stop_reason;
	bool	wait_for_steady_state;
	double 	stabilized_confidence;
	int	steady_after_step;
	
	double last_force_y;
	double last_force_z;
	double last_force_pos;
	double llast_force_y;
	double llast_force_z;
	double llast_force_pos;
	bool falled;
	double doublestance_duration;
	double total_doublestance_duration;
	double doublestance_duration_mean;
	double doublestance_duration_number;
	std::map<std::string, double> getMap();
};

//--------------------------------------------------------------------------------------------------------------
#endif
