#include "Rob.hh"
#include "Interneuron.hh"
#include "Motoneuron.hh"
#include <boost/xpressive/xpressive.hpp>
using namespace std;
using namespace boost::xpressive;
// generate a random number in the range [0;1]
double rnd(){
	return (double)rand()/RAND_MAX;
}
/*
 * v *oid Rob::show_everything(){
 * // Joints
 * for(auto& kv joints)
 * {
 *	 cout << kv.first << " " kv.second->reference_angle << endl;
 }
 //
 
 }
 */
void Rob::initialise_constant()
{
	fixedViewPoint = false;
	cpg_right_loaded = false;
	cpg_left_loaded = false;
	feedback_right_loaded = false;
	feedback_left_loaded = false;
	flag = false;
	name = "";
	name2 = "REGIS";
	dt = double(time_step)/1000.0;
}
void Rob::initialise_joints()
{
	//---------------------------------------------------------------
	//get and enable the servo
	//-----------------------------------------------------------------
	//get the initial angles of servos
	double direction = 0.0;
	double alpha     = 0.0;
	double initial_theta_trunk;
	//theta trunk
	
	initial_theta_trunk = - this->getFromDef(this->name2)->getField("rotation")->getSFRotation()[5];
	theta_trunk_ref = 0.0; //theta_trunk_ref = parameters_geyer["kref"];
	trunk = new Trunk(initial_theta_trunk,theta_trunk_ref, parameters_controlLayer->set["parameters"]["trunk_ref"]);
	
	double reference_phi_hip_right;
	double reference_phi_knee_right;
	double reference_phi_ankle_right;
	double reference_phi_hip_left;
	double reference_phi_knee_left;
	double reference_phi_ankle_left;
	//hip sagittal right
	reference_phi_hip_right = 3.14;
	//knee right
	reference_phi_knee_right = 3.14;
	
	//ankle right
	reference_phi_ankle_right =  3.14/2.0;
	
	//hip sagittal left
	reference_phi_hip_left = 3.14;
	
	//knee left
	reference_phi_knee_left = 3.14;
	
	//ankle left
	reference_phi_ankle_left =  3.14/2.0;
	
	double phi_hip_min   = 20.0  * (2.0*3.14/360.0);
	double phi_knee_min  = 45.0  * (2.0*3.14/360.0);
	double phi_ankle_min = 70.0  * (2.0*3.14/360.0);
	double phi_hip_max   = 230.0 * (2.0*3.14/360.0);
	double phi_knee_max  = 175.0 * (2.0*3.14/360.0);
	double phi_ankle_max = 130.0 * (2.0*3.14/360.0);
	
	Joint * joint;
	
	//HIPS
	joint = new Joint(this->name+"HIP_RIGHT", * getServo(this->name+"HIP_RIGHT"), time_step, reference_phi_hip_right, phi_hip_min, phi_hip_max, this);
	joints[this->name+"HIP_RIGHT"] = joint;
	joint = new Joint(this->name+"HIP_LEFT", * getServo(this->name+"HIP_LEFT"), time_step, reference_phi_hip_left, phi_hip_min, phi_hip_max, this);
	joints[this->name+"HIP_LEFT"] = joint;
	//KNEES
	joint = new Joint(this->name+"KNEE_RIGHT", * getServo(this->name+"KNEE_RIGHT"), time_step, reference_phi_knee_right, phi_knee_min, phi_knee_max, this);
	joints[this->name+"KNEE_RIGHT"] = joint;
	joint = new Joint(this->name+"KNEE_LEFT", * getServo(this->name+"KNEE_LEFT"), time_step, reference_phi_knee_left, phi_knee_min, phi_knee_max, this);
	joints[this->name+"KNEE_LEFT"] = joint;
	//ANKLES
	joint = new Joint(this->name+"ANKLE_RIGHT", * getServo(this->name+"ANKLE_RIGHT"), time_step, reference_phi_ankle_right, phi_ankle_min, phi_ankle_max, this);
	joints[this->name+"ANKLE_RIGHT"] = joint;
	joint = new Joint(this->name+"ANKLE_LEFT", * getServo(this->name+"ANKLE_LEFT"), time_step, reference_phi_ankle_left, phi_ankle_min, phi_ankle_max, this);
	joints[this->name+"ANKLE_LEFT"] = joint;
	
	
	soft_limit_stiffness = 17.19; //[Nm/rad] = 0.3[Nm/deg]
}
// buggy
void Rob::reinitializeRobotPosition(){
	//this->set_position(this->name2);
	for(auto& kv: this->initial_translation){
		this->set_position(kv.first);
	}
	this->simulationPhysicsReset();
}
// buggy
void Rob::initialise_position(){
	double noisy_IC = 0;
	//this->reinitializeRobotPosition();
	if(boost::any_cast<int>(Settings::set["randomized_initial_condition"]) == 1){
		noisy_IC = -0.05 + rnd()/10;
		cout << "noise : " << noisy_IC;
		if(round(rnd()) == 0){
			//getServo(this->name+"HIP_RIGHT")->setPosition(noisy_IC);
			this->setHipRot("right", noisy_IC);
			cout << " on the right limb servo IC" << endl;
		}
		else{
			//getServo(this->name+"HIP_LEFT")->setPosition(noisy_IC);
			this->setHipRot("left", noisy_IC);
			cout << " on the left limb servo IC" << endl;
		}
	}
	// save field position
	this->save_field(this->name2);
	this->save_field(this->name+"HIP_LEFT");
	this->save_field(this->name+"KNEE_LEFT");
	this->save_field(this->name+"ANKLE_LEFT");
	this->save_field(this->name+"HIP_RIGHT");
	this->save_field(this->name+"KNEE_RIGHT");
	this->save_field(this->name+"ANKLE_RIGHT");
}
// buggy
void Rob::set_position(std::string field_name){
	Node * node = this->getFromDef(field_name);
	Field * transField = node->getField("translation");
	Field * rotField = node->getField("rotation");
	double rot[4] = {0,0,0,0};
	double trans[3] = {0,0,0};
	//const double INITIAL_TRANS[3] = { 0, 1.53, 0 };
	//const double INITIAL_ROT[4] = { 1, 0, 0, 0.2 };
	for(int i=0;i<4;i++)
		rot[i] = this->initial_rotation[field_name][i];
	for(int i=0;i<3;i++)
		trans[i] = this->initial_translation[field_name][i];
	transField->setSFVec3f(trans);
	rotField->setSFRotation(rot);
}
// buggy
void Rob::save_field(std::string field_name){
	Node * node = this->getFromDef(field_name);
	Field * transField = node->getField("translation");
	Field * rotField = node->getField("rotation");
	this->initial_translation[field_name] = (double *)transField->getSFVec3f();
	this->initial_rotation[field_name] = (double *)rotField->getSFRotation();
}
void Rob::initialise_communication(){
	// -----------------------------------------------------------
	//get and enable receiver
	receiver = getReceiver("RECEIVER");
	receiver ->enable(time_step);
	emitter  = getEmitter("EMITTER");
	emitter_matlab = getEmitter("EMITTER_MATLAB");
	//	emitter_analyzer = getEmitter("EMITTER_ANALYZER");
}

void Rob::initialise_parameters(Parameters * parameters){
		parameters_geyer = parameters->set["parameters"];
	//------------------------------------------------------------------
}

void Rob::initialise_muscles(){
	//initialise muscle
	
	//optimization::Webots &opti = optimization::Webots::Instance();
	//l_slack[m], l_opt[m], v_max[l_opt/s], mass[kg], pennation[rad] , r0[m], joint, 
	bool clockwise = true;
	bool cclockwise = false;
	
	double r_SOL       = 0.05 ;
	double r_TA        = 0.04 ;
	double r_GAS_ankle = 0.05 ;
	double r_GAS_knee  = 0.05 ;
	double r_VAS       = 0.06 ;
	double r_HAM_knee  = 0.05 ;
	double r_HAM_hip   = 0.08 ;
	double r_GLU       = 0.10 ;
	double r_HF        = 0.10 ;
	
	double phi_max_SOL       = 110.0 * (2.0*3.14/360.0);
	double phi_max_TA        = 80.0  * (2.0*3.14/360.0);
	double phi_max_GAS_ankle = 110.0 * (2.0*3.14/360.0);
	double phi_max_GAS_knee  = 140.0 * (2.0*3.14/360.0);
	double phi_max_VAS       = 165.0 * (2.0*3.14/360.0);
	double phi_max_HAM_knee  = 180.0 * (2.0*3.14/360.0);
	
	double phi_ref_SOL       = 80.0  * (2.0*3.14/360.0);
	double phi_ref_TA        = 110.0 * (2.0*3.14/360.0);
	double phi_ref_GAS_ankle = 80.0  * (2.0*3.14/360.0);
	double phi_ref_GAS_knee  = 165.0 * (2.0*3.14/360.0);
	double phi_ref_VAS       = 125.0 * (2.0*3.14/360.0);
	double phi_ref_HAM_knee  = 180.0 * (2.0*3.14/360.0);
	double phi_ref_HAM_hip   = 155.0 * (2.0*3.14/360.0);
	double phi_ref_GLU       = 150.0 * (2.0*3.14/360.0);
	double phi_ref_HF        = 180.0 * (2.0*3.14/360.0);
	
	Muscle  * m;
	
	/**
	 * 
	 * clockwise muscle will have a flag -1 
	 * 
	 */
	
	string name;
	//double fmax[7] = {2000, 1500, 6000, 3000, 1500, 4000, 800};
	double l_opts[7] = {0.11, 0.11, 0.08, 0.10, 0.05, 0.04, 0.06};
	double l_slacks[7] = {0.10, 0.13, 0.23, 0.31, 0.40, 0.26, 0.24};
	double v_maxes[7] = {12.0, 12.0, 12.0, 12.0, 12.0, 6.0, 12.0};
	double masses[7] = {1.87, 1.4, 2.91, 1.82, 0.45, 1.36, 0.29};
	

	name = "hf";
	m = new Muscle(SIDE::LEFT,name,l_slacks[0], l_opts[0], v_maxes[0], masses[0],  0.5, cclockwise, joints["HIP_LEFT"],r_HF, phi_ref_HF, 0.0,0.5); //hf
	muscles["left_hf"] = m;
	m = new Muscle(SIDE::RIGHT,name,l_slacks[0], l_opts[0], v_maxes[0], masses[0],  0.5, cclockwise, joints["HIP_RIGHT"],r_HF, phi_ref_HF, 0.0,0.5); //hf
	muscles["right_hf"] = m;
	
	name = "glu";	
	m = new Muscle(SIDE::LEFT,name,l_slacks[1], l_opts[1], v_maxes[1], masses[1],  0.5, clockwise, joints["HIP_LEFT"],-r_GLU, phi_ref_GLU, 0.0,0.5); //glu
	muscles["left_glu"] = m;
	m = new Muscle(SIDE::RIGHT,name,l_slacks[1], l_opts[1], v_maxes[1], masses[1],  0.5, clockwise, joints["HIP_RIGHT"],-r_GLU, phi_ref_GLU, 0.0,0.5); //glu
	muscles["right_glu"] = m;
	
	name = "vas";
	m = new Muscle(SIDE::LEFT,name,l_slacks[2], l_opts[2], v_maxes[2], masses[2],  0.7, cclockwise, joints["KNEE_LEFT"], -r_VAS, phi_ref_VAS, phi_max_VAS,0.5); //vas
	muscles["left_vas"] = m;
	m = new Muscle(SIDE::RIGHT,name,l_slacks[2], l_opts[2], v_maxes[2], masses[2],  0.7, cclockwise, joints["KNEE_RIGHT"], -r_VAS, phi_ref_VAS, phi_max_VAS,0.5); //vas
	muscles["right_vas"] = m;
	
	name = "ham";
	m = new PosturalMuscle(SIDE::LEFT, name, l_slacks[3], l_opts[3], v_maxes[3], masses[3],  0.7, clockwise, joints["HIP_LEFT"],-r_HAM_hip, phi_ref_HAM_hip, 0.0, joints["KNEE_LEFT"], r_HAM_knee, phi_ref_HAM_knee, phi_max_HAM_knee,0.44);//ham
	muscles["left_ham"] = m;
	m = new PosturalMuscle(SIDE::RIGHT, name, l_slacks[3], l_opts[3], v_maxes[3], masses[3],  0.7, clockwise, joints["HIP_RIGHT"],-r_HAM_hip, phi_ref_HAM_hip, 0.0, joints["KNEE_RIGHT"], r_HAM_knee, phi_ref_HAM_knee, phi_max_HAM_knee,0.44);//ham
	muscles["right_ham"] = m;
	
	name = "gas";
	m = new PosturalMuscle(SIDE::LEFT,name,l_slacks[4], l_opts[4], v_maxes[4], masses[4],  0.7,clockwise, joints["ANKLE_LEFT"],-r_GAS_ankle, phi_ref_GAS_ankle, phi_max_GAS_ankle, joints["KNEE_LEFT"], r_GAS_knee, phi_ref_GAS_knee, phi_max_GAS_knee,0.54);//gas
	muscles["left_gas"] = m;
	m = new PosturalMuscle(SIDE::RIGHT,name,l_slacks[4], l_opts[4], v_maxes[4], masses[4],  0.7,clockwise, joints["ANKLE_RIGHT"],-r_GAS_ankle, phi_ref_GAS_ankle, phi_max_GAS_ankle, joints["KNEE_RIGHT"], r_GAS_knee, phi_ref_GAS_knee, phi_max_GAS_knee,0.54);//gas
	muscles["right_gas"] = m;
	
	name = "sol";
	m = new Muscle(SIDE::LEFT,name,l_slacks[5], l_opts[5], v_maxes[5], masses[5],  0.5,clockwise, joints["ANKLE_LEFT"],-r_SOL, phi_ref_SOL, phi_max_SOL,0.81); //sol
	muscles["left_sol"] = m;
	m = new Muscle(SIDE::RIGHT,name,l_slacks[5], l_opts[5], v_maxes[5], masses[5],  0.5,clockwise, joints["ANKLE_RIGHT"],-r_SOL, phi_ref_SOL, phi_max_SOL,0.81); //sol
	muscles["right_sol"] = m;
	
	name = "ta";
	m = new Muscle(SIDE::LEFT,name,l_slacks[6], l_opts[6], v_maxes[6], masses[6],  0.7,cclockwise, joints["ANKLE_LEFT"],r_TA, phi_ref_TA, phi_max_TA,0.7); //ta
	muscles["left_ta"] = m;
	m = new Muscle(SIDE::RIGHT,name,l_slacks[6], l_opts[6], v_maxes[6], masses[6],  0.7,cclockwise, joints["ANKLE_RIGHT"],r_TA, phi_ref_TA, phi_max_TA,0.7); //ta
	muscles["right_ta"] = m;
	
}
void Rob::initialise_motoneurons(){
	Motoneuron * mn;
	mn = new Motoneuron("left");
	motoneurons["left_sol"] = mn;
	mn = new Motoneuron("left");
	motoneurons["left_ta"] = mn;
	mn = new Motoneuron("left");
	motoneurons["left_gas"] = mn;
	mn = new Motoneuron("left");
	motoneurons["left_vas"] = mn;
	mn = new Motoneuron("left");
	motoneurons["left_ham"] = mn;
	mn = new Motoneuron("left");
	motoneurons["left_glu"] = mn;
	mn = new Motoneuron("left");
	motoneurons["left_hf"] = mn;
	
	mn = new Motoneuron();
	motoneurons["right_sol"] = mn;
	mn = new Motoneuron();
	motoneurons["right_ta"] = mn;
	mn = new Motoneuron();
	motoneurons["right_gas"] = mn;
	mn = new Motoneuron();
	motoneurons["right_vas"] = mn;
	mn = new Motoneuron();
	motoneurons["right_ham"] = mn;
	mn = new Motoneuron();
	motoneurons["right_glu"] = mn;
	mn = new Motoneuron();
	motoneurons["right_hf"] = mn;
	
}

void Rob::initialise_sensors(){
	/**
	 * Ground sensors (left_foot , right_foot sensors)
	 */
	hdisttocm_right = 0.0;
	hdisttocm_left = 0.0;
	vdisttocm_right = 0.0;
	vdisttocm_left = 0.0;
	BioTouchSensor * sensor_heel;
	BioTouchSensor * sensor_toe;
	sensor_heel = new BioTouchSensor(* getTouchSensor(this->name+"SENSOR_HEEL_LEFT"),time_step);
	sensor_toe = new BioTouchSensor(* getTouchSensor(this->name+"SENSOR_TOE_LEFT"),time_step);
	left_foot = new Foot(sensor_heel,sensor_toe,"LEFT",parameters_controlLayer->set["parameters"]["offset_change_reflex"]);
	sensor_heel = new BioTouchSensor(* getTouchSensor(this->name+"SENSOR_HEEL_RIGHT"),time_step);
	sensor_toe = new BioTouchSensor(* getTouchSensor(this->name+"SENSOR_TOE_RIGHT"),time_step);
	right_foot = new Foot(sensor_heel,sensor_toe,"RIGHT",parameters_controlLayer->set["parameters"]["offset_change_reflex"]);	
	
	sensors["left_foot"] = left_foot;
	sensors["right_foot"] = right_foot;
	
	/**
	 * Muscle sensors
	 */
	MuscleLengthSensor * mls;
	MuscleForceSensor * mfs;
	zero_length_offset = 0.0;
	one_gain = 1.0;
	for(auto& kv: muscles){
		sregex rex = sregex::compile( "([^_]*)_(.*)" );
		smatch mat;
		if( regex_match ( kv.first, mat, rex))
		{
			mfs = new MuscleForceSensor(kv.second, one_gain);
			auto it = parameters_geyer.find(mat[2]+"_bl");
			if(it != parameters_geyer.end())
				mls = new MuscleLengthSensor(kv.second, it->second, one_gain);
			else
				mls = new MuscleLengthSensor(kv.second, zero_length_offset, one_gain);
			sensors[kv.first+"_length"] = mls;
			sensors[kv.first+"_force"] = mfs;
		}
	}
	
	/**
	 * Trunk sensors (stability sensors)
	 */
	TrunkPositionSensor1 * tps1;
	tps1 = new TrunkPositionSensor1(trunk, parameters_geyer["kp"], parameters_geyer["kd"]);
	sensors["trunk_pd"] = tps1;
	
	TrunkPositionSensor2 * tps2;
	tps2 = new TrunkPositionSensor2(trunk, parameters_geyer["kp"], parameters_geyer["kd"]);
	sensors["trunk_pd_0.68"] = tps2;
	TrunkTakeOffSensorOld * tts;
	tts = new TrunkTakeOffSensorOld(trunk);
	sensors["trunk_leak"] = tts;
	
	/**
	 * Angle sensors for the knee
	 */
	phi_knee_off    = 2.97;
	AngleSensor * as;
	as = new AngleSensor(joints[this->name+"KNEE_RIGHT"], parameters_geyer["kphiknee_off"]);
	sensors["right_knee_angle"] = as;
	as = new AngleSensor(joints[this->name+"KNEE_LEFT"], parameters_geyer["kphiknee_off"]);
	sensors["left_knee_angle"] = as;
	
	
}
void Rob::initialise_feedbacks(Parameters * parameters, string side){
	string cside;
	Foot *foot;
	std::map<std::string, Entity *> *feedbacks;
	if(side == "left"){
		cside = "right";
		foot = left_foot;
		feedbacks = &feedbacks_left;
	}
	else{
		cside = "left";
		foot = right_foot;
		feedbacks = &feedbacks_right;
	}
	//CstInterneuron * cs;
	SensoryInterneuron * ss;
	SensorySensoryInterneuron * sss;
	
	
	/** sol */
	ss = new SensoryInterneuron(foot,"stance",parameters->set["parameters"],sensors[side+"_sol_force"], motoneurons[side+"_sol"], parameters_geyer["solsol_wf"]);
	(* feedbacks)[side+"_sol__mff_stance"] = ss;
	
	/** sol */
	ss = new SensoryInterneuron(right_foot,"stance",parameters->set["parameters"],sensors[side+"_sol_force"], motoneurons[side+"_sol"], parameters_geyer["solsol_wf"]);
	(* feedbacks)[side+"_sol__mff_stance"] = ss;
	/** ta */
	ss = new SensoryInterneuron(right_foot,"cycle",parameters->set["parameters"],sensors[side+"_ta_length"], motoneurons[side+"_ta"], parameters_geyer["tata_wl"]);
	(* feedbacks)[side+"_ta__mlf_cycle"] = ss;
	ss = new SensoryInterneuron(right_foot,"stance",parameters->set["parameters"],sensors[side+"_sol_force"], motoneurons[side+"_ta"], parameters_geyer["solta_wf"],-1.0);
	(* feedbacks)[side+"_sol_ta_mff_stance"] = ss;
	/** gas */
	ss = new SensoryInterneuron(right_foot,"stance",parameters->set["parameters"],sensors[side+"_gas_force"], motoneurons[side+"_gas"], parameters_geyer["gasgas_wf"]);
	(* feedbacks)[side+"_gas__mff_stance"] = ss;
	/** vas */
	ss = new SensoryInterneuron(right_foot,"stance",parameters->set["parameters"],sensors[side+"_vas_force"], motoneurons[side+"_vas"], parameters_geyer["vasvas_wf"]);
	(* feedbacks)[side+"_vas__mff_stance"] = ss;
	ss = new SensoryInterneuron(right_foot,"angleoffset",parameters->set["parameters"],sensors[side+"_knee_angle"], motoneurons[side+"_vas"], parameters_geyer["kphiknee"],-1.0);
	(* feedbacks)[side+"_vas_pko_angleoffset"] = ss;
	ss = new SensoryInterneuron(right_foot,"stance_end",parameters->set["parameters"],sensors[cside+"_foot"], motoneurons[side+"_vas"], parameters_geyer["kbodyweight"],-1.0);
	(* feedbacks)[side+"_vas_gcf_finishingstance"] = ss;
	/** ham */
	sss = new SensorySensoryInterneuron(right_foot,"stance",parameters->set["parameters"],sensors[side+"_foot"], sensors["trunk_pd"], motoneurons[side+"_ham"], parameters_geyer["kbodyweight"]);
	(* feedbacks)[side+"_ham_gif_stance"] = sss;
	ss = new SensoryInterneuron(right_foot,"swing",parameters->set["parameters"],sensors[side+"_ham_force"], motoneurons[side+"_ham"], parameters_geyer["hamham_wf"]);
	(* feedbacks)[side+"_ham__mff_swing"] = ss;
	/** glu */
	sss = new SensorySensoryInterneuron(right_foot,"stance",parameters->set["parameters"],sensors[side+"_foot"], sensors["trunk_pd_0.68"], motoneurons[side+"_glu"], parameters_geyer["kbodyweight"]);
	(* feedbacks)[side+"_glu_gif_stance"] = sss;
	ss = new SensoryInterneuron(right_foot,"swing",parameters->set["parameters"],sensors[side+"_glu_force"], motoneurons[side+"_glu"], parameters_geyer["gluglu_wf"]);
	(* feedbacks)[side+"_glu__mff_swing"] = ss;
	/** hf */
	sss = new SensorySensoryInterneuron(right_foot,"stance",parameters->set["parameters"],sensors[side+"_foot"], sensors["trunk_pd"], motoneurons[side+"_hf"], parameters_geyer["kbodyweight"],-1.0);
	(* feedbacks)[side+"_hf_gif_stance"] = sss;
	ss = new SensoryInterneuron(right_foot,"swing",parameters->set["parameters"],sensors[side+"_hf_length"], motoneurons[side+"_hf"], parameters_geyer["hfhf_wl"]);
	(* feedbacks)[side+"_hf__mlf_swing"] = ss;
	ss = new SensoryInterneuron(right_foot,"swing",parameters->set["parameters"],sensors[side+"_ham_length"], motoneurons[side+"_hf"], parameters_geyer["hamhf_wl"],-1.0);
	(* feedbacks)[side+"_ham_hf_mlf_swing"] = ss;
	ss = new SensoryInterneuron(right_foot,"swing",parameters->set["parameters"],sensors["trunk_leak"], motoneurons[side+"_hf"], parameters_geyer["klean"]);
	(* feedbacks)[side+"_hf_tl_swing"] = ss;
}

void Rob::initialise_feedbacks(Parameters * parameters){
	//SensorySensoryInterneuron * sss;
	
	initialise_feedbacks(parameters,"right");
	initialise_feedbacks(parameters,"left");
	
	
	
	loadPassiveFeedback();
}
void Rob::initialise_cpgs(Parameters * parameters){
	/**
	 * 
	 * CPG
	 * 
	 *  */
	CPGInterneuron * cs;
	
	/*
	 * load interneurons to be modelized
	 */
	std::vector<std::string> strs; // the vector containing the key->value pair
	
	std::map<std::string, double> modelized_sensory_interneurons_amplitude;
	ifstream myfile (Settings::str["config"]+"cpg_gate/cpg_interneurons.txt");
	string line ; // the string containing the line
	if (myfile.is_open()){
		while( std::getline( myfile, line ) )
		{
			boost::split(strs, line, boost::is_any_of("\t "));
			
			//cout << strs[0] << endl;
			if(strs[0][0]!='#'){
				modelized_sensory_interneurons.push_back(strs[0]);
				modelized_sensory_interneurons_left.push_back("left_"+strs[0]);
				modelized_sensory_interneurons_right.push_back("right_"+strs[0]);
				if(strs.size()>1){
					modelized_sensory_interneurons_amplitude[strs[0]] = convertToDouble(strs[1]);
				}
				else
					modelized_sensory_interneurons_amplitude[strs[0]] = 1.0;
			}
			else{
				modelized_sensory_interneurons_amplitude[strs[0]] = -1.0;
			}
		}
		myfile.close();
	}
	else{
		cout << "ERROR: unable to open file '" << Settings::str["config"]+"cpg_gate/cpg_interneurons.txt" << "'. In:Parameters.cc, constructor(string, string)"<<endl;
	}
	/**
	 * 
	 * sol (0/1)
	 */
	cs = new CPGInterneuron("stance",parameters,time_step, motoneurons["left_sol"], left_foot, right_foot,"sol__mff_stance",modelized_sensory_interneurons_amplitude["sol__mff_stance"]);
	cpgs_left["left_sol__mff_stance"] = cs;
	cs = new CPGInterneuron("stance",parameters,time_step, motoneurons["right_sol"], right_foot, left_foot,"sol__mff_stance",modelized_sensory_interneurons_amplitude["sol__mff_stance"]);
	cpgs_right["right_sol__mff_stance"] = cs;
	
	/** 
	 * 
	 * ta (0/2)
	 */
	cs = new CPGInterneuron("cycle",parameters,time_step, motoneurons["left_ta"], left_foot, right_foot,"ta__mlf_cycle",modelized_sensory_interneurons_amplitude["ta__mlf_cycle"]);
	cpgs_left["left_ta__mlf_cycle"] = cs;
	cs = new CPGInterneuron("cycle",parameters,time_step, motoneurons["right_ta"], right_foot, left_foot,"ta__mlf_cycle",modelized_sensory_interneurons_amplitude["ta__mlf_cycle"]);
	cpgs_right["right_ta__mlf_cycle"] = cs;
	
	cs = new CPGInterneuron("stance",parameters,time_step, motoneurons["left_ta"], left_foot, right_foot,"sol_ta_mff_stance",modelized_sensory_interneurons_amplitude["sol_ta_mff_stance"]);
	cpgs_left["left_sol_ta_mff_stance"] = cs;
	cs = new CPGInterneuron("stance",parameters,time_step, motoneurons["right_ta"], right_foot, left_foot,"sol_ta_mff_stance",modelized_sensory_interneurons_amplitude["sol_ta_mff_stance"]);
	cpgs_right["right_sol_ta_mff_stance"] = cs;
	
	/** 
	 * 
	 * gas (1/1)
	 */
	cs = new CPGInterneuron("stance",parameters,time_step, motoneurons["left_gas"], left_foot, right_foot,"gas__mff_stance",modelized_sensory_interneurons_amplitude["gas__mff_stance"]);
	cpgs_left["left_gas__mff_stance"] = cs;
	cs = new CPGInterneuron("stance",parameters,time_step, motoneurons["right_gas"], right_foot, left_foot,"gas__mff_stance",modelized_sensory_interneurons_amplitude["gas__mff_stance"]);
	cpgs_right["right_gas__mff_stance"] = cs;
	
	/**
	 * 
	 * vas (2/2) (pko feedback can be maintained as a feedback and the constant input
	 * 				is already not a "reflex")
	 */
	//????? STRANGE WHY INVERSE FOOT ??
	cs = new CPGInterneuron("stance",parameters,time_step, motoneurons["left_vas"], left_foot, right_foot,"vas__mff_stance",modelized_sensory_interneurons_amplitude["vas__mff_stance"]);
	cpgs_left["left_vas__mff_stance"] = cs;
	cs = new CPGInterneuron("stance",parameters,time_step, motoneurons["right_vas"], right_foot, left_foot,"vas__mff_stance",modelized_sensory_interneurons_amplitude["vas__mff_stance"]);
	cpgs_right["right_vas__mff_stance"] = cs;
	
	cs = new CPGInterneuron("angleoffset", parameters,time_step, motoneurons["left_vas"], left_foot, right_foot,"vas_pko_angleoffset",modelized_sensory_interneurons_amplitude["vas_pko_angleoffset"]);
	cpgs_left["left_vas_pko_angleoffset"] = cs;
	cs = new CPGInterneuron("angleoffset", parameters,time_step, motoneurons["right_vas"], right_foot, left_foot,"vas_pko_angleoffset",modelized_sensory_interneurons_amplitude["vas_pko_angleoffset"]);
	cpgs_right["right_vas_pko_angleoffset"] = cs;
	
	cs = new CPGInterneuron("stance_end", parameters,time_step, motoneurons["left_vas"], left_foot, right_foot,"vas_gcf_finishingstance",modelized_sensory_interneurons_amplitude["vas_gcf_finishingstance"]);
	cpgs_left["left_vas_gcf_finishingstance"] = cs;
	cs = new CPGInterneuron("stance_end", parameters,time_step, motoneurons["right_vas"], right_foot, left_foot,"vas_gcf_finishingstance",modelized_sensory_interneurons_amplitude["vas_gcf_finishingstance"]);
	cpgs_right["right_vas_gcf_finishingstance"] = cs;
	
	/**
	 * 
	 * ham (1/2)
	 */
	cs = new CPGInterneuron("stance",parameters,time_step, motoneurons["left_ham"], left_foot, right_foot,"ham_gif_stance",modelized_sensory_interneurons_amplitude["ham_gif_stance"]);
	cpgs_left["left_ham_gif_stance"] = cs;
	cs = new CPGInterneuron("stance",parameters,time_step, motoneurons["right_ham"], right_foot, left_foot,"ham_gif_stance",modelized_sensory_interneurons_amplitude["ham_gif_stance"]);
	cpgs_right["right_ham_gif_stance"] = cs;
	
	cs = new CPGInterneuron("swing",parameters,time_step, motoneurons["left_ham"], left_foot, right_foot,"ham__mff_swing",modelized_sensory_interneurons_amplitude["ham__mff_swing"]);
	cpgs_left["left_ham__mff_swing"] = cs;
	cs = new CPGInterneuron("swing",parameters,time_step, motoneurons["right_ham"], right_foot, left_foot,"ham__mff_swing",modelized_sensory_interneurons_amplitude["ham__mff_swing"]);
	cpgs_right["right_ham__mff_swing"] = cs;
	
	/**
	 * 
	 * glu (1/2)
	 */
	cs = new CPGInterneuron("stance",parameters,time_step, motoneurons["left_glu"], left_foot, right_foot,"glu_gif_stance",modelized_sensory_interneurons_amplitude["glu_gif_stance"]);
	cpgs_left["left_glu_gif_stance"] = cs;
	cs = new CPGInterneuron("stance",parameters,time_step, motoneurons["right_glu"], right_foot, left_foot,"glu_gif_stance",modelized_sensory_interneurons_amplitude["glu_gif_stance"]);
	cpgs_right["right_glu_gif_stance"] = cs;
	
	cs = new CPGInterneuron("swing",parameters,time_step, motoneurons["left_glu"], left_foot, right_foot,"glu__mff_swing",modelized_sensory_interneurons_amplitude["glu__mff_swing"]);
	cpgs_left["left_glu__mff_swing"] = cs;
	cs = new CPGInterneuron("swing",parameters,time_step, motoneurons["right_glu"], right_foot, left_foot,"glu__mff_swing",modelized_sensory_interneurons_amplitude["glu__mff_swing"]);
	cpgs_right["right_glu__mff_swing"] = cs;
	
	/** 
	 * 
	 * hf  (2/4) or (~3/4) 
	 */
	cs = new CPGInterneuron("swing",parameters,time_step, motoneurons["left_hf"], left_foot, right_foot,"hf_tl_swing",modelized_sensory_interneurons_amplitude["hf_tl_swing"]);
	cpgs_left["left_hf_tl_swing"] = cs;
	cs = new CPGInterneuron("swing",parameters,time_step, motoneurons["right_hf"], right_foot, left_foot,"hf_tl_swing",modelized_sensory_interneurons_amplitude["hf_tl_swing"]);
	cpgs_right["right_hf_tl_swing"] = cs;
	
	// works with but walking less natural
	cs = new CPGInterneuron("stance",parameters,time_step, motoneurons["left_hf"], left_foot, right_foot,"hf_gif_stance",modelized_sensory_interneurons_amplitude["hf_gif_stance"]);
	cpgs_left["left_hf_gif_stance"] = cs;
	cs = new CPGInterneuron("stance",parameters,time_step, motoneurons["right_hf"], right_foot, left_foot,"hf_gif_stance",modelized_sensory_interneurons_amplitude["hf_gif_stance"]);
	cpgs_right["right_hf_gif_stance"] = cs;
	
	cs = new CPGInterneuron("swing",parameters,time_step, motoneurons["left_hf"], left_foot, right_foot,"hf__mlf_swing",modelized_sensory_interneurons_amplitude["hf__mlf_swing"]);
	cpgs_left["left_hf__mlf_swing"] = cs;
	cs = new CPGInterneuron("swing",parameters,time_step, motoneurons["right_hf"], right_foot, left_foot,"hf__mlf_swing",modelized_sensory_interneurons_amplitude["hf__mlf_swing"]);
	cpgs_right["right_hf__mlf_swing"] = cs;
	
	cs = new CPGInterneuron("swing",parameters,time_step, motoneurons["left_hf"], left_foot, right_foot,"ham_hf_mlf_swing",modelized_sensory_interneurons_amplitude["ham_hf_mlf_swing"]);
	cpgs_left["left_ham_hf_mlf_swing"] = cs;
	cs = new CPGInterneuron("swing",parameters,time_step, motoneurons["right_hf"], right_foot, left_foot,"ham_hf_mlf_swing",modelized_sensory_interneurons_amplitude["ham_hf_mlf_swing"]);
	cpgs_right["right_ham_hf_mlf_swing"] = cs;
		
	loadPassiveCpg();
}
void Rob::loadPassiveCpg(){
	for(auto& kv : cpgs_right){
		cpgs_name[kv.second]=kv.first;
		cpgs[kv.first] = kv.second;
	}
	for(auto& kv : cpgs_left){
		cpgs_name[kv.second]=kv.first;
		cpgs[kv.first] = kv.second;
	}
}
void Rob::loadPassiveFeedback(){
	for(auto& kv : feedbacks_right){
		feedbacks_name[kv.second]=kv.first;
		feedbacks[kv.first] = kv.second;
	}
	for(auto& kv : feedbacks_left){
		feedbacks_name[kv.second]=kv.first;
		feedbacks[kv.first] = kv.second;
	}
}

/* 
 * load all feedbacks (as defined in initialise_feedbacks) in the interneurones network so that they act on motoneurons
 */
void Rob::loadFeedback(std::string prefix){
	if(prefix.find("right") != string::npos){
		cout << "right feedback_loaded" <<  endl;
		feedback_right_loaded = true;
		for(auto& kv : feedbacks_right){
			interneurones_name[kv.second]=kv.first;
			interneurones[kv.first] = kv.second;
			connections[kv.first] = new NoDelayConnection(kv.second,((Interneuron*)kv.second)->getOutputEntity());
		}
	}
	else if(prefix.find("left") != string::npos)
	{
		cout << "left feedback_loaded" <<  endl;
		feedback_left_loaded = true;
		for(auto& kv : feedbacks_left){
			interneurones_name[kv.second]=kv.first;
			interneurones[kv.first] = kv.second;
			connections[kv.first] = new NoDelayConnection(kv.second,((Interneuron*)kv.second)->getOutputEntity());
		}
	}
}

void Rob::loadCpg(std::string prefix){
	if(prefix.find("right") != string::npos){
		cout << "right cpg_loaded" <<  endl;
		cpg_right_loaded = true;
		for(auto& kv : cpgs_right){
			interneurones_name[kv.second]=kv.first;
			interneurones[kv.first] = kv.second;
			connections[kv.first] = new NoDelayConnection(kv.second,((Interneuron*)kv.second)->getOutputEntity());
		}
	}
	else if(prefix.find("left") != string::npos)
	{
		cout << "left cpg_loaded" <<  endl;
		cpg_left_loaded = true;
		for(auto& kv : cpgs_left){
			interneurones_name[kv.second]=kv.first;
			interneurones[kv.first] = kv.second;
			connections[kv.first] = new NoDelayConnection(kv.second,((Interneuron*)kv.second)->getOutputEntity());
		}
	}
}

void Rob::loadCpg(std::string prefix,std::string prefix2){
	if(prefix.find("right") != string::npos){
		cout << "right cpg_loaded" <<  endl;
		cpg_right_loaded = true;
		for(auto& kv : cpgs_right){
			interneurones_name[kv.second]=kv.first;
			interneurones[kv.first] = kv.second;
			connections[prefix2+"_"+kv.first] = new NoDelayConnection(kv.second,((Interneuron*)kv.second)->getOutputEntity());
		}
	}
	else if(prefix.find("left") != string::npos)
	{
		cout << "left cpg_loaded" <<  endl;
		cpg_left_loaded = true;
		for(auto& kv : cpgs_left){
			interneurones_name[kv.second]=kv.first;
			interneurones[kv.first] = kv.second;
			connections[prefix2+"_"+kv.first] = new NoDelayConnection(kv.second,((Interneuron*)kv.second)->getOutputEntity());
		}
	}
}

/* 
 * replace feedbacks by their cpgs counterpart (for cpgs defined in initialise_cpgs)
 */
void Rob::loadPartialCpg(std::string prefix, std::string prefix2){
	if(prefix.find("right") != string::npos){
		cpg_right_loaded = true;
		cout << "right cpg_loaded" <<  endl;
		for(auto& kv : motoneurons){
			//cout << "elements in " << kv.first << " > " << network[kv.second].size();
			for(auto& kv2 : modelized_sensory_interneurons){
				auto it = cpgs_right.find("right_" + kv2);
				interneurones["right_" + kv2] = it->second;
				interneurones_name[it->second] = "right_" + kv2;
				if(((Interneuron*)it->second)->getOutputEntity() == kv.second)
					connections[prefix2+"_"+it->first] = new NoDelayConnection(it->second,((Interneuron*)it->second)->getOutputEntity());
			}
		}
	}
	else if(prefix.find("left") != string::npos)
	{
		cpg_left_loaded = true;
		cout << "left cpg_loaded" <<  endl;
		for(auto& kv : motoneurons){
			//cout << "elements in " << kv.first << " > " << network[kv.second].size();
			for(auto& kv2 : modelized_sensory_interneurons){
				auto it = cpgs_left.find("left_" + kv2);
				interneurones["left_" + kv2] = it->second;
				interneurones_name[it->second] = "left_" + kv2;
				if(((Interneuron*)it->second)->getOutputEntity() == kv.second)
					connections[prefix2+"_"+it->first] = new NoDelayConnection(it->second,((Interneuron*)it->second)->getOutputEntity());
			}
		}
	}
}
void Rob::loadPartialCpg(std::string prefix){

	if(prefix.find("right") != string::npos){
		cpg_right_loaded = true;
		cout << "right cpg_loaded" <<  endl;
		for(auto& kv : motoneurons){
			//cout << "elements in " << kv.first << " > " << network[kv.second].size();
			for(auto& kv2 : modelized_sensory_interneurons){
				
				
				auto it = cpgs_right.find("right_" + kv2);
				interneurones["right_" + kv2] = it->second;
				interneurones_name[it->second] = "right_" + kv2;
				
				if(((Interneuron*)it->second)->getOutputEntity() == kv.second){
					connections[it->first]->input = it->second;
				}
			}
		}
	}
	else if(prefix.find("left") != string::npos)
	{
		cpg_left_loaded = true;
		cout << "left cpg_loaded" <<  endl;
		for(auto& kv : motoneurons){
			//cout << "elements in " << kv.first << " > " << network[kv.second].size();
			for(auto& kv2 : modelized_sensory_interneurons){
				auto it = cpgs_left.find("left_" + kv2);
				interneurones["left_" + kv2] = it->second;
				interneurones_name[it->second] = "left_" + kv2;
				if(((Interneuron*)it->second)->getOutputEntity() == kv.second)
					connections[it->first]->input = it->second;
			}
		}
	}
}
