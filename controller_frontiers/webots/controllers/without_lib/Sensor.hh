#ifndef __SENSOR_HH__
#define __SENSOR_HH__

/*!
 * \file Sensor.hh
 * \brief Sensor description (touch sensor / muscle length / muscle force / foot / ...)
 * \author efx
 * \version 0.1
 */

#include <vector>
#include <string>
#include <math.h>
#include <map>
#include <webots/TouchSensor.hpp>

#include <boost/any.hpp>
#include "RungeKutta.hh"
#include "Muscle.hh"


extern State state;
class Sensor {
public:
	virtual double get() = 0;
};


/**
 * 
 * Trunk class, angle are with respect to the vertical axes (the one // to gravity)
 * 
 */
class Trunk: public webots::Supervisor{
public:
	
	double theta;
	double& theta_ref;
	double& theta_offset;
	double d_theta;
	double theta_to;
	//Trunk():theta(0.0),theta_ref(0.0),d_theta(0.0),theta_to(0.0){}
	Trunk(double initial_theta_trunk, double& theta_trunk_ref, double& theta_offset): theta(initial_theta_trunk), theta_ref(theta_trunk_ref), theta_offset(theta_offset), d_theta(0.0), theta_to(theta_trunk_ref){}
	void ComputeAngle(double dt){
		double old_theta;
		old_theta     = theta;
		theta = -this->getFromDef("REGIS")->getOrientation()[5] ; //angle of the trunk with respect to gravity
		d_theta     = (theta     - old_theta   )  / dt;
	}
	void setThetaRef(double t){
		theta_ref = t;
	}
	double getThetaRef(){
		return theta_ref;
	}
	void addThetaRef(double t){
		theta_ref += t;
	}
	
};


/**
 * 
 * BioTouchSensor class
 * 
 * 		Ground sensors (left teel, left ankle, right teel, right ankle) are Biosensors
 * 
 * 
 */
class BioTouchSensor : public TouchSensor, public Sensor, public Derivatives {
public:
	double value;
	double dt;
	BioTouchSensor(TouchSensor & touchsensor,int &time_step): TouchSensor(touchsensor),value(10.0),dt(0.001){this->enable(time_step);}
	
	double get(int i){
		if(i==0 || i==1)
		{
			double val = this->getValues()[i];
			//if(val < 50.0)
			//	val = 0.0;
			return val;
		}
		else
			return 0.0;
	}
	
	double get(){
		step();
		return value;
	}
	void step(){
		vector<double> in;
		in.push_back(get(1));
		
		vector<double> y;
		y.push_back(value);
		
		y=rungeKutta4(state.duration,dt,y,in,(Derivatives *)this);
		value = y[0];
	}
	
	vector<double> dydt(double x, vector<double> y, vector<double> param, vector<double> input){
		vector<double> dadt(y);
		double tau = 1.0/(5.0/0.6931*dt);
		dadt.at(0) = tau*(input[0]-y[0]);
		return dadt;
	}
};


/**
 * 
 * MuscleLengthSensor class
 * 
 * 
 */
class MuscleLengthSensor: public Sensor{
public:
	Muscle * muscle;
	double& offset;
	double& gain;
	MuscleLengthSensor(Muscle * muscle, double &offset, double &gain):muscle(muscle),offset(offset),gain(gain){}
	double get(){
		//change was: return gain*(muscle->l_CE/muscle->l_opt - offset);
		double out = gain*(muscle->l_CE/muscle->l_opt - offset);
		if(out <0)
			return 0.0;
		else
			return out;
	}
};

/**
 * 
 * MuscleForceSensor class
 * 
 * 
 */
class MuscleForceSensor: public Sensor{
	Muscle * muscle;
	double& gain;
public:
	MuscleForceSensor(Muscle * muscle, double &gain):muscle(muscle), gain(gain){}
	double get(){
		return gain * muscle->F_MTC / muscle->F_max;
	}
};


/**
 * 
 * JointPositionSensor class
 * 
 * 
 */
class JointPositionSensor: public Sensor{
	Joint * joint;
	double& kp;
	double& kd;
	double& ref;
public:
	JointPositionSensor(Joint * joint, double& kp, double& kd, double & ref):joint(joint),kp(kp),kd(kd), ref(ref){}
	double get(){
		return kp * (this->joint->angle - ref) + kd * joint->d_angle;
	}
};

/**
 * 
 * TrunkPositionSensor class
 * 
 * 
 */
/*
class TrunkPositionSensor: public Sensor{
	Trunk * trunk;
	double& kp;
	double& kd;
	double& ref;
public:
	TrunkPositionSensor(Trunk * trunk, double& kp, double& kd, double & ref):trunk(trunk),kp(kp),kd(kd), ref(ref){}
	double get(){
		return get(0.0);
	}
	double get(double o){
		return kp * (this->trunk->theta - (ref+o) ) + kd * trunk->d_theta;
	}
};
*/
class TrunkPositionSensor1: public Sensor{
	Trunk * trunk;
	double& kp;
	double& kd;
public:
	TrunkPositionSensor1(Trunk * trunk, double& kp, double& kd):trunk(trunk),kp(kp),kd(kd){}
	double get(){
		return 
			kp * (this->trunk->theta - this->trunk->theta_ref - this->trunk->theta_offset) 
			+ 
			kd * trunk->d_theta;
	}
};
class TrunkPositionSensor2: public Sensor{
	Trunk * trunk;
	double& kp;
	double& kd;
public:
	TrunkPositionSensor2(Trunk * trunk, double& kp, double& kd):trunk(trunk),kp(kp),kd(kd){}
	double get(){
		return
			kp *0.68 * (this->trunk->theta - this->trunk->theta_ref - this->trunk->theta_offset) 
			+ 
			kd * trunk->d_theta;
	}
};
/**
 * 
 * TrunkTakeOffSensor class
 * 
 * 
 */
class TrunkTakeOffSensor: public Sensor{
	Trunk * trunk;
	double& ref;
public:
	TrunkTakeOffSensor(Trunk * trunk, double &ref): trunk(trunk),ref(ref){}
	double get(){
		return trunk->theta_to - ref;
	}
};
class TrunkTakeOffSensorOld: public Sensor{
	Trunk * trunk;
public:
	TrunkTakeOffSensorOld(Trunk * trunk): trunk(trunk){}
	double get(){
		return trunk->theta_to - trunk->theta_ref;
	}
};
/**
 * 
 * AngleSensor class for OverExtensionReflex
 * 
 * 
 */
class AngleSensor: public Sensor{
	Joint * joint;
	double& offset;
public:
	AngleSensor(Joint * joint, double& offset): joint(joint), offset(offset){}
	double get(){
		return joint->angle - offset;
	}
};
/**
 * 
 * Foot class (special sensors that have to BioTouchSensor to feel the ground 
 * and also a limb_state variable that says if the foot thinks is in stance or swing
 * 
 *   This is a special sensor used by the Gate that turns the feedbacks on/off 
 *   regarding the limb_state on which they need to be active.
 *   
 */

class Foot: public Sensor
{
public:
	double scalingfactor;
	std::string limb_state;
	std::string lastlimb_state;
	double delay;
	BioTouchSensor * heel;
	BioTouchSensor * toe;
	double last_time_limb_state_changed;
	double last_time_foot_touch;
	double duration_last_step;
	double distance_last_step;
	double last_pos_foot_touch;
	double &off;
	std::string side;
	Foot(BioTouchSensor * heel, BioTouchSensor * toe, std::string side,double &off):  scalingfactor(784.8),limb_state("stance"), lastlimb_state("stance"), delay(0.05), heel(heel), toe(toe),last_time_limb_state_changed(-1.0),last_time_foot_touch(0.5),duration_last_step(0.5),distance_last_step(0.0), last_pos_foot_touch(0.0),off(off),side(side){}
	Foot(BioTouchSensor * heel, BioTouchSensor * toe, double time, std::string side, double &off):  scalingfactor(784.8),limb_state("stance"), lastlimb_state("stance"), delay(0.05), heel(heel), toe(toe), last_time_limb_state_changed(time),off(off),side(side){}
	std::string getName(){
		return side;
	}
	double get(){
		// return off+getFeltForce()/scalingfactor; THIS CAN BE USED TO ADD A FORCE SENSOR
		return getFeltForce()/scalingfactor;
	}
	bool inStance(){
		if(limb_state=="stance")
			return true;
		else
			return false;
	}
	bool justTouchTheGround(){
		if(limb_state=="stance" && lastlimb_state=="swing"){
			return true;
		}
		else
			return false;
	}
	bool justGetOffGround(){
		if(limb_state=="swing" && lastlimb_state=="stance")
			return true;
		else
			return false;
	}
	double getFeltForce(){
		double x = heel->get()+toe->get();
		return 1/(1+exp(-5*(x-100)))*x; // pass through a sigmoid filter
	}
	double getRealFeltForce(){
		double val = heel->get()+toe->get();
		return val;
	}
	double getXForce(){
		return heel->get(0)+toe->get(0);
	}
	double getYForce(){
		return heel->get(1)+toe->get(1);
	}
	double getZForce(){
		return heel->get(2)+toe->get(2);
	}
	std::string getLastState(){
		return lastlimb_state;
	}
	std::string getState(){
		return limb_state;
	}
	void setState(std::string str){
		limb_state = str;
	}
	void updateState(double actual_time){
		lastlimb_state = limb_state;
		//		if(actual_time - last_time_limb_state_changed > delay){
			if(this->getRealFeltForce() > 0.1){ //if the foot bear enough weight, the leg is in stance
				limb_state="stance";
			}
			else{
				limb_state="swing";
			}
			//		}
			if(lastlimb_state != limb_state){
				last_time_limb_state_changed = actual_time;
				if(lastlimb_state == "swing" && limb_state == "stance")
				{
					duration_last_step = actual_time - last_time_foot_touch;
					distance_last_step = state.position - last_pos_foot_touch;
					last_time_foot_touch = actual_time;
					last_pos_foot_touch = state.position;
					if(duration_last_step < 0.0 && !state.is_in_launching_phase)
						std::cout << "error duration negative: " << duration_last_step << std::endl;
				}
			}
			//return limb_state;
	}
};


#endif /* __SENSOR_HH__ */

