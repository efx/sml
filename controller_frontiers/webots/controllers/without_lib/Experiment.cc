#include "Experiment.hh"
//#include <unordered_map>
#include "config.hh"
#include "Phase.hh"

using namespace std;
extern State state;
extern bool debug;
inline int convertToInt(string const& s);
//constructor
Experiment::Experiment(Rob * body, Parameters *set) : body(body), parameters(set){ this->init(); }

Experiment::Experiment(Rob * body, Parameters *set1,Parameters *set2): body(body), parameters(set1), parameters_cpg(set2){ this->init();}

// Init
void Experiment::init(){
	changed=false;
	first_saved=false;
	max_wait=5.0;
	spent_energy=0.0;
	waited=0.0;
	changed_at_dist=0.0;
	changed_at_nbsl=0;
	change_number=-1;
	loadingEnded = false;
	color = 0xffffff; // color of the text in the 3d windows
	laststeppos = 0.0;
	last_phase = false;
	data_time.push_back(0.0);
	this->trySetMode("optimization");
	
	if(boost::any_cast<int>(Settings::set["save_for_matlab"]) == 1)
		this->createRawFiles();
	
	
	
	if(Settings::str["experiment"] == "initialPhase"){
		phases.push_back(new InitialPhaseFinder(this));
	}
	else{
		phases.push_back(new InitialPhase(this));
		istringstream f(Settings::str["experiment"]);
		string p;    
		while (getline(f, p, '|')) {

			map<string, string> options = {
				{"start_after_step","3"},
				{"p_file",""},
				{"restart_state","restart state"}
			};
			
			vector<string> strs;
			boost::split(strs, p, boost::is_any_of("+"));
			if(strs.size() > 1){
				options["start_after_step"] = strs[1];
				p = strs[0];
				strs.clear();
			}
			
			boost::split(strs, p, boost::is_any_of("{"));			



			// If option
			if(strs.size() > 1)
			{
				vector<string> strs2;
				boost::split(strs2, strs[1], boost::is_any_of("}"));
				vector<string> strs3;
				boost::split(strs3, strs2[0], boost::is_any_of(","));
				
				for(auto &it:strs3){
					vector<string> strs4;
					boost::split(strs4, it, boost::is_any_of("="));
					options[strs4[0]] = strs4[1];
				}
			}
			
			int start_after_step = stoi( options["start_after_step"]);
			string p_file = options["p_file"];
			string restart_state = options["restart_state"] != "restart state" ? "" : "restart state";
			
			
			if(strs[0] == "fullReflex"){
				if(p_file != "")
					phases.push_back(new FullReflexPhase(this,p_file,start_after_step,restart_state));
				else
					phases.push_back(new FullReflexPhase(this, start_after_step, restart_state));
			}
	
			else if(strs[0]=="semiCpg"){
				if(p_file != "")
					phases.push_back(new SemiReflexPhase(this,p_file,start_after_step,restart_state));
				else
					phases.push_back(new SemiReflexPhase(this, start_after_step, restart_state));
				
			}
			else if(strs[0]=="semiCpg-replace"){
				phases.push_back(new SemiReflexReplacedPhase(this, start_after_step,restart_state));
			}
			else if(strs[0]=="fullCpg")
				phases.push_back(new CpgReflexPhase(this,start_after_step,restart_state));
			else if(strs[0]=="systematicFdbReplace")
				phases.push_back(new SystematicFeedbacksStudyPhase(this, start_after_step, restart_state));
			else
			{
				cout << "unknown phase : " << p << endl;
			}
		}
	}
	/*
	 * 
	 *  semiCpg_replace --> semiCpg-replace
	 *  fullReflex_noOpti --> fullReflex-noOpti
	 * 
	 */
	
	phase = phases.begin();
	
	if(boost::any_cast<int>(Settings::set["automatic_systematic_search"]) == 1){
		parameters_cpg_control = new parametersControl(Settings::str["config"] + Settings::str["systematic_control_rule_file"], parameters_cpg);
	}
	if(boost::any_cast<int>(Settings::set["automatic_systematic_search"]) == 2){
		parametersControl::loadFromFile(Settings::str["config"] + Settings::str["systematic_control_rule_file"],parameters_cpg_control_file);
	}	
	if(boost::any_cast<int>(Settings::set["automatic_systematic_search"]) == 3){
		parametersControl::loadFromFile(Settings::str["config"] + Settings::str["systematic_control_rule_file"],parameters_cpg_control_file);
		string folder = Settings::str["config"] + Settings::str["linearInterpolant_folder"];
		std::map<std::string,double>* set;
		if(Settings::str["interpolate"] == "cpg"){
			set = &this->body->parameters_INSenLayer->set["parameters"];
		}
		else{
			set = &this->body->parameters_controlLayer->set["parameters"];
		}
		linearInterpolant = new LinearInterpolant(folder, set, set->size());
	}
	
	
}

// Run an experiment
void Experiment::run(){
	initiation();
	one_trial("energyoptimisation");
	this->termination();
}

// The trial
State Experiment::one_trial(string mode){
	bool quit = false;
	
	while(state.stay_in_loop && !quit) {
		if(phase < phases.end())
		{
			if((*phase)->rule(&phase)){
				//_size ++;
				//cout << "Phase " << _size << " ended" << endl;
			}
		}
		if(phase ==  phases.end() && !last_phase){
			last_phase = true;
			if(boost::any_cast<int>(Settings::set["extract_video"]) == 1)
				body->startMovie(Settings::str["video_name"],500,400,0,100);
		}
		
		for(auto &parameters: parameters_cpg->set["parameters"]){
			if(parameters.second != lastparameters_cpg_value[parameters.first]){
				//cout << "parameters changed, keeping track...";
				lastparameters_cpg_value[parameters.first] = parameters.second;
			}
		}

		this->step();
	}
	

	if((*(phases.end()-1))->name == "SystematicFeedbacksStudy"){
		cout << "\nFalled but in systematic Feedback study...\nsaving phase state" << endl;
		
		SystematicFeedbacksStudyPhase::serialize_state(
			(SystematicFeedbacksStudyPhase*)(*(phases.end()-1)));	
		((SystematicFeedbacksStudyPhase*)(*(phases.end()-1)))->save_phase_state();
		body->simulationRevert();
	}
	
	
	int falled_at = -1;
	if(quit == false){

		*parameters_cpg_file_associated_state << "1" << endl;
		falled_at = change_number+1;
	}

	if(boost::any_cast<int>(Settings::set["automatic_systematic_search"]) == 2){
		ofstream *file;
		file = new ofstream();
		file->open("../../../log/"+Settings::str["systematic_control_rule_file"]+".log");
		(*file) << "falled at line " << falled_at;
		file->close();
	}
	return state;
}


void Experiment::step(){
	
	
	
	body->step();
	writeRawContent();
	addRawData();
	body->plotTextInWindow(color);
	
	
	debug == true ? cout << "[ok] : one step (Experiment.cc)" << endl:true;
}
