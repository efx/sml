#ifndef __MOTONEURON_HH__
#define __MOTONEURON_HH__

/*!
 * \file Motoneuron.hh
 * \brief Motoneuron definition
 * \author efx
 * \version 0.1
 */

#include <string>
#include <iostream>
#include "Entity.hh"

class Motoneuron : public Entity{
	public:
	double input;
	double tau;
	Motoneuron();
	Motoneuron(std::string side);
	Motoneuron(double state);
	Motoneuron(double state, double input);
	double get(double dt);
	void set(double value);
	void add(double value);
	void apply(){};
	void apply(double a, double o){};
};

#endif /* __MOTONEURON_HH__ */