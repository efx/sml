#ifndef __SIGNAL_HH__
#define __SIGNAL_HH__

/*!
 * \file Interneuron.hh
 * \brief Interneurons definition (all are children of the Entity mother class) (mono-sensory / bi-sensory / cpg)
 * \author efx
 * \version 0.1
 */

#include <string>
#include <math.h>
#include <fstream>
#include <map>
#include "Entity.hh"
#include "Muscle.hh"
#include "Sensor.hh"
#include "Settings.hh"

#include <boost/any.hpp>
/**
 * 
 *  Mother SensoryInterneuron class
 * 
 */
extern double weight_const;
using namespace std;
class Interneuron : public Entity{
public:
	Entity * output_neuron; // output_neuron in output
	double& weight; // connexion weight
	double factor;
	Interneuron() : Entity(), weight(weight_const),factor(1.0){};
	Interneuron(Entity * output_neuron) : Entity(), output_neuron(output_neuron), weight(weight_const),factor(1.0){};
	Interneuron(Entity * output_neuron, double& weight) : Entity(), output_neuron(output_neuron), weight(weight),factor(1.0){};
	Interneuron(Entity * output_neuron, double& weight, double factor) : Entity(), output_neuron(output_neuron), weight(weight),factor(factor){};
	Interneuron(Entity * output_neuron, string active_during) : Entity("cycle",active_during), output_neuron(output_neuron), weight(weight_const),factor(1.0){};
	Interneuron(Entity * output_neuron, double& weight, string active_during) : Entity("cycle",active_during), output_neuron(output_neuron), weight(weight),factor(1.0){};
	Interneuron(Entity * output_neuron, double& weight, double factor, string active_during) : Entity("cycle",active_during), output_neuron(output_neuron), weight(weight),factor(factor){};
	virtual void apply() = 0;
	virtual void apply(double o, double a) = 0;
	virtual void add(double state ){this->state += state;}
	virtual Entity * getOutputEntity(){return output_neuron;}
};
class SensoryInterneuron : public Interneuron{
public:
	Foot * foot;
	map<string, double> parameters;
	Sensor * sensor; // sensor in input
	SensoryInterneuron(Foot * foot, string active_during, map<string, double> parameters, Sensor * sensor, Entity * output_neuron, double& weight): Interneuron(output_neuron,weight,active_during), foot(foot), parameters(parameters),sensor(sensor){}
	SensoryInterneuron(Foot * foot, string active_during, map<string, double> parameters, Sensor * sensor, Entity * output_neuron, double& weight, double factor): Interneuron(output_neuron,weight,factor,active_during), foot(foot), parameters(parameters), sensor(sensor){}
	void apply();
	void apply(double o, double a);
	};
class SensorySensoryInterneuron : public Interneuron{
public:
	Foot * foot;
	map<string, double> parameters;
	Sensor * sensor1; // sensor in input
	Sensor * sensor2;
	
	SensorySensoryInterneuron(Foot * foot,string active_during,map<string, double> parameters,Sensor * sensor1, Sensor * sensor2, Entity * output_neuron, double& weight): Interneuron(output_neuron,weight,active_during), foot(foot), parameters(parameters), sensor1(sensor1), sensor2(sensor2){};
	SensorySensoryInterneuron(Foot * foot,string active_during,map<string, double> parameters,Sensor * sensor1, Sensor * sensor2, Entity * output_neuron, double& weight, double factor): Interneuron(output_neuron,weight,factor,active_during), foot(foot), parameters(parameters), sensor1(sensor1), sensor2(sensor2){};
	void apply();
	void apply(double o, double a);
	};
		
class CPGInterneuron : public Interneuron{
public:
	double dt;
	Parameters * parameters;
	double t;
	double tau;
	double offset;
	double amplitude;
	double freq;
	string file;
	Foot * foot;
	Foot * contra_foot;
	vector<double> yi;
	vector<double> teacher;
	double f;
	CPGInterneuron(string active_during, Parameters * parameters, int time_step,Entity * output_neuron, Foot * foot, Foot * contra_foot, string file,double amp): Interneuron(output_neuron, active_during), dt(double(time_step)/1000.0),parameters(parameters), t(0.0),tau(200.0),file(file), foot(foot), contra_foot(contra_foot){
		//cout << "../../conf/cpg_data/" + file + ".txt" << endl;
		ifstream myfile (Settings::str["config"]+"cpg_gate/cpg_data/" + file + ".txt");
		string line ; // the string containing the line
		vector<string> strs; // the vector containing the key->value pair
		if (myfile.is_open()){
			getline( myfile, line );
			boost::split(strs, line, boost::is_any_of("\t "));
				offset = convertToDouble(strs[0]);
			if(amp != 0.0){
				amplitude = convertToDouble(strs[1]);
			}
			else{
				amplitude = amp;
			}
			freq = 1.0;

			while( getline( myfile, line ) )
			{
				teacher.push_back(convertToDouble(line));
			}
			if(foot->getName().find("LEFT") != string::npos){
				yi.push_back(0.0);
				yi.push_back(offset+amplitude*teacher[0]);
			}
			else{
				yi.push_back(0.5);
				yi.push_back(offset+amplitude*teacher[0]);
			}
			myfile.close();
		}
		else{
			cout<<"ERROR: unable to open file '" << file << "'. In:Interneuron.hh, constructor(string, string)"<< endl;
			exit (EXIT_FAILURE);
		}
	}
	void update_state();
	void apply();
	void apply(double o, double a);
private:
	void initialize();
	void initialize(double mean, double std);
};
#endif /* __SIGNAL_HH__ */