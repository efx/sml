#include "CentralClock.hh"
#include "Interneuron.hh"
#include "Settings.hh"

using namespace std;
using namespace webots;
extern State state;
extern CentralClock * centralclock;
extern double debug;
#include <efx/rungekutta.h>




double weight_const = 0.0;
double in(double x,vector<double> &teacher){
	double size = double(teacher.size());
	return teacher[int(round(fmod(double(round(size*x))/size,1.0)*size))];
}
vector<double> dydtF(double t, vector<double> y, vector<double> param, vector<double> input){
	//dydt(1) = w;
	//dydt(2) = tau*(in(y(1))-y(2))+(in(y(1)+h/2)-in(y(1)-h/2))/(h)*dydt(1);
	double f   = param[0];
	double tau = param[1];
	double dt  = param[2];
	double offset = param[3];
	double amplitude = param[4];
	vector<double> dydt(y);
	if( fmod(y[0],1.0) < 0.9){
		//cout << "----" << endl;
		dydt[0] = f;
	}
	else{
		dydt[0] = 10*f*exp(-10*f*t+9-log(10));
		//double oldy0 = y[0];
		//y[0] = 1-exp(-10*f*t+9-log(10));
		//dydt[0] = y[0]-oldy0
		//cout  << f << "," << y[0] << endl;
	}
	dydt[1] = tau*(offset+amplitude*in(y[0],input)-y[1])+amplitude*(in(y[0]+dt/2,input)-in(y[0]-dt/2,input))/dt*dydt[0];
	return dydt;
}
void CPGInterneuron::initialize(){
	initialize(offset, amplitude);
}
void CPGInterneuron::initialize(double offset, double amplitude){
	if(yi.size()==2){
		yi[0] = 0.0;
		yi[1] = offset+amplitude*teacher[0];
		t=0.0;
	}
	else{
		t=0.0;
		yi.push_back(0.0);
		yi.push_back(offset+amplitude*teacher[0]);
	}
	f = foot->duration_last_step;
}

void CPGInterneuron::update_state(){
	freq = centralclock->getFrequency()*parameters->set["parameters"]["freq_change"];//1./duration;
}
void CPGInterneuron::apply(double o, double a){
	apply();
	state = o+a*state;
}
void CPGInterneuron::apply(){
	if(foot->justTouchTheGround()){
		update_state();
		initialize();
	}
	vector<double> param(5,0.0);
	param[0] = freq;
	param[1] = tau;
	param[2] = dt;
	param[3] = offset;
	param[4] = amplitude;
	yi = rungeKutta4(t,dt,yi,param,teacher,(*dydtF));
	t+=dt;
	state = yi[1];
}

void SensorySensoryInterneuron::apply(){
	state = factor*weight*sensor1->get()*sensor2->get();
}
void SensorySensoryInterneuron::apply(double o, double a){
	apply();
	state = o+a*state;
}
void SensoryInterneuron::apply(){
	state = factor*weight*sensor->get();
}
void SensoryInterneuron::apply(double o, double a){
	apply();
	state = o+a*state;
}
