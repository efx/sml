#include <stdio.h>
#include <sml/sml.hh>

#include "Experiment.hh"
#include <string>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>




using namespace std;

CentralClock * centralclock;
double debug=false;
std::map<std::string, double_int_string> Settings::set;

void loadControlParametersName(std::map<std::string, std::map<std::string, double> >& param);
void loadGeyerParametersName(std::map<std::string, std::map<std::string, double> >& param);

int main(int argc, char* argv[])
{
    Parameters *Geyer;
    Parameters *Control;
    
    /**__**/ cout << "1) Loading settings...............";

	//if(argc == 2)
	//	Settings settings(argv[1]);
	//else
		Settings settings;
    
    cout << Settings::set["experiment"] << endl;
    /**__**/ cout << "Ok" << endl;
    /**__**/ cout << "2) Loading default parameters...............";
    std::map<std::string, std::map<std::string, double> > geyer;
    //std::map<std::string, std::map<std::string, double> > efx;
    std::map<std::string, std::map<std::string, double> > control;
    loadGeyerParametersName(geyer);
    loadControlParametersName(control);
    Geyer = new Parameters(geyer);
    Control = new Parameters(control);
    /**__**/ cout << "Ok" << endl;

	if(boost::get<int>(Settings::set["log"]) == 2){
		ofstream out("../../../log/webots_log.txt", ios::app);
		streambuf *coutbuf = cout.rdbuf(); //save old buf
		cout.rdbuf(out.rdbuf()); //redirect std::cout to out.txt!
	}
	srand(time(0));
	
	
    /**__**/ cout << "3) Create CentralClock...............";
	centralclock = new CentralClock(Geyer->set["parameters"]["freq"]);
	/**__**/ cout << "Ok" << endl;
    /**__**/ cout << "4) Create Robot...............";
    Rob *Regis = new Rob(Geyer->set["parameters"], Control->set["parameters"]);
    
    //eventManager = Regis->eventManager;
    /**__**/ cout << "Ok" << endl;
    /**__**/ cout << "5) Create Experiment...............";
	Experiment exp(Regis, Geyer, Control);
	/**__**/ cout << "Ok" << endl;
	//if(argc == 3)
	//	exp.change_number = convertToInt(argv[2])-1;
	// Run the experiment
	exp.run();
	
	return 0;
}
void loadControlParametersName(std::map<std::string, std::map<std::string, double> >& param){
	param["parameters"]["offset_change"] = 0.0;
	param["parameters"]["offset_change_bas"] = 0.0;
	param["parameters"]["offset_change_reflex"] = 0.0;
	param["parameters"]["amp_change"] = 1.0;
	param["parameters"]["amp_change_bas"] = 1.0;
	param["parameters"]["amp_change_reflex"] = 1.0;
	param["parameters"]["trunk_ref"] = 0.0;
	
	param["parameters"]["freq_change"] = 1.0;
	param["parameters"]["stance_end"] = 0.0;
	param["parameters"]["swing_end"] = 0.0;
}
void loadGeyerParametersName(std::map<std::string, std::map<std::string, double> >& param){
    // Default parameters
    param["parameters"]["solsol_wf"] = 0.65341611611853989139;
    param["parameters"]["tata_wl_st"] = 0.332814881307563315;
    param["parameters"]["tata_wl_sw"] = 0.332814881307563315;
    param["parameters"]["solta_wf"] = 1.1526980944100497783;
    param["parameters"]["gasgas_wf"] = 1.1068077891810712554;
    param["parameters"]["vasvas_wf"] = 0.77993426421509415292;
    param["parameters"]["hamham_wf"] = 0.49029746580493743791;
    param["parameters"]["gluglu_wf"] = 0.076462083734209956853;
    param["parameters"]["hfhf_wl"] = 0.50585588988083418638;
    param["parameters"]["hamhf_wl"] = 0.82778009455857337606;
    param["parameters"]["ta_bl_sw"] = 0.20741854578256185837;
    param["parameters"]["ta_bl_st"] = 0.20741854578256185837;
    param["parameters"]["hf_bl"] = 0.48522419143032552435;
    param["parameters"]["ham_bl"] = 0.48103142880817645333;
    param["parameters"]["kphiknee"] = 0.3612329212386057864;
    param["parameters"]["kphiknee_off"] = 2.97;
    param["parameters"]["kbodyweight"] = 0.43017472946576890136;
    
    // trunk control
    param["parameters"]["kp_ham"] = 0.50226761669972064261;
    param["parameters"]["kd_ham"] = 1.1592334201551834916;
    param["parameters"]["kref_ham"] = 0.105;
    
    param["parameters"]["kp_glu"] = 0.50226761669972064261;
    param["parameters"]["kd_glu"] = 1.1592334201551834916;
    param["parameters"]["kref_glu"] = 0.105;
    
    param["parameters"]["kp_hf"] = 0.50226761669972064261;
    param["parameters"]["kd_hf"] = 1.1592334201551834916;
    param["parameters"]["kref_hf"] = 0.105;
    
    // hip control
    param["parameters"]["hipkp_hf"] = 0.50226761669972064261;
    param["parameters"]["hipkd_hf"] = 0.50226761669972064261;
    
    param["parameters"]["hipkp_glu"] = 0.50226761669972064261;
    param["parameters"]["hipkd_glu"] = 0.50226761669972064261;
    
    param["parameters"]["kref_hip"] = 0.50226761669972064261;
    // knee control
    param["parameters"]["kneekp_vas"] = 0.50226761669972064261;
    param["parameters"]["kneekd_vas"] = 0.50226761669972064261;
    param["parameters"]["kref_knee"] = 0.50226761669972064261;
    
    
    param["parameters"]["sol_activitybasal_swing"] = 0.7310296678150665084;
    param["parameters"]["ta_activitybasal_swing"] = 0.60467744570980996865;
    param["parameters"]["gas_activitybasal_swing"] = 0.31046097011931184095;
    param["parameters"]["vas_activitybasal_swing"] = 0.69071424442607254335;
    param["parameters"]["ham_activitybasal_swing"] = 0.53213646478239429172;
    param["parameters"]["glu_activitybasal_swing"] = 1.1263660725436519527;
    param["parameters"]["hf_activitybasal_swing"] = 0.32943183288119209928;
    
    param["parameters"]["sol_activitybasal_stance"] = 0.7310296678150665084;
    param["parameters"]["ta_activitybasal_stance"] = 0.60467744570980996865;
    param["parameters"]["gas_activitybasal_stance"] = 0.31046097011931184095;
    param["parameters"]["vas_activitybasal_stance"] = 0.69071424442607254335;
    param["parameters"]["ham_activitybasal_stance"] = 0.53213646478239429172;
    param["parameters"]["glu_activitybasal_stance"] = 1.1263660725436519527;
    param["parameters"]["hf_activitybasal_stance"] = 0.32943183288119209928;
    
    param["parameters"]["klean"] = 0.17090550802838316846;
    param["parameters"]["kref"] = 0.105;
    
    param["parameters"]["stance_end"] = 1.0;
    param["parameters"]["swing_end"] = -1.0;
    param["parameters"]["freq"] = 1.0;
    param["parameters"]["deltas_vas"] = 0.73559614103666370877;
    param["parameters"]["deltas_glu"] = 0.73559614103666370877;
    param["parameters"]["deltas_hf"] = 0.73559614103666370877;
    param["parameters"]["1.0"] = 1.0;
}

