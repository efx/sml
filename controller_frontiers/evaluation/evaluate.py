#!/bin/python2.7

import subprocess
import argparse
import os
sml_path = os.environ['smlpath']

        
parser = argparse.ArgumentParser()
parser.add_argument("expserie", help="experiment serie")
parser.add_argument("-r", "--run", help="extraction type", type=str, default="all")
parser.add_argument("-m", "--models", help="model file", type=str, default="models.txt")
parser.add_argument("-f", "--folder", help="model file", type=str, default="repeat")
args = parser.parse_args()




def decorable(cls):
    cls.__lshift__ = lambda objet, fonction: fonction(objet)
    return cls

@decorable
class Evaluator:
    repeat = 6
    def __init__(self,experimenttype, modeltype, path):
        self.experimenttype = experimenttype
        self.modeltype = modeltype
        self.path = path
    def prepare(self):
        self.override("modeltype",self.modeltype)
        self.override("experimenttype", self.experimenttype)
        self.override("mode", "batch")
        self.override("distance_max", "120")
        self.override("webotsPath","webots_loadlib_optiextractor")
        self.override("save_for_matlab","0")
        self.override("save_for_matlab_overwrite","0")
        self.override("save_for_matlab_folder","session5/")
    def override(self,what,by):
        pass
    def call(self,world):
        pass
    def run(self,value):
        {
        'base': self._extract_base,
        'force': self._extract_force,
        'distance': self._extract_distance,
        'rp_eval': self._extract_rp_eval,
        }.get(value, lambda : None)()
    
    def _extract_base(self):
        self.override("extract", "base")
        self.override("extractoutput", "base.txt")
        self.override("worldname", "base.wbt")
        print "Extracting on base.wbt"
        self.call("{0}/current/webots/worlds/version52.wbt".format(sml_path))
        print "--> end\n"
        
    
    def _extract_force(self):
        self.override("extract", "force")
        self.override("extractoutput", "force.txt")
        self.override("worldname", "rp.wbt")
        print "Extracting on rp.wbt"
        for i in range(self.repeat):
            print "{0}..".format(self.repeat-i+1)
            self.call("{0}/current/webots/worlds/version52_rp.wbt".format(sml_path))
        print " --> end\n"
        
    def _extract_distance(self):
        self.override("extract", "distance")
        self.override("extractoutput", "distance.txt")
        self.override("distance_max", "80")
        
        
        #self.override("worldBuilderPath","/home/dzeladin/Development/v2.01/webots_tools/world_builder");
        
        print "Extracting on wg.wbt "
#        for i in range(1,6):
        for i in range(1,7):
#        for i in range(6,7):
            print "{0}..".format(self.repeat-i+1)
            self.override("worldname", "version52_wg{0}.wbt".format(i))
            self.call("{1}/current/webots/worlds/version52_wg{0}.wbt".format(i,sml_path))
        print " --> end\n"
        
    def _extract_rp_eval(self):
        self.override("extract", "rp_eval")
        self.override("extractoutput", "rp_eval.txt")
        self.override("worldname", "rp_eval.wbt")
        print "Extracting on rp_eval.wbt"
        self.override("distance_max", "60")
        force = 20;
        for i in range(self.repeat):
            print "{0}..".format(self.repeat-i)
            self.override("force", "{0}".format(force))
            self.call("{0}/current/webots/worlds/version52_rp_eval.wbt".format(sml_path))
            force = force+10;
        print " --> end\n"


class Decorator(Evaluator):
    def __init__(self, evaluator):
        Evaluator.__init__(self,evaluator.experimenttype, evaluator.modeltype, evaluator.path)
        self.evaluator = evaluator
     
     

path="{}/current".format(sml_path);
db_path="{}/datassh/biorobcn/serie13/{}/".format(sml_path,args.folder);
sett_path="{}/webots/conf_articleFrontier_wavy2/settings".format(path)
conf_path="articleFrontier_wavy2"

class OptiExtractor(Decorator):
    def __init__(self,evaluator):
        Decorator.__init__(self,evaluator)
        self.path = "{}/{}".format(db_path,self.path);
        self.prepare()
    def override(self,what,by):
        subprocess.call(["optiextractor","--override-setting="+what,"--override-value="+by,self.path]);
    def call(self,world):
        self.override("world",world)
        subprocess.call(["./myoptiextractor",self.path])

class NormalExtractor(Decorator):
    def __init__(self,evaluator):

        Decorator.__init__(self,evaluator)
        self.path = "{}/{}".format(sett_path,self.path);
        self.settings_template = self.path
        self.settings="evaluator.xml"
        self.settings_path = "{}/{}".format(sett_path,self.settings)
        self.config = conf_path
        self.prepare()
    def override(self,what,by):
        subprocess.call(["add_settings",self.path,what,by])
    def call(self,world):
        subprocess.call(["cp",self.settings_template,self.settings_path])
        subprocess.call(["update_settings",self.settings_path,"NUM",self.modeltype.replace("param","")])
        subprocess.call(["update_settings",self.settings_path,"CPG",self.experimenttype])
        subprocess.call(["webots_loadlib","-s {}".format(self.settings),"-c {}".format(self.config),world])
        
if args.expserie == "normal":
    Extractor = NormalExtractor
else:
    Extractor = OptiExtractor
with open(args.models, 'r') as f:
    for l in f.readlines():
        if l[0] != '#':
            (experimenttype, modeltype, path) = l.replace("\n","").split(" ");
            opti = Evaluator(experimenttype,modeltype, path) << Extractor

            if args.run == "all":
#                opti.run("base")
#                opti.run("force")
                opti.run("distance")
                opti.run("rp_eval")
            else:
                opti.run(args.run)
