#!/usr/bin/python
import math, sys, shutil, re, sqlite3 as lite
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose", help="active verbosity", action="store_true")
parser.add_argument("world", help="output world name")
parser.add_argument("-p", "--wave-starting-pos", help="first wave mean position", type=int, default=13)
parser.add_argument("-w", "--wave-number", help="number of waves", type=int, default=20)
parser.add_argument("-l", "--wave-length", help="wave length (-1 for random, >=0 to specify a distance)", type=int, default=-1)
parser.add_argument("-s", "--wave-slope", help="wave slope (-1 for increasing slope, >0 to specify a slope)", type=int, default=-1)
parser.add_argument("-r", "--number-of-repeat", help="number of repeat of the wavy ground", type=int, default=1)

parser.add_argument("-i", "--interspace", help="interspace scheme (-1 for random, >=0 to specify a distance)", type=int, default=-1)

args = parser.parse_args()


from _world import World
from _world import path

from _world import WavyGround

world = World(
	ground_repeat = args.number_of_repeat,
	wavy_ground = WavyGround(
		number = args.wave_number,
		mean_initial_pos = args.wave_starting_pos,
		repeat = args.number_of_repeat,
		interspace=args.interspace,
		length=args.wave_length,
		slope=args.wave_slope
	)
)
world.write(path+'/webots/worlds'+'/'+args.world)
#world.writeWaveData(path+'/webots/worlds'+'/'+args.world+'.data')
