function varargout = periodic_variability_create_measure(varargin)
    
% [range,A] = periodic_variability_create_measure();
% color = jet(100)
% figure;
% hold on;
% i=10;plot(A(i,:),'color',color(i,:));
% i=25;plot(A(i,:)+3,'color',color(i,:));
% i=50;plot(A(i,:)+6,'color',color(i,:));
% i=75;plot(A(i,:)+9,'color',color(i,:));
% colorbar
% caxis([0,1])
    function varargout = periodic_variability_sinus(Knot)

        x = linspace(0,1,Knot);

        y = sin(2*pi*x);
        dev = std(y);
        signals_local = zeros(12,Knot);
        for j=1:100
            %signals(j,:) = y+(rand(1,Knot)-0.5)*dev*j*0.25;
            %signals_local(j,:) = y+(normrnd(0,1,1,Knot))*dev*j*0.01;
            signals_local(j,:) = y+(normrnd(0,dev*j*0.01,1,Knot));
        end
        [out_local,~] = periodic_variability(signals_local',y');
        varargout{1} = out_local;
        varargout{2} = signals_local;
    end



    if(nargin == 1)
        Knot = varargin{1};
    else
        Knot = 100;
    end    

    nrepeat = 1000;
    A = zeros(nrepeat,Knot);
    for i=1:nrepeat
        [A(i,:),~] = periodic_variability_sinus(Knot);
    end
    
    [~,signals] = periodic_variability_sinus(Knot);
    
    range = sqrt(mean(A,1));
    
    varargout{1} = range;
    varargout{2} = signals;

    

end