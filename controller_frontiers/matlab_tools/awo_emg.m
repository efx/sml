% path
addpath('./lib/cpg/');
addpath('./fct');

% generate nominal motoneurones signal
nominal_emg
[t,y] = awoLaunch(signal,signals,footfall,'mono',0.01,0.001);
hold on;
plot(spike_rate.time,spike_rate.val.data(:,6),'r-.')
plot(1:50,signals(i).amplitude(1:50),'*')
plot(1:50,signals(i).offset(1:50),'*g')
plot(t,y(:,2+3+3+3+3+3)) 
hold off;
legend('real signal',' amp','var off', 'AWO') 