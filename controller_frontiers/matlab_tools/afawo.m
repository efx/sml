function varargout = afawo(varargin)
    if(nargin ~= 3)
		exit
    end
	dt = varargin{1};
	iny = varargin{2};
	input = varargin{3};
	Pteach=@(x)iny(round((x/dt+1)));
	in = @(t) input(round(mod(round(t/dt)*dt,1.0)/dt+1));
    mu = 1.;
    gamma = 8.;
    nu = 0.01;
    
	epsilon = 5.9;
    tau = 8.;

    function dydt = addOne(y,F,i,t)
        X=y(1,i);
        Y=y(2,i);
        W=y(3,i);
        A=y(4,i);
        
        %T = sign(X)*acos(-Y/R);
        
        R = sqrt(X^2+Y^2);
        dX = gamma*(mu-R^2)*X-W*Y+epsilon*F;%+tau*sin(T-P);
        dY = gamma*(mu-R^2)*Y+W*X;
        dW = -epsilon*F*Y/R;

        
        Xawo=y(5,i);
        Tawo=y(6,i);
        Pawo=y(7,i);

        dAawo = nu*Xawo*F;
        dTawo = W/2/pi;
        dPawo = nu*Xawo*F;
        
        if(dTawo~=0)
            Tawo = Tawo-1/(2*pi/Pawo); %P,1 = 3.41
        end
        dXawo = tau*(in(Tawo)-Xawo)+(in(Tawo+dt/2)-in(Tawo-dt/2))/(dt)*dTawo;
		

        
        
        
        
        
        
        
        

        dydt(1) = dX;
        dydt(2) = dY;
        dydt(3) = dW;

        
        dydt(4) = dAawo;
        dydt(5) = dXawo;
        dydt(6) = dTawo;
        dydt(7) = dPawo;
        
        if(sum(sum(isnan(dydt)))~=0)
            keyboard
        end
    end
    function dydt = fprime(t,y)
%        if(t>450 && perturbed == false)
%            y(2:nEq:end) = 10*rand(nOs,1);
%            y(3:nEq:end) = 10*rand(nOs,1);
%            perturbed = true;
%        end
        y=reshape(y(1:end),nEq,nOs);
        Q=sum(y(4,:).*y(5,:));
        if(t>7000)
            F=0;
        else
            F=Pteach(t)-Q;
        end
        
        dydt = zeros(nEq,nOs);
        for i=1:nOs
            dydt(:,i) = addOne(y,F,i,t);
        end
        
    end

    ode = @(t,y) fprime(t,y);

    y0 = [
        1           %X
        0           %Y
        0.8*2*pi    %W
        1           %Aawo
        0           %Xawo
        0           %Tawo
        pi           %Pawo

        %1 0 1.0 1 0.01 
        %1 0 3.5 0.1 0.01 
        %1 0 2*pi 1 0.01
        %1 0 100 3.7664 0.01
        %1 1 0 0.1 0.01 1 0
    ];

    nEq = size(y0,1);
    nOs = size(y0,2);
    %y0 = [ 1 ; y0(:)];
    tspan = [0, 50.0];
    tStart = cputime();
    hw = waitbar(0,sprintf('diff. equation solver'));
    [t,y] = rk4(ode,tspan,y0,dt,hw);
    if(nargout == 2)
        varargout{1} = t;
        varargout{2} = y;
    else
        varargout{1}.t = t;
        varargout{1}.epsilon = y(:,1);
        %y = y(:,2:end);
        varargout{1}.x = y(:,1:nEq:end);
        varargout{1}.y = y(:,2:nEq:end);
        varargout{1}.w = y(:,3:nEq:end);
        varargout{1}.aawo = y(:,4:nEq:end);
        varargout{1}.xawo = y(:,5:nEq:end);
        varargout{1}.tawo = y(:,6:nEq:end);
        varargout{1}.pawo = y(:,7:nEq:end);
        varargout{1}.Pteaching = Pteach(t);
        %varargout{1}.Qlearned = sum(varargout{1}.x.*varargout{1}.a,2);
        varargout{1}.Qlearned = sum(varargout{1}.xawo.*varargout{1}.aawo,2);
    end            
    disp(['duration ' num2str(cputime()-tStart)]);
    close(hw);
end