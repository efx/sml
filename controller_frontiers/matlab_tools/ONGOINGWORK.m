folder = '/home/efx/Development/PHD/LabImmersion/global_data/full_reflex/';
exps = {'base','rp','wg'};
%% HUMAN ROBOT COMP ANGLE
for i=1:length(exps)
    [c(i,1) c(i,2) c(i,3)] = human_robot_comparison('angle',exps(i),folder);
end

%% HUMAN ROBOT COMP TORQUE

i=1;[c(i,1) c(i,2) c(i,3)] = human_robot_comparison('torque',exps(i),folder);
i=2;[c(i,1) c(i,2) c(i,3)] = human_robot_comparison('torque',exps(i),folder);
i=3;[c(i,1) c(i,2) c(i,3)] = human_robot_comparison('torque',exps(i),folder);
i=4;[c(i,1) c(i,2) c(i,3)] = human_robot_comparison('torque',exps(i),folder);

%% HUMAN ROBOT COMP GRF

i=1;[c(i,1) c(i,2)] = human_robot_comparison('grf',exps(i),folder);
i=2;[c(i,1) c(i,2)] = human_robot_comparison('grf',exps(i),folder);
i=3;[c(i,1) c(i,2)] = human_robot_comparison('grf',exps(i),folder);
i=4;[c(i,1) c(i,2)] = human_robot_comparison('grf',exps(i),folder);
%%% OSCILLATORS

%% AFO
% the oscillators frequency varies
t = 0:0.001:1.0008*10^3;
iny = 4*sin((sin(t)*0.001+1).*t*3*2*pi)';
iny = 4*sin(3*t*2*pi)';
Y = afo([0,0.001],iny);

%error
subplot(221)
plot(Y.t,(Y.Qlearned-Y.Pteaching).^2)
title('error')
subplot(222)
plot(Y.t,Y.w)
title('frequency')
subplot(223)
plot(Y.t,Y.a)
title('amplitude')
subplot(224)
plot(Y.t,Y.Qlearned,'r');hold on;plot(Y.t,Y.Pteaching)

%% AFAWO
step(1)=0.01;
colors = hsv(10);
for i=2:8
    step(i)=step(i-1)*4;
end
for i=1:8
    [t,o,y] = dynanet(dt, iny, zscore(input),step(i));
    plot(t,o(:,1),'Color',colors(i,:)); hold on;
end


%%
folder = '../raw_files/';
[a,b,c] = extract('feedbacks', 1, folder); % unstable ground (up-down)
iny = repmat(c(1613:11860,1),100,1);
[a,b,c] = extract('feedbacks', 2, folder); % normal ground
input = interp1(linspace(0,1,100),a(1).value,0:0.001:1);
iny = repmat(c(1613:42670,1),100,1);

iny = repmat(c(1600:19820,1),100,1);
dt = b(1).time(2)-b(1).time(1);


Y = afawo(dt, zscore(iny), zscore(input));

subplot(121);
plot(Y.t,(Y.Qlearned-Y.Pteaching).^2);
title('error');
xlabel('time');
subplot(122);
plot(Y.t,Y.Qlearned);hold on;plot(Y.t,Y.Pteaching,'r')
title('signals')
xlabel('time');
legend('learned','teaching');

%% dynanet


folder = '../raw_files/';
feedbacks = load_data([folder 'feedbacks' int2str(7)]);
fields = fieldnames(feedbacks.data);
act_on_stance = [1,3,5,8,9,10,11,13];
act_on_swing = [2,4,6,7,12];


[a,b,c] = extract('feedbacks', 2, folder); % normal ground
input = interp1(linspace(0,1,100),a(1).value,0:0.001:1);
offset = mean(b(1).offset);
amplitude = mean(b(1).amplitude);

% try to follow one signal
i=1;
iny1=eval(['feedbacks.data.' cell2mat(fields(i))]);
iny1 = repmat(iny1(1600:19820,1),100,1);
% try to follow the mean sensory signal
iny = (sum(feedbacks.val.data(:,act_on_stance),2)/30);
iny = repmat(iny(1600:19820,1),100,1);


[t,o,y] = dynanet(0.001, iny1,input,amplitude,offset);




plot(t,zscore(iny1(1:length(t))))
hold on;plot(t,zscore(cos(o(:,3))),'r')

plot(t,zscore(iny1(1:length(t))),'r');hold on;
plot(t,zscore(y(:,end-1)))

%% VARIABILITY STUDY
folder = '/home/efx/Development/PHD/LabImmersion/Regis/v0.4/raw_files/';
B1 = zeros(7,30);
B2 = zeros(7,30);
y11 = zeros(7,30);
e11 = zeros(7,30);
y21 = zeros(7,30);
e21 = zeros(7,30);
y12 = zeros(7,30);
e12 = zeros(7,30);
y22 = zeros(7,30);
e22 = zeros(7,30);
for j=1:7
    [a,b,c] = extract('feedbacks', j, folder);
    for i=1:30
        K=b(i).best(end);
        for k=b(i).best(end:-1:1)
            if(k~=K)
                break;
            end
            K=K-1;
        end
        [~,B1(j,i)] = periodic_variability(b(i).value(:,2:10));
        [~,B2(j,i)] = periodic_variability(b(i).value(:,11:end));
        y11(j,i) = mean(b(i).amplitude(2:10));
        e11(j,i) = std(b(i).amplitude(2:10));
        y21(j,i) = mean(b(i).offset(2:10));
        e21(j,i) = std(b(i).offset(2:10));
        y12(j,i) = mean(b(i).amplitude(11:end));
        e12(j,i) = std(b(i).amplitude(11:end));
        y22(j,i) = mean(b(i).offset(11:end));
        e22(j,i) = std(b(i).offset(11:end));
        %y1(j,i) = mean(b(i).amplitude(b(i).best));
        %e1(j,i) = std(b(i).amplitude(b(i).best));
        %y2(j,i) = mean(b(i).offset(b(i).best));
        %e2(j,i) = std(b(i).offset(b(i).best));
    end
end

subplot(121);
plot(B1(1:2,:)','+','MarkerSize', 10,'LineWidth',2);
legend('Exp1 A','Exp1 B');
subplot(122);
plot(B2(3:end,:)','+','MarkerSize', 10,'LineWidth',2);
legend('Exp2 A','Exp2 B','Exp3 A','Exp3 B','Exp4');


figure;
B1(find(B1<0.1))=0;
subplot(121)
imagesc(B1(:,[1:10 12:15])')
grid_rectangular(0.5,7.5,8,0.5,15.5,16);
subplot(122)
imagesc(B1(:,[16:25 27:30])')
grid_rectangular(0.5,7.5,8,0.5,15.5,16);
colorbar;
colormap(1-gray)

figure;
B2(find(B2<0.1))=0;
subplot(121)
imagesc(B2(:,[1:10 12:15])')
grid_rectangular(0.5,7.5,8,0.5,15.5,16);
subplot(122)
imagesc(B2(:,[16:25 27:30])')
grid_rectangular(0.5,7.5,8,0.5,15.5,16);
colorbar;
colormap(1-gray)

figure;
bar(log10(abs(y1(:,1:15)./e1(:,1:15))));
figure;
bar(log10(abs(y2(:,1:15)./e2(:,1:15))));
figure;
bar(log10(abs(y1(:,16:30)./e1(:,16:30))))
figure;
bar(log10(abs(y2(:,16:30)./e2(:,16:30))))
%%%%%%
snrMEAN=log10(abs(y11(:,:)./e11(:,:)));
snrSTD=log10(abs(y21(:,:)./e21(:,:)));

snrMEAN(snrMEAN<0.0)=0;
snrMEAN(snrMEAN>0.0 & snrMEAN<0.5)=0.5;
snrMEAN(snrMEAN>0.5 & snrMEAN<1)=1;
snrMEAN(snrMEAN>1 & snrMEAN<1.5)=1.5;
snrMEAN(snrMEAN>1.5)=2;
subplot(221)
imagesc(snrMEAN(:,[1:10 12:15])')
grid_rectangular(0.5,7.5,8,0.5,15.5,16);
subplot(222)
imagesc(snrMEAN(:,[16:25 27:30])')
grid_rectangular(0.5,7.5,8,0.5,15.5,16);
caxis([0 2])
colormap(jet(5))

snrSTD(snrSTD<0.0)=0;
snrSTD(snrSTD>0.0 & snrSTD<0.5)=0.5;
snrSTD(snrSTD>0.5 & snrSTD<1)=1;
snrSTD(snrSTD>1 & snrSTD<1.5)=1.5;
snrSTD(snrSTD>1.5)=2;
subplot(223)
imagesc(snrSTD(:,[1:10 12:15])')
grid_rectangular(0.5,7.5,8,0.5,15.5,16);
subplot(224)
imagesc(snrSTD(:,[16:25 27:30])')
grid_rectangular(0.5,7.5,8,0.5,15.5,16);
caxis([0 2])
colormap(jet(5))
%%%%%%
snrMEAN=log10(abs(y12(:,:)./e12(:,:)));
snrSTD=log10(abs(y22(:,:)./e22(:,:)));

snrMEAN(snrMEAN<0.0)=0;
snrMEAN(snrMEAN>0.0 & snrMEAN<0.5)=0.5;
snrMEAN(snrMEAN>0.5 & snrMEAN<1)=1;
snrMEAN(snrMEAN>1 & snrMEAN<1.5)=1.5;
snrMEAN(snrMEAN>1.5)=2;
subplot(221)
imagesc(snrMEAN(:,[1:10 12:15])')
grid_rectangular(0.5,7.5,8,0.5,15.5,16);
subplot(222)
imagesc(snrMEAN(:,[16:25 27:30])')
grid_rectangular(0.5,7.5,8,0.5,15.5,16);
caxis([0 2])
colormap(jet(5))

snrSTD(snrSTD<0.0)=0;
snrSTD(snrSTD>0.0 & snrSTD<0.5)=0.5;
snrSTD(snrSTD>0.5 & snrSTD<1)=1;
snrSTD(snrSTD>1 & snrSTD<1.5)=1.5;
snrSTD(snrSTD>1.5)=2;
subplot(223)
imagesc(snrSTD(:,[1:10 12:15])')
grid_rectangular(0.5,7.5,8,0.5,15.5,16);
subplot(224)
imagesc(snrSTD(:,[16:25 27:30])')
grid_rectangular(0.5,7.5,8,0.5,15.5,16);
caxis([0 2])
colormap(jet(5))

%%%%%%%%
%A(exp,repeat,joint)
A(1,:,:) = [0.56,0.36,0.71;0.57,0.13,0.62;0.48,0.09,0.55];
A(2,:,:) = [0.65,0.32,0.72;0.49,0.25,0.52;0.43,0.08,0.56];
A(3,:,:) = [0.51,0.30,0.70;0.48,0.02,0.55;0.58,0.01,0.61];
A(4,:,:) = [0.48,0.14,0.56;0.52,0.36,0.71;0.66 0.67 0.74];
A(5,:,:) = [0.43,0.01,0.72;0.65,0.52,0.75;0.56,0.49,0.74];
A(6,:,:) = [0.63,0.03,0.73;0.62,0.50,0.75;0.66, 0.53, 0.75];


for i=1:6
    subplot(3,2,i);
    imagesc(reshape(A(i,:,:),3,3));
    caxis([0 0.7])
    axis off;
end
%imagesc(reshape(A(4,:,:),3,3));axis off;
%imagesc(reshape(A(4,:,:),3,3));axis off;


%% Extract cpg signal

names = {'gas__mff_stance'
'glu__mff_swing'
'glu_gif_stance'
'ham__mff_swing'
'ham_gif_stance'
'ham_hf_mlf_swing'
'hf__mlf_swing'
'hf_gif_stance'
'hf_tl_swing'
'sol__mff_stance'
'sol_ta_mff_stance'
'ta__mlf_cycle'
'vas__mff_stance'
'vas_gcf_doublestancesupport'
'vas_pko_angleoffset'};
for i=1:15
[mean(b(i).offset) mean(b(i).amplitude)];
fid = fopen(['../../v0.7/webots/conf/cpg_data/' names{i} '.txt'],'w+');  % Note the 'wt' for writing in text mode
input = interp1(linspace(0,1,100),a(i).value,0:0.001:1-0.001);
%input = interp1(linspace(0,1,100),a(1).value,0:0.001:1-0.001);
hold on;
plot(input)

fprintf(fid,[num2str(mean(b(i).offset)) ' ' num2str(mean(b(i).amplitude)) '\n']);
fprintf(fid,'%f\n',input);  % The format string is applied to each element of a
fclose(fid);
end

%% compare cpg normal signal
folder = '../../v0.10/raw_files/';
cpgs = load_data([folder 'cpgs' int2str(12)]);
feedbacks = load_data([folder 'feedbacks' int2str(12)]);
fields = fieldnames(cpgs.data);
for i=1:14
    subplot(5,3,i)
    cor(i) = corr(eval(['cpgs.data.' cell2mat(fields(i))]),eval(['feedbacks.data.' cell2mat(fields(i))]));
    plot(feedbacks.time,eval(['feedbacks.data.' cell2mat(fields(i))]));
    hold on;
    plot(cpgs.time,eval(['cpgs.data.' cell2mat(fields(i))]),'r');
    xlim([1.5 4.5]);
    title(strrep(strrep(cell2mat(fields(i)),'_','\_'),'left\_',''));
end
cor(14) = 0.0;
subplot(5,3,15)
imagesc(reshape(cor,5,3))
%% systematic
figure;
what = {
    'a_ar'
    'a_f'
    'a_o'
    'ar_f'
    'ar_o'
    'o_f'
    
    'a_or'
    'ar_or'
    'o_or'
    'f_or'
    
    };
labels = {
        'a'
        'ar'
        'o'
        'or'
        'f'
    };
labelsname = {
        'Amp change (cpg)'
        'Amp change (reflex)'
        'Offset change (cpg)'
        'Offset change (reflex)'
        'Frequency change'
    };

for i=1:10
    name = what{i};
    X=importdata(['../../v0.9/raw_files/' name]);
    %X=importdata('../../v0.10/raw_files/old/systematic4.txt');
    x=unique(X(:,1));
    y=unique(X(:,2));
    X(:,5)=(X(:,3)-9.8)./(X(:,4)-14.299+6.84);

    speed = X(X(:,3) > 34.9 & X(:,5) < 5,:);
    figure
    scatter3(speed(:,1),speed(:,2),speed(:,5),150,speed(:,5),'filled');

    xlabel(labelsname((strcmp(labels, name(1:find(name == '_')-1)))));
    ylabel(labelsname((strcmp(labels, name(find(name == '_')+1:end)))));
end
%% systematic (with correlations)
figure;
what = {
    'a_ar'
    'a_f'
    'a_o'
    'ar_f'
    'ar_o'
    'o_f'};
X=importdata(['../../v0.9/raw_files/' what{4}]);
x=unique(X(:,1));
y=unique(X(:,2));
speed_dim = size(X,2)+1;
X(:,speed_dim)=(X(:,3)-9.8)./(X(:,4)-14.299+6.84);

speed = X(X(:,3) > 34.9 & X(:,speed_dim) < 5.0,:);
subplot(221);
scatter(speed(:,1),speed(:,2),30,speed(:,speed_dim),'filled');
title('speed');
xlabel('Amp % change (cpg)');
ylabel('Amp % change (reflex)');
colorbar;
subplot(222);
scatter(speed(:,1),speed(:,2),30,speed(:,5),'filled');
title('hip correlation');
xlabel('Amp % change (cpg)');
ylabel('Amp % change (reflex)');
colorbar;
subplot(223);
scatter(speed(:,1),speed(:,2),30,speed(:,6),'filled');
title('knee correlation');
xlabel('Amp % change (cpg)');
ylabel('Amp % change (reflex)');
colorbar;
subplot(224);
scatter(speed(:,1),speed(:,2),30,speed(:,7),'filled');
title('ankle correlation');
xlabel('Amp % change (cpg)');
ylabel('Amp % change (reflex)');
colorbar;

%% systematic three parameters
figure
subplot(121);
X=importdata('../../v0.10/raw_files/systematic23.txt');
speed_dim = size(X,2)+1;
X(:,speed_dim)=(X(:,4)-9.8)./(X(:,5)-14.299+6.84);
X = X(X(:,4) > 34.9,:);
x=X(:,1);
y=X(:,2);
z=X(:,3);
c=X(:,speed_dim);
corr_hip=X(:,6);
corr_knee=X(:,7);
corr_ankle=X(:,8);
scatter3(x(:),y(:),z(:),100,c(:),'filled');
xlabel('Amp % change (cpg)');
ylabel('Amp % change (reflex)');
zlabel('Freq % change');

% sort element by frequency
[~,I]=sort(X(:,3));
Xfreq=X(I,:);

z = unique(Xfreq(:,3));
x=unique(Xfreq(:,1));
y=unique(Xfreq(:,2));
l=1;
for i=1:length(x)
    for j=1:length(y)
        x__ = Xfreq(Xfreq(:,1) == x(i) & Xfreq(:,2)== y(j),:);
        if(size(x__,1) ~= 0)
            % hip correlation
            [~,I]=max(x__(:,6),[],1);
            Z_hip(i,j) = x__(I(1),end);
            C_hip(i,j) = x__(I(1),6);
            % knee correlation
            [~,I]=max(x__(:,7),[],1);
            Z_knee(i,j) = x__(I(1),end);
            C_knee(i,j) = x__(I(1),7);
            [~,I]=min(x__(:,7),[],1);
            Z_minknee(i,j) = x__(I(1),end);
            C_minknee(i,j) = x__(I(1),7);
            % ankle correlation
            [~,I]=max(x__(:,8),[],1);
            Z_ankle(i,j) = x__(I(1),end);
            C_ankle(i,j) = x__(I(1),8);
            
            % hip correlation
            [~,I]=max(x__(:,6),[],1);
            Z__ = x__(I(1),end);
            C__ = x__(I(1),6);
            YH(l,:) = [x(i),y(j),x__(I(1),3),Z__,C__];
            % knee correlation
            [~,I]=max(x__(:,7),[],2);
            Z__ = x__(I(1),end);
            C__ = x__(I(1),7);
            YK(l,:) = [x(i),y(j),x__(I(1),3),Z__,C__];
            % ankle correlation
            [~,I]=max(x__(:,8),[],3);
            Z__ = x__(I(1),end);
            C__ = x__(I(1),8);
            YA(l,:) = [x(i),y(j),x__(I(1),3),Z__,C__];
            l=l+1;
        end
    end
end
Z_hip(Z_hip==0) = mean(Z_hip(Z_hip~=0));
Z_minknee(Z_minknee==0) = mean(Z_minknee(Z_minknee~=0));
Z_knee(Z_knee==0) = mean(Z_knee(Z_knee~=0));
Z_ankle(Z_ankle==0) = mean(Z_ankle(Z_ankle~=0));

C_hip(C_hip==0) = mean(C_hip(C_hip~=0));
C_minknee(C_minknee==0) = mean(C_minknee(C_minknee~=0));
C_knee(C_knee==0) = mean(C_knee(C_knee~=0));
C_ankle(C_ankle==0) = mean(C_ankle(C_ankle~=0));


CircleSize=100;
Y = YH;
x = Y(:,1);y = Y(:,2);z = Y(:,3);c = Y(:,4);
figure;h2=scatter3(x,y,z,CircleSize,c,'s','filled');
title('Hip correlation');
xlabel('Amp % change (cpg)');
ylabel('Amp % change (reflex)');
zlabel('Freq % change');
Y = YK;
x = Y(:,1);y = Y(:,2);z = Y(:,3);c = Y(:,4);
figure;h2=scatter3(x,y,z,CircleSize,c,'s','filled');
title('Knee correlation');
xlabel('Amp % change (cpg)');
ylabel('Amp % change (reflex)');
zlabel('Freq % change');

Y = YA;
x = Y(:,1);y = Y(:,2);z = Y(:,3);c = Y(:,4);
figure;h2=scatter3(x,y,z,CircleSize,c,'s','filled');
title('Ankle correlation');
xlabel('Amp % change (cpg)');
ylabel('Amp % change (reflex)');
zlabel('Freq % change');
%%
subplot(122);
X=importdata('../../v0.8/raw_files/systematic.txt');
X(:,6) = (X(:,4)-9.6)./(X(:,5)-10.57+3.148);
%slow:
%X(:,6) = (X(:,4)-8.1)./(X(:,5)-10.57+3.148);
x=unique(X(:,1));
y=unique(X(:,2));
z=unique(X(:,3));

x=X(X(:,4) > 34.9,1);
y=X(X(:,4) > 34.9,2);
z=X(X(:,4) > 34.9,3);
c=X(X(:,4) > 34.9,6);
scatter3(x(:),y(:),z(:),100,c(:),'filled');
xlabel('Amp % change (cpg)');
ylabel('Amp % change (reflex)');
zlabel('Freq % change');
%%% 20.2 meters

% FAST 14.9 secondes >>> 1.36 m/s
%amp_change 1.597
%freq_change 1.203

% SLOW 17.9 secondes >>> 1.13 m/s
%amp_change 0.8879
%freq_change 0.8879

%-> freq/amp change at 10.9 seconde , 9.8 meter