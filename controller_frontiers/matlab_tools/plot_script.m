muscles_name = {'gas','glu','ham','hf','sol','ta','vas'};
leg = 'legend( muscles_name{1}';
plo = 'plot( 1,1';
for i=2:7
	leg = [leg ', muscles_name{' int2str(i) '}'];
	plo = [plo ', 1,1'];
end

plot(linspace(0,1,1000),A(8300:9299,:));
eval([leg ')']);

folder = '../raw_files/human_robot_comparaison/';
what = 'motoneurons';
% generate nominal motoneurones signal
exp_number = 1;
[signal, signals] = extract(what, exp_number, folder);
signal10 = signal;
signals10 = signals;
spike_rate10 = spike_rate;
exp_number = 2;
[signal, signals] = extract(what, exp_number, folder);
signal12 = signal;
signals12 = signals;
spike_rate12 = spike_rate;
exp_number = 3;
[signal, signals] = extract(what, exp_number, folder);
signal13 = signal;
signals13 = signals;
spike_rate13 = spike_rate;
%i=4;
%nominal_emg;
%signal14 = signal;


%% SHOW LEGEND
muscles_name = {'gas','glu','ham','hf','sol','ta','vas'};
legend_name  = {'left_{gas}','left_{glu}','left_{ham}','left_{hf}','left_{sol}','left_{ta}','left_{vas}','right_{gas}','right_{glu}','right_{ham}', 'right_{hf}','right_{sol}','right_{ta}','right_{vas}'};
leg = 'legend( legend_name{1}';
plo = 'plot( 1,1';
for i=2:7
	leg = [leg ', legend_name{' int2str(i) '}'];
	plo = [plo ', 1,1'];
end
eval([plo ')']);
eval([leg ')']);

%% PLOT LIMIT CYCLE BY DOING A PCA IN THE SIGNALS FROM MOTONEURONS
col = lines(3);
subplot(121)
hold on;

A = spike_rate10.val.data(:,1:7);
[W,~,a1] = princomp(A);
plot(A(1000:end-1000,:)*W(:,1),A(1000:end-1000,:)*W(:,2),'Color',col(1,:));
A = spike_rate12.val.data(:,1:7);
[~,~,a2] = princomp(A);
plot(A(1000:end-1000,:)*W(:,1),A(1000:end-1000,:)*W(:,2),'Color',col(2,:));
A = spike_rate13.val.data(:,1:7);
[~,~,a3] = princomp(A);
plot(A(1000:end-1000,:)*W(:,1),A(1000:end-1000,:)*W(:,2),'Color',col(3,:));
legend('1.0 m/s', '1.2 m/s', '1.3 m/s');
title('PC1 and PC2 of the motoneurons signals space');
hold off;
subplot(122);
bar([a1 a2 a3]);
legend('1.0 m/s','1.2 m/s','1.3 m/s'); 
title('Eigenvalues associated with the motoneurons signals');

%% PLOT MEAN SIGNALS BY MOTONEURONS
col = lines(3);
hold on;
labels = spike_rate.val.labels;
for i=1:7
    subplot(4,2,i)
    hold on;
    plot(signal10(i).value,'Color',col(1,:),'LineWidth',2)
    plot(signal12(i).value,'Color',col(2,:),'LineWidth',2)
    plot(signal13(i).value,'Color',col(3,:),'LineWidth',2)
    title(legend_name{i});
    hold off;
end
legend('

%% PLOT AMPLITUDE SIGNALS BY MOTONEURONS
col = lines(3);
hold on;
labels = spike_rate.val.labels;
for i=1:7
    subplot(7,1,i)
    hold on;
    plot(signals10(i).amplitude, '*','Color',col(1,:))
    plot(signals12(i).amplitude, '*','Color',col(2,:))
    plot(signals13(i).amplitude, '*','Color',col(3,:))
    title(legend_name{i});
    hold off;
end

%% PLOT OFFSET SIGNALS BY MOTONEURONS
col = lines(3);
hold on;
labels = spike_rate.val.labels;
for i=1:7
    subplot(7,1,i)
    hold on;
    plot(signals10(i).offset, '*','Color',col(1,:))
    plot(signals12(i).offset, '*','Color',col(2,:))
    plot(signals13(i).offset, '*','Color',col(3,:))
    title(legend_name{i});
    hold off;
end
%% ERROR BAR OFFSETS AMP BY MOTONEURONS
for i=1:7
    o(i,1) = mean(signals10(i).offset);
    o(i,2) = mean(signals12(i).offset);
    o(i,3) = mean(signals13(i).offset);
    o_s(i,1) = std(signals10(i).offset);
    o_s(i,2) = std(signals12(i).offset);
    o_s(i,3) = std(signals13(i).offset);
    a(i,1) = mean(signals10(i).amplitude);
    a(i,2) = mean(signals12(i).amplitude);
    a(i,3) = mean(signals13(i).amplitude);
    a_s(i,1) = std(signals10(i).amplitude);
    a_s(i,2) = std(signals12(i).amplitude);
    a_s(i,3) = std(signals13(i).amplitude);
end
subplot(121)
errorb(o,o_s,'linewidth',0.01)
title('offsets');
set(gca,'XTickLabel',muscles_name);
subplot(122)
errorb(a,a_s,'linewidth',0.01)
title('amplitudes');
set(gca,'XTickLabel',muscles_name);
legend('1.0 m/s','1.2 m/s', '1.3 m/s')
%% PLOT MEAN SIGNALS
subplot(121)
col = lines(8);
hold on;
labels = spike_rate.val.labels;
for i=1:7
    plot(signal(i).value,'Color',col(i,:))
end
eval([leg ')']);
title('mean signal')
hold off;
%% PLOT ALL SIGNALS
col = lines(8);
hold on;
labels = spike_rate.val.labels;
for i=1:7
    plot(signals(i).value,'Color',col(i,:))
end
eval([leg ')']);
hold off;
%    bar(X,Y) draws the columns of the M-by-N matrix Y as M groups of N
%    vertical bars.  The vector X must not have duplicate values.
    s = [o10 o12 o13];
    l(1:length(o10)) = 0;
    l(1+length(o10):length(o10)+length(o12)) = 1;
    l(1+length(o10)+length(o12):length(o10)+length(o12)+length(o13)) = 2;
    subplot(7,2,i*2-1)
    boxplot(s,l)
    title('offset');

    s = [a10 a12 a13];
    l(1:length(a10)) = 0;
    l(1+length(a10):length(a10)+length(a12)) = 1;
    l(1+length(a10)+length(a12):length(a10)+length(a12)+length(a13)) = 2;
    subplot(7,2,i*2)
    boxplot(s,l)
    title('amplitude');
%% PLOT BEST SIGNALS
subplot(122)
col = lines(8);
hold on;
labels = spike_rate.val.labels;
for i=1:7
    plot(signals(i).value(:,signals(i).best),'Color',col(i,:))
end
eval([leg ')']);
title('best signal')
hold off;

%% PLOT ERROR
xlabel('steps')
ylabel('reconstruction error')
col = lines(8);
hold on;
for i=1:7
    plot(signals(i).error,'*','Color',col(i,:))
end
hold off;

%% PLOT AMPLITUDE
subplot(121)
title('amplitudes')
xlabel('steps')
ylabel('amplitude')
col = lines(8);
hold on;
for i=1:7
    plot(signals(i).amplitude,'*','Color',col(i,:))
end
hold off;


%% PLOT OFFSET
subplot(122)
title('offsets')
xlabel('steps')
ylabel('offset')
col = lines(8);
hold on;
for i=1:7
    plot(signals(i).offset,'*','Color',col(i,:))
end
hold off;

%% PLOT AMPLITUDE CORRELATION
subplot(121)
A=[];
for i=1:7
    A = [A signals(i).amplitude(10:end)'];
end


end_i = min(size(A,1));
for i =1:7
    for j = 1:7
        [ss1(i,j) cc1(i,j)] = snr(A(:,i), A(:,j));
    end
end

imagesc(abs(cc1));
caxis([0 1] .* color_bar_scale);
set(gca,'XTick',1:1:7);
title('Signal amplitude correlation');
set(gca,'XTickLabel',legend_name);
set(gca,'YTick',1:1:7);
set(gca,'YTickLabel',legend_name);

colorbar;

%% PLOT OFFSET CORRELATION
subplot(122)
A=[];
for i=1:7
    A = [A signals(i).offset(10:end)'];
end


end_i = min(size(A,1));
for i =1:7
    for j = 1:7
        [ss1(i,j) cc1(i,j)] = snr(A(:,i), A(:,j));
    end
end

imagesc(abs(cc1));
caxis([0 1] .* color_bar_scale);
set(gca,'XTick',1:1:7);
title('Signal offset correlation');
set(gca,'XTickLabel',legend_name);
set(gca,'YTick',1:1:7);
set(gca,'YTickLabel',legend_name);

colorbar;


%% PLOT AMPLITUDE CORRELATION BETWEEN LIMB
subplot(121)
B=[];
for i=8:14
    B = [B signals(i).offset(11:end)'];
end

clear cc1 ss1
end_i = min([size(A,1) size(B,1)]);
for i =1:7
    for j = 1:7
        [ss1(i,j) cc1(i,j)] = snr(A(:,i), B(:,j));
    end
end

imagesc(abs(cc1));
caxis([0 1] .* color_bar_scale);
set(gca,'XTick',1:1:7);
title('Signal amplitude correlation');
set(gca,'XTickLabel',{legend_name{1:7}});
set(gca,'YTick',1:1:7);
set(gca,'YTickLabel',{legend_name{8:14}});

colorbar;

%% PLOT OFFSET CORRELATION BETWEEN LIMB
subplot(122)
B=[];
for i=8:14
    B = [B signals(i).offset(11:end)'];
end


end_i = min([size(A,1) size(B,1)]);
for i =1:7
    for j = 1:7
        [ss1(i,j) cc1(i,j)] = snr(A(:,i), B(:,j));
    end
end

imagesc(abs(cc1));
caxis([0 1] .* color_bar_scale);
set(gca,'XTick',1:1:7);
title('Signal offset correlation');
set(gca,'XTickLabel',{legend_name{1:7}});
set(gca,'YTick',1:1:7);
set(gca,'YTickLabel',{legend_name{8:14}});

colorbar;