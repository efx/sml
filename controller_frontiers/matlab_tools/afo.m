function varargout = afo(varargin)
    perturbed = false;
    if(nargin == 2)
        iny = varargin{2};
        dt = varargin{1};
        Pteach=@(x)iny(round((x/(dt)+1)));
        %Pteach(8);
        %0 -> 0+1
        %0.01 -> 1+1
        %0.02 -> 2+1
        %interp1(varargin{1},varargin{2},x);
    else
        Pteach=@(x) 4*sin(3*x*2*pi)+3;
    end
    mu = 1.;
    gamma = 8.;
    nu = 10.5;
	epsilon = 5.9;
    tau = 2;
    function RESULT = iff(CONDITION,TRUE,FALSE)
        if CONDITION
        RESULT = TRUE();
        else
        RESULT = FALSE();
        end
    end
    function dydt = addOne(y,F,i)
        X=y(1,i);
        Y=y(2,i);
        W=y(3,i);
        A=y(4,1);
        P=y(5,i);
        
        R = sqrt(X^2+Y^2);
        T = sign(X)*acos(-Y/R);
        
        
        dX = gamma*(mu-R^2)*X-W*Y+epsilon*F;%+tau*sin(T-P);
        dY = gamma*(mu-R^2)*Y+W*X;
        dW = -epsilon*F*Y/R;
        %dFF = 0.1*(F-FF);
        dA = nu*X*F;
        %dA = nu*(F-sum(y(1:nEq:end).*y(4:nEq:end)));
              
%        if(i~=1)
%            W0 = y(3,1);
%            T0 = sign(y(1,1))*acos(-y(2,1)/sqrt(y(1,1)^2+y(2,1)^2));
%            dP = sin(W/W0*T0-T-P);
%        else
            dP = 0;
%        end
        dydt(1) = dX;
        dydt(2) = dY;
        dydt(3) = dW;
        dydt(4) = dA;
        dydt(5) = dP;
    end
    function dydt = fprime(t,y)
%        if(t>450 && perturbed == false)
%            y(2:nEq:end) = 10*rand(nOs,1);
%            y(3:nEq:end) = 10*rand(nOs,1);
%            perturbed = true;
%        end
        y=reshape(y(1:end),nEq,nOs);
        Q=sum(y(4,:).*y(1,:));
        if(t>7000)
            F=0;
        else
            F=Pteach(t)-Q;
        end
        
        dydt = zeros(nEq,nOs);
        for i=1:nOs
            dydt(:,i) = addOne(y,F,i);
        end
        
        %dydt = [ dE ; dydt(:) ];
    end

    ode = @(t,y) fprime(t,y);
    h = 0.001;
    % X Y W A P
    y0 = [
        1 0 10 1 0.01
        %1 0 1.0 1 0.01 
        %1 0 3.5 0.1 0.01 
        %1 0 2*pi 1 0.01
        %1 0 100 3.7664 0.01
        %1 0 0 0.1 0.01 
    ]';
    nEq = size(y0,1);
    nOs = size(y0,2);
    %y0 = [ 1 ; y0(:)];
    tspan = [0, 20.0];
    tStart = cputime();
    hw = waitbar(0,sprintf('diff. equation solver'));
    [t,y] = rk4(ode,tspan,y0,h,hw);
    if(nargout == 2)
        varargout{1} = t;
        varargout{2} = y;
    else
        varargout{1}.t = t;
        varargout{1}.epsilon = y(:,1);
        %y = y(:,2:end);
        varargout{1}.x = y(:,1:nEq:end);
        varargout{1}.y = y(:,2:nEq:end);
        varargout{1}.w = y(:,3:nEq:end);
        varargout{1}.a = y(:,4:nEq:end);
        varargout{1}.p = y(:,5:nEq:end);
        varargout{1}.Pteaching = Pteach(t);
        varargout{1}.Qlearned = sum(varargout{1}.x.*varargout{1}.a,2);
    end            
    disp(['duration ' num2str(cputime()-tStart)]);
    close(hw);
end