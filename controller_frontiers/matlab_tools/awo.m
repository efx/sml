function [t,y] = awo(input,w)
    % input should be a vector of points equally spaced of a 
    % one period of a periodic function with period 1.
    % the sampling rate should be twice the one of the diff equation
    in = @(t) input(round(mod(round(1000*t)/1000,1.0)/0.001+1));
    perturb=false;
    function dydt = fprime(t,y,w)
        if(t>6 && perturb==false)
            y = rand(2,1)*10;
            perturb=true;
        end
        dydt = zeros(2,1);
        dydt(1) = w;
        dydt(2) = tau*(in(y(1))-y(2))+(in(y(1)+h/2)-in(y(1)-h/2))/(h)*dydt(1);
    end
    ode = @(t,y) fprime(t,y,w);    
    h = 0.01;
    tau = 100;
    y0 = [0.0;0.0];
    tspan = [0, 40];
    tStart = cputime();
    hw = waitbar(0,sprintf('diff. equation solver'));
    [t,y] = rk4(ode,tspan,y0,h,hw);
    disp(['duration ' num2str(cputime()-tStart)]);
    close(hw);
end