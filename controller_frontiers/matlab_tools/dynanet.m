function varargout = dynanet(varargin)
    %% INPUT
    tspan = [0, 10.0];
	dt = varargin{1};
	iny = varargin{2};
    Pteach=@(x)iny(round((x/dt+1)));
    %Pteach2=@(x)iny2(round((x/dt+1)));
    
    %% global variable to talk between neurons    
    MEAN = mean(iny);
    VARIANCE = var(iny);
    FREQ = 0;
    
    %iny2 = varargin{3};
    if(nargin > 2)
        teacher = varargin{3};
        amplitude = varargin{4};
        offset = varargin{5};
        in = @(t) offset+amplitude*teacher(round(mod(round(t/dt)*dt,1.0)/dt+1));
    end
	
	

    %% Network Functions
    function dydt = getLorenz(y,Fext,t)
        c = 10;
        r = 28;
        b = 8/3;
        dydt = [
            -c*y(1)+c*y(2);
            -y(1)*y(3)+r*y(1)-y(2);
            y(1)*y(2)-b*y(3);
        ];
    end
    function dydt = getMean(y,Fext,t)
        %g = t+dt/2;
        g=20;
        MEAN = y(1);
        dydt = [
             %dt/(t+dt)*y(1)+((t/dt+1)/((t+dt)/dt)^2)*(Fext - 0);
             1/(g)*(Fext-y(1));
             ];
    end
    function dydt = getVariance(y,Fext,t)
        %g = t+dt/2;
        g=20;
        VARIANCE = y(1);
        dydt = [
             %dt/(t+dt)*y(1)+((t/dt+1)/((t+dt)/dt)^2)*(Fext - 0);
             %-y(1)/(g)+1/(g)^2*(dt/2+g)*(Fext-MEAN)^2;
             -1/(g)*(y(1)-(Fext-MEAN)^2);
             ];
    end
    function dydt = getFiltered(y,Pteach,t)
        getFiltered_g = 1;
        Fext = (Pteach-MEAN)/sqrt(VARIANCE);
        dydt = -getFiltered_g*(y(1)-Fext);
    end
    function dydt = getAwo(y,Pteach,t)
        
        getAwo_tau = 10;
        w=FREQ/2/pi;

        T = y(1)-1/(2*pi/y(3)); %P,1 = 3.41
        dydt = [
            w;
            %getAwo_tau*(in(PHASE)-y(1))+(in(PHASE+dt/2)-in(PHASE-dt/2))/(dt)*FREQ
            getAwo_tau*(in(T)-y(2))+(in(T+dt/2)-in(T-dt/2))/(dt)*w;
            1*y(2)*Pteach;
            ];
    end

    function dydt = getAfo(y,Pteach,t)
        getAfo_e = 1;
        getAfo_mu = 1;
        Fext = (Pteach-MEAN)/sqrt(VARIANCE);
        %Fext = Pteach;
        R=getAfo_mu;%y(1);
        E=getAfo_e;
        O=y(2);
        W=y(3);
        dR = (getAfo_mu-R*R)*R+getAfo_e*Fext*cos(O);
        dO = W - E*Fext*sin(O)/R;
        FREQ=W;
        % uncomment this line if you want to have the speed decrease with
        % time.
        dW = -E*Fext*sin(O);
        %dW = -E*Fext*sin(O);
        
        
        dydt(1) = dR;
        dydt(2) = dO;
        dydt(3) = dW;
    end

    %% Network Creation Function
    function [inputs,outputs] = getNetworkIO(net)
        inputs = zeros(length(net),1);
        outputs = zeros(length(net),1);
        
        flag = net{1}.dim;
        outputs(1) = net{1}.output;
        for i=2:length(net)
            outputs(i) = flag+net{i}.output;
            flag = flag+net{i}.dim;
        end
        for i=1:length(net)
            if(net{i}.input == 0)
                inputs(i) = 0;
            else
                inputs(i) = outputs(net{i}.input);
            end
        end
    end
    function ic = getNetworkIC(IC)
        ic=[];
        for i=1:length(IC)
            ic = [ic;cell2mat(IC(i))];
        end
    end
    function [out ic] = newNeuron(fun,input,output,icin)
        out.dim = length(icin);
        out.fun = fun;
        out.input = input;
        out.output = output;
        ic=icin;
    end


    %% Network difference main function
    function dydt = fprime(t,y)
        dydt = zeros(nEq,1);
        %if(t>50)
        Fext=Pteach(t);
        %else
        %    Fext=Pteach2(t);
        %end
        start = 1;
        for i=1:length(net.neurons)
            dim = net.neurons{i}.dim;
            input = net.inputs(i);
            if(input == 0)
                out = net.neurons{i}.fun(y(start:start+dim-1),Fext,t);
            else
                out = net.neurons{i}.fun(y(start:start+dim-1),y(input),t);
            end
            dydt(start:start+dim-1) = out;
            start = start + dim;
        end
        
    end
    ode = @(t,y) fprime(t,y);



    %% Network Definition
    %function [out ic] = newNeuron(dim,fun,input,output,ic)
%     [net.neurons{1} IC{1}] = newNeuron(...
%             @(y,f,t) getFiltered(y,f,t),...
%             0,...
%             1,...
%         0);
%    [net.neurons{1} IC{1}] = newNeuron(...
%            @(y,f,t) getMean(y,f,t),...
%            0,...
%            1,...
%            [3;0.9]...
%        );
%    [net.neurons{1} IC{1}] = newNeuron(...
%           @(y,f,t) getLorenz(y,f,t),...
%           0,...
%           3,...
%            [1;1;1]...
%        );
    [net.neurons{1} IC{1}] = newNeuron(...
            @(y,f,t) getVariance(y,f,t),...
            0,...
            1,...
            [VARIANCE]...
        );
    [net.neurons{2} IC{2}] = newNeuron(...
            @(y,f,t) getMean(y,f,t),...
            0,...
            1,...
            [MEAN]...
        );
    [net.neurons{3} IC{3}] = newNeuron(...
            @(y,f,t) getAfo(y,f,t),...
            0,...
            2,...
            [1;0;0.87*2*pi]...
        );
%     [net.neurons{4} IC{4}] = newNeuron(...
%             @(y,f,t) getFiltered(y,f,t),...
%             1,...
%             1,...
%         0);
    [net.neurons{4} IC{4}] = newNeuron(...
            @(y,f,t) getAwo(y,f,t),...
            0,...
            2,...
             [0;0;0]...
        );
    [net.inputs, net.outputs] = getNetworkIO(net.neurons);
    net.IC = getNetworkIC(IC);
    nEq = length(net.IC);
    
    %% Differential Equation Solver
    y0 = net.IC;
    tStart = cputime();
    hw = waitbar(0,sprintf('diff. equation solver'));
    [t,y] = rk4(ode,tspan,y0,dt,hw);
    
    for i=1:length(net.neurons)
        output(:,i) = y(:,net.outputs(i));
    end
    if(nargout==2)
        varargout{1} = t;
        varargout{2} = output;
    else
        varargout{1} = t;
        varargout{2} = output;
        varargout{3} = y;
    end
    disp(['duration ' num2str(cputime()-tStart)]);
    close(hw);
end