addpath('/usr/local/share/btk-0.1/Wrapping/Matlab/btk/');

folder='/home/efx/Development/PHD/LabImmersion/Regis/human_data/Me/MoCap Data/';
i=2;
acq_normal_walk = btkReadAcquisition([folder 'florian_normal_walk_T06.c3d']);
acq_slow_walk = btkReadAcquisition([folder 'florian_slow_T02.c3d']);
acq_run = btkReadAcquisition([folder 'florian_run_T14.c3d']);

subplot(311)
title('slow walking')
acq = acq_slow_walk;
events = btkGetEvents(acq);
forces = btkGetForces(acq);
angles = btkGetAngles(acq);
hold on; 
h1 = plot(angles.LHipAngles(:,1),'b');
h2 = plot(angles.LKneeAngles(:,1),'g');
h3 = plot(angles.LAnkleAngles(:,1),'r');
h4 = plot(100*events.Left_Foot_Strike',1,'xr');
h5 = plot(100*events.Left_Foot_Off',1,'xb');
legend([h1 h2 h3])
subplot(312)
title('normal walking')
acq = acq_normal_walk;
events = btkGetEvents(acq);
forces = btkGetForces(acq);
angles = btkGetAngles(acq);
hold on; 
plot(angles.LHipAngles(:,1),'b')
plot(angles.LKneeAngles(:,1),'g')
plot(angles.LAnkleAngles(:,1),'r')
plot(100*events.Left_Foot_Strike',1,'xr')
plot(100*events.Left_Foot_Off',1,'xb')

subplot(313)
title('slow running')
acq = acq_run;
events = btkGetEvents(acq);
forces = btkGetForces(acq);
angles = btkGetAngles(acq);
hold on; 
plot(angles.RHipAngles(:,1),'b')
plot(angles.RKneeAngles(:,1),'g')
plot(angles.RAnkleAngles(:,1),'r')
plot(100*events.Right_Foot_Strike',1,'xr')
plot(100*events.Right_Foot_Off',1,'xb')






(min(nw_angles.LHipAngles(130:240,1))+min(nw_angles.LHipAngles(240:350,1))+min(nw_angles.LHipAngles(350:463,1)))/3- (max(nw_angles.LHipAngles(130:240,1))+max(nw_angles.LHipAngles(240:350,1))+max(nw_angles.LHipAngles(350:463,1)))/3
(min(sw_angles.LHipAngles(130:240,1))+min(sw_angles.LHipAngles(240:350,1))+min(sw_angles.LHipAngles(350:463,1)))/3-(max(sw_angles.LHipAngles(130:240,1))+max(sw_angles.LHipAngles(240:350,1))+max(sw_angles.LHipAngles(350:463,1)))/3
(min(nw_angles.LKneeAngles(130:240,1))+min(nw_angles.LKneeAngles(240:350,1))+min(nw_angles.LKneeAngles(350:463,1)))/3- (max(nw_angles.LKneeAngles(130:240,1))+max(nw_angles.LKneeAngles(240:350,1))+max(nw_angles.LKneeAngles(350:463,1)))/3
(min(sw_angles.LKneeAngles(130:240,1))+min(sw_angles.LKneeAngles(240:350,1))+min(sw_angles.LKneeAngles(350:463,1)))/3-(max(sw_angles.LKneeAngles(130:240,1))+max(sw_angles.LKneeAngles(240:350,1))+max(sw_angles.LKneeAngles(350:463,1)))/3


A=btkGetForces(btkReadAcquisition(['florian_run_T13.c3d']));plot(A.RGroundReactionForce);
plot(A.RNormalisedGRF(:,3));


A=btkGetForces(btkReadAcquisition(['florian_normal_walk_T07.c3d']));
subplot(321);plot(A.LNormalisedGRF(:,1));
subplot(323);plot(A.LNormalisedGRF(:,2));
subplot(325);plot(A.LNormalisedGRF(:,3));

A=btkGetForces(btkReadAcquisition(['florian_run_T13.c3d']));

subplot(322);plot(A.RNormalisedGRF(:,1));
subplot(324);plot(A.RNormalisedGRF(:,2));
subplot(326);plot(A.RNormalisedGRF(:,3));




%% EMG
addpath('/usr/local/share/btk-0.1/Wrapping/Matlab/btk/');
folder='/home/efx/Development/PHD/LabImmersion/Regis/human_data/Me/MoCap Data/';

%1-9 -  VAS
%2-10 - TA
%3-11 - GAS
%4-12 - HAM
%5-13 - GLU
%6-14 - GLU

Name{1} = 'VAS';
Name{2} = 'TA';
Name{3} = 'GAS';
Name{4} = 'HAM';
Name{5} = 'GLU';
Name{6} = 'GLU';
Summup_EMG = zeros(6,1000);
Exp_num = 6;
Mus_num = 7;
for j=4:Exp_num
clear Rectified_EMG
acq_normal_walk = btkReadAcquisition([folder 'florian_normal_walk_T06.c3d']);
acq = acq_normal_walk;
AA = btkGetAnalogs(acq);
events = btkGetEvents(acq);
for i=1:Mus_num
Rectified_EMG(i,:) = EMGfilt(eval(['AA.L' num2str(i)])); % quadriceps

Fe=1000; %Samling frequency
Fc=4; % Cut-off frequency (from 2 Hz to 6 Hz depending to the type of your electrod)
N=4; % Filter Order
[B, A] = butter(N,Fc*2/Fe, 'low'); %filter's parameters 
EMG=filtfilt(B, A, Rectified_EMG(i,:)); 
%subplot(6,1,i)
%plot(1000*events.Left_Foot_Strike',0.005,'xr');
%plot(1000*events.Left_Foot_Off',0.005,'xb');
%plot(EMG)
deb = events.Left_Foot_Strike(3)*1000;
fin = events.Left_Foot_Strike(4)*1000;
Summup_EMG(i,:) = 1/Exp_num*interp1(EMG(deb:fin),linspace(1,length(EMG(deb:fin)),1000));
%plot(EMG)
%xlim();
%title(Name{i});
end
end
Summup_EMG(5,:) = 0.5*(Summup_EMG(5,:)+Summup_EMG(6,:));
j=1;
for i=[5 4 1 3 2]
    subplot(5,1,j)
    plot(Summup_EMG(i,:))
    title(Name{i});
    j=j+1;
end