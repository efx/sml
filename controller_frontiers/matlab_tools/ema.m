function varargout = ema(varargin)
    if(nargin == 2)
        iny = varargin{2};
        inx = varargin{1};

        Pteach=@(x)iny(round((x/(inx(2)-inx(1))+1)));
    else
        Pteach=@(x) 4*sin(3*x*2*pi)+3;
    end;
    function dydt = addOne(y,F,i,t)
        X=y(1,i);
        A=y(2,i);
   
        dX = -A/(1-A)*(X-F);
        dA = gamma*(alpha-A);
        %dX = -alpha*(X-F)+alpha*(dF);

        
        dydt(1) = dX;
        dydt(2) = dA;

    end
    function dydt = fprime(t,y)

        y=reshape(y(1:end),nEq,nOs);
        F=Pteach(t);
        dydt = zeros(nEq,nOs);
        for i=1:nOs
            dydt(:,i) = addOne(y,F,i,t);
        end

    end

    ode = @(t,y) fprime(t,y);
    h = 0.001;
    w = 3;
    alpha = 1/(3*2*pi/h);
    gamma = 0.1;
    y0 = [
        50 0.9
    ]';
    nEq = size(y0,1);
    nOs = size(y0,2);

    tspan = [0, 40.0];
    tStart = cputime();
    hw = waitbar(0,sprintf('diff. equation solver'));
    [t,y] = rk4(ode,tspan,y0,h,hw);
    if(nargout == 2)
        varargout{1} = t;
        varargout{2} = y;
    else
        varargout{1}.t = t;
        varargout{1}.x = y(:,1:nEq:end);
        varargout{1}.a = y(:,2:nEq:end);
        varargout{1}.Pteaching = Pteach(t);
    end            
    disp(['duration ' num2str(cputime()-tStart)]);
    close all;
end