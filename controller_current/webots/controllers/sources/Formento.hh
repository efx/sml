#ifndef __Formento_HH__
#define __Formento_HH__


#include <string>
#include <math.h>
#include <vector>
#include <fstream>
#include <map>

//#include <boost/algorithm/string.hpp>


#include <webots/Servo.hpp>
#include <webots/Supervisor.hpp>
#include <webots/TouchSensor.hpp>

#include "Experiment.hh"

#include <sml/types/types.h>
#include <sml/sml-tools/PerturbationManager.hh>
#include <sml/musculoSkeletalSystem/Sml.hh>

#define DEBUG 0


class RobFormento;

class ControllerFormento: public Controller{
public:
    ControllerFormento(RobFormento* robot);
    RobFormento* robot;
    void update();
};

class RobFormento: public webots::Supervisor, public Sml{
private:
    ControllerFormento controller;
    void ConstantInit();
    void InputInit();
    void InputUpdate();
    void initialiseSpinalControl();
    void step_SPINE_TO_MTU(double dt);
public:
    RobFormento(): controller(this){
        sml::init();
    }
    typedef Sml sml;
    typedef Supervisor supervisor;
    int  step();
};


class ExperimentFormento: public BaseExperiment{
public: 
    ExperimentFormento(RobFormento *body) : BaseExperiment(body), body(body) {};
    RobFormento *body;
    void initiation();
    void termination();
    void step();
};

#endif