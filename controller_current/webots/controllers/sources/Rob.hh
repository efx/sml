#ifndef __Rob_HH__
#define __Rob_HH__


#include <string>
#include <math.h>
#include <vector>
#include <fstream>
#include <map>

//#include <boost/algorithm/string.hpp>


#include <webots/Servo.hpp>
#include <webots/Supervisor.hpp>
#include <webots/TouchSensor.hpp>

#include <sml/types/types.h>
#include <sml/sml-tools/PerturbationManager.hh>
#include <sml/musculoSkeletalSystem/GeyerSml.hh>

#define DEBUG 0


typedef struct{
  int is_in_lift_mode; 
  double last_force_y;
  double last_force_z;
  double last_force_pos;
} PtoW; //from Physic plugin to Webots

typedef struct{
//  int reinitialisation;
  int backward;
  double force_amplitude;
} WtoP; //from Webots to Physic plug in

typedef struct{
  double muscle_signal;
  double muscle_activity;
} WtoM; //from Webots to Raw supervisor

class Rob: public webots::Supervisor, public GeyerSml
{
private:
    bool fixedViewPoint;
    int counter;

    void ConstantInit();
    void InputInit();
    void InputUpdate();

    std::vector<std::string> parameterControlVec;
    std::map<std::string, std::string> parameterControl;
    int control;
    double* parameterControlValue;
    std::string parameterControlName;
    std::map<std::string, int> parametersColor;

public:
    typedef Supervisor supervisor;
    typedef GeyerSml sml;
    int step();
    
    //Receiver
    webots::Receiver* receiver;
    PtoW *message_PtoW;
    webots::Emitter* emitter;
    WtoP message_WtoP;
    //Emitter
    webots::Emitter* emitter_matlab;
    webots::Emitter* emitter_analyzer;
    WtoM message_WtoM;
    

  //-----------------------------------
    
    //constructor
    Rob(): GeyerSml(){
        sml::init();
        init();
        }
    
    //destructor
    //~Rob();
    void initKeyboardControl();
    void initWebotsCommunication();
    void init();
    
    void listen_();
    void emit_();
    void fixedYAxis(double Y);    
    void plotTextInWindow(int color);
    void stepParametersControl();
};

#endif /* __ROB_HH__ */
