#include "Formento.hh"

#include <webots/Node.hpp>
#include <webots/Receiver.hpp>
#include <webots/PositionSensor.hpp>
#include <webots/Motor.hpp>
#include <webots/Emitter.hpp>
#include <boost/xpressive/xpressive.hpp>

#include <sml/sml-tools/Settings.hh>
#include <sml/sml-tools/Helper.hh>
#include <sml/sml-tools/FitnessManager.hh>
#include <sml/sml-tools/PhaseManager.hh>

using namespace std;
using namespace webots;
using namespace boost::xpressive;


extern double debug;
extern EventManager* eventManager;
extern SmlParameters parameters;


ControllerFormento::ControllerFormento(RobFormento* robot) : robot(robot){};
void ControllerFormento::update(){
    for(int i=0; i< robot->muscles.size(); i++){
        *outputs.get<double*>(MUSCLES::toString(i)) = 0.3;
    }
}


void ExperimentFormento::step(){
	body->step();
}
void ExperimentFormento::initiation(){

}
void ExperimentFormento::termination(){
	std::map<std::string,double> fitnesses = FitnessManager::buildFitness();

    if(Settings::isOptimization())
    {
        optimizer &opti = tryGetOpti();
        opti.Respond(fitnesses);
    }
}


void RobFormento::step_SPINE_TO_MTU(double dt){
    controller.update();
}

void RobFormento::initialiseSpinalControl(){
    for(auto&kv : sensors){
        controller.inputs.get<const double*>(kv.first) = &kv.second->get();
    } 
    controller.inputs.get<const SIDE::Side*>("finishing_stance") = &finishing_stance;

    for(int i=0; i< muscles.size(); i++){
        controller.outputs.get<double*>(MUSCLES::toString(i)) = &muscles[i]->stim;
    }
}


int RobFormento::step(){
    sml::step();
    
    for (
        unsigned joint=JOINT::FIRST_JOINT,
                 joint_tau=OUTPUT::FIRST_JOINT; 
        joint_tau<=OUTPUT::LAST_JOINT; 
        joint_tau++,
        joint++
        ){
        double torque;
            torque = -sml::Output[joint_tau];
            getMotor(JOINT::toString(joint))->setTorque(torque);
        }

    return 0;
}

void RobFormento::ConstantInit(){
    // Segment constant
    sml::Constant[CONSTANT::LENGTH_TRUNK] = 0.8;
    sml::Constant[CONSTANT::LENGTH_THIGH] = 0.5;
    sml::Constant[CONSTANT::LENGTH_SHIN] = 0.5;
    sml::Constant[CONSTANT::LENGTH_ANKLE] = 0.1;
    sml::Constant[CONSTANT::LENGTH_FOOT] = 0.16;
    sml::Constant[CONSTANT::WEIGHT_TRUNK] = 53.5;
    sml::Constant[CONSTANT::WEIGHT_THIGH] = 8.5;
    sml::Constant[CONSTANT::WEIGHT_SHIN] = 3.5;
    sml::Constant[CONSTANT::WEIGHT_ANKLE] = 0.0;
    sml::Constant[CONSTANT::WEIGHT_FOOT] = 1.25;
    sml::Constant[CONSTANT::MODEL_WEIGHT] = 70;
    sml::Constant[CONSTANT::MODEL_HEIGHT] = 1.8;
    
        

    
    // Joints constant
    sml::Constant[CONSTANT::REF_POS_HIP_LEFT] = 0;
    sml::Constant[CONSTANT::REF_POS_HIP_RIGHT] = 0;
    sml::Constant[CONSTANT::MAX_POS_HIP_LEFT] = 50.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
    sml::Constant[CONSTANT::MAX_POS_HIP_RIGHT] = 50.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
    sml::Constant[CONSTANT::MIN_POS_HIP_LEFT] = -160.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
    sml::Constant[CONSTANT::MIN_POS_HIP_RIGHT] = -160.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
#ifdef MODEL3D
    sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT] = 0.0;
    sml::Constant[CONSTANT::REF_POS_HIPCOR_RIGHT] = 0.0;
    sml::Constant[CONSTANT::MAX_POS_HIPCOR_LEFT] = 80.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT];
    sml::Constant[CONSTANT::MAX_POS_HIPCOR_RIGHT] = 80.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT];
    sml::Constant[CONSTANT::MIN_POS_HIPCOR_LEFT] = 0.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT];
    sml::Constant[CONSTANT::MIN_POS_HIPCOR_RIGHT] = 0.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT];
#endif
    
    

    sml::Constant[CONSTANT::REF_POS_KNEE_LEFT] = 0;
    sml::Constant[CONSTANT::REF_POS_KNEE_RIGHT] = 0;
    sml::Constant[CONSTANT::MAX_POS_KNEE_LEFT] = 135  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::MAX_POS_KNEE_RIGHT] = 135  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];    
    sml::Constant[CONSTANT::MIN_POS_KNEE_LEFT] = 5  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::MIN_POS_KNEE_RIGHT] = 5  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];



    sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT] = 0;
    sml::Constant[CONSTANT::REF_POS_ANKLE_RIGHT] = 0;
    sml::Constant[CONSTANT::MAX_POS_ANKLE_LEFT] = 40.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::MAX_POS_ANKLE_RIGHT] = 40.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::MIN_POS_ANKLE_LEFT] = -20.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::MIN_POS_ANKLE_RIGHT] = -20.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    // Mono-articular Muscle constant
    
    
    sml::Constant[CONSTANT::R_SOL] = 0.05;
    sml::Constant[CONSTANT::R_TA] = 0.04;
    sml::Constant[CONSTANT::R_VAS] = 0.06;
    sml::Constant[CONSTANT::R_GLU] = 0.10;
    sml::Constant[CONSTANT::R_HF] = 0.10;
    sml::Constant[CONSTANT::MAX_PHI_SOL] = 20.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::MAX_PHI_TA] = -10.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];;
    sml::Constant[CONSTANT::MAX_PHI_VAS] = 15.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];;
    sml::Constant[CONSTANT::MAX_PHI_GLU] = -180.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];;
    sml::Constant[CONSTANT::MAX_PHI_HF] = -180.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];;
    sml::Constant[CONSTANT::REF_PHI_SOL] = -10.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];;
    sml::Constant[CONSTANT::REF_PHI_TA] = 20.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];;
    sml::Constant[CONSTANT::REF_PHI_VAS] = 55.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];;
    sml::Constant[CONSTANT::REF_PHI_GLU] = -30.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];;
    sml::Constant[CONSTANT::REF_PHI_HF] = 0.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];;
    sml::Constant[CONSTANT::OPT_L_SOL] = 0.04;
    sml::Constant[CONSTANT::OPT_L_TA] = 0.06;
    sml::Constant[CONSTANT::OPT_L_VAS] = 0.08;
    sml::Constant[CONSTANT::OPT_L_GLU] = 0.11;
    sml::Constant[CONSTANT::OPT_L_HF] = 0.11;
    sml::Constant[CONSTANT::SLACK_L_SOL] = 0.26;
    sml::Constant[CONSTANT::SLACK_L_TA] = 0.24;
    sml::Constant[CONSTANT::SLACK_L_VAS] = 0.23;
    sml::Constant[CONSTANT::SLACK_L_GLU] = 0.13;
    sml::Constant[CONSTANT::SLACK_L_HF] = 0.10;
    sml::Constant[CONSTANT::MAX_V_SOL] = 6.0;
    sml::Constant[CONSTANT::MAX_V_TA] = 12.0;
    sml::Constant[CONSTANT::MAX_V_VAS] = 12.0;
    sml::Constant[CONSTANT::MAX_V_GLU] = 12.0;
    sml::Constant[CONSTANT::MAX_V_HF] = 12.0;
    sml::Constant[CONSTANT::MASS_SOL] = 1.36;
    sml::Constant[CONSTANT::MASS_TA] = 0.29;
    sml::Constant[CONSTANT::MASS_VAS] = 2.91;
    sml::Constant[CONSTANT::MASS_GLU] = 1.4;
    sml::Constant[CONSTANT::MASS_HF] = 1.87;
    sml::Constant[CONSTANT::PENNATION_SOL] = 0.5;
    sml::Constant[CONSTANT::PENNATION_TA] = 0.7;
    sml::Constant[CONSTANT::PENNATION_VAS] = 0.7;
    sml::Constant[CONSTANT::PENNATION_GLU] = 0.5;
    sml::Constant[CONSTANT::PENNATION_HF] = 0.5;
    sml::Constant[CONSTANT::TYPE1FIBER_SOL] = 0.81;
    sml::Constant[CONSTANT::TYPE1FIBER_TA] = 0.7;
    sml::Constant[CONSTANT::TYPE1FIBER_VAS] = 0.5;
    sml::Constant[CONSTANT::TYPE1FIBER_GLU] = 0.5;
    sml::Constant[CONSTANT::TYPE1FIBER_HF] = 0.5;
    sml::Constant[CONSTANT::DIRECTION_SOL] = -1.0;
    sml::Constant[CONSTANT::DIRECTION_TA] = 1.0;
    sml::Constant[CONSTANT::DIRECTION_VAS] = 1.0;
    sml::Constant[CONSTANT::DIRECTION_GLU] = -1.0;
    sml::Constant[CONSTANT::DIRECTION_HF] = 1.0;
    // Mono-articular Muscle constant    
    sml::Constant[CONSTANT::R_GAS_ANKLE] = 0.05;
    sml::Constant[CONSTANT::R_GAS_KNEE] = 0.05;
    sml::Constant[CONSTANT::R_HAM_KNEE] = 0.05;
    sml::Constant[CONSTANT::R_HAM_HIP] = 0.08;
    sml::Constant[CONSTANT::MAX_PHI_GAS_ANKLE] = 20.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::MAX_PHI_GAS_KNEE] = 40.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::MAX_PHI_HAM_KNEE] = 0.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::MAX_PHI_HAM_HIP] =  -180.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
    sml::Constant[CONSTANT::REF_PHI_GAS_ANKLE] = -10.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::REF_PHI_GAS_KNEE] = 15.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::REF_PHI_HAM_KNEE] = 0.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::REF_PHI_HAM_HIP] = -25.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
    sml::Constant[CONSTANT::OPT_L_GAS] = 0.05;
    sml::Constant[CONSTANT::OPT_L_HAM] = 0.10;
    sml::Constant[CONSTANT::SLACK_L_GAS] = 0.40;
    sml::Constant[CONSTANT::SLACK_L_HAM] = 0.31;
    sml::Constant[CONSTANT::MAX_V_GAS] = 12.0;
    sml::Constant[CONSTANT::MAX_V_HAM] = 12.0;
    sml::Constant[CONSTANT::MASS_GAS] = 0.45;
    sml::Constant[CONSTANT::MASS_HAM] = 1.82;
    sml::Constant[CONSTANT::PENNATION_GAS] = 0.7;
    sml::Constant[CONSTANT::PENNATION_HAM] = 0.7;
    sml::Constant[CONSTANT::TYPE1FIBER_GAS] = 0.54;
    sml::Constant[CONSTANT::TYPE1FIBER_HAM] = 0.44;
    sml::Constant[CONSTANT::DIRECTION_GAS] = -1.0;
    sml::Constant[CONSTANT::DIRECTION_HAM] = -1.0;
    
   if(Settings::get<int>("coman")){
    sml::Constant[CONSTANT::LENGTH_TRUNK] = 0.8;
    sml::Constant[CONSTANT::LENGTH_THIGH] = 0.5;
    sml::Constant[CONSTANT::LENGTH_SHIN] = 0.5;
    sml::Constant[CONSTANT::LENGTH_ANKLE] = 0.1;
    sml::Constant[CONSTANT::LENGTH_FOOT] = 0.16;
    sml::Constant[CONSTANT::WEIGHT_TRUNK] = 53.5;
    sml::Constant[CONSTANT::WEIGHT_THIGH] = 8.5;
    sml::Constant[CONSTANT::WEIGHT_SHIN] = 3.5;
    sml::Constant[CONSTANT::WEIGHT_ANKLE] = 0.0;
    sml::Constant[CONSTANT::WEIGHT_FOOT] = 1.25;
    sml::Constant[CONSTANT::MODEL_WEIGHT] = 70;
    sml::Constant[CONSTANT::MODEL_HEIGHT] = 0.9;
    }
}

void RobFormento::InputInit(){
        for (unsigned joint_pos=INPUT::FIRST_JOINT, joint=JOINT::FIRST_JOINT; joint_pos<=INPUT::LAST_JOINT; joint_pos++, joint++){
#ifdef WEBOTS6
            getServo(JOINT::toString(joint))->enablePosition(time_step);
#elif WEBOTS8
            getPositionSensor(JOINT::toString(joint)+"_POS")->enable(time_step);
#else
            getMotor(JOINT::toString(joint))->enablePosition(time_step);
#endif
        }
        for (unsigned sensor_force=INPUT::FIRST_FORCE_SENSOR; sensor_force<=INPUT::LAST_FORCE_SENSOR; sensor_force++){
            getTouchSensor(INPUT::toString(sensor_force))->enable(time_step);
        }
        InputUpdate();
}
void RobFormento::InputUpdate(){
    double sintheta = supervisor::getFromDef(Settings::get<std::string>("robot_name"))->getOrientation()[5];
    sml::Input[INPUT::THETA_TRUNK] = -asin(sintheta) ;
    for (unsigned joint_pos=INPUT::FIRST_JOINT, joint=JOINT::FIRST_JOINT; joint_pos<=INPUT::LAST_JOINT; joint_pos++, joint++){
        //cout << getServo("HIP_LEFT")->getPosition() << endl;
#ifdef WEBOTS6
         sml::Input[joint_pos] = getServo(JOINT::toString(joint))->getPosition();
#elif WEBOTS8
         sml::Input[joint_pos] = getPositionSensor(JOINT::toString(joint)+"_POS")->getValue();
#else
         sml::Input[joint_pos] = getMotor(JOINT::toString(joint))->getPosition();
#endif
    }
    for (unsigned sensor_force=INPUT::FIRST_FORCE_SENSOR; sensor_force<=INPUT::LAST_FORCE_SENSOR; sensor_force++){
        sml::Input[sensor_force] = -getTouchSensor(INPUT::toString(sensor_force))->getValues()[2];
        //cout << sml::Input[sensor_force] << " ----- ";
    }
    //cout << endl;
    sml::Input[INPUT::NECK_Y]       = supervisor::getFromDef(Settings::get<std::string>("robot_name"))->getPosition()[1];
    sml::Input[INPUT::NECK_X]       = supervisor::getFromDef(Settings::get<std::string>("robot_name"))->getPosition()[2];
#ifdef MODEL3D
    sml::Input[INPUT::ANGLE_HIPCOR_RIGHT] *= -1.0;
    sml::Input[INPUT::THETACOR_TRUNK] = -atan2(supervisor::getFromDef(Settings::get<std::string>("robot_name"))->getOrientation()[1],supervisor::getFromDef(Settings::get<std::string>("robot_name"))->getOrientation()[4]);
#endif
    sml::Input[INPUT::TOE_LEFT_X] = supervisor::getFromDef("SENSOR_TOE_LEFT")->getPosition()[0];
    sml::Input[INPUT::TOE_LEFT_Y] = supervisor::getFromDef("SENSOR_TOE_LEFT")->getPosition()[1];
    sml::Input[INPUT::TOE_LEFT_Z] = supervisor::getFromDef("SENSOR_TOE_LEFT")->getPosition()[2];
    sml::Input[INPUT::TOE_RIGHT_X] = supervisor::getFromDef("SENSOR_TOE_RIGHT")->getPosition()[0];
    sml::Input[INPUT::TOE_RIGHT_Y] = supervisor::getFromDef("SENSOR_TOE_RIGHT")->getPosition()[1];
    sml::Input[INPUT::TOE_RIGHT_Z] = supervisor::getFromDef("SENSOR_TOE_RIGHT")->getPosition()[2];
    sml::Input[INPUT::HEEL_LEFT_X] = supervisor::getFromDef("SENSOR_HEEL_LEFT")->getPosition()[0];
    sml::Input[INPUT::HEEL_LEFT_Y] = supervisor::getFromDef("SENSOR_HEEL_LEFT")->getPosition()[1];
    sml::Input[INPUT::HEEL_LEFT_Z] = supervisor::getFromDef("SENSOR_HEEL_LEFT")->getPosition()[2];
    sml::Input[INPUT::HEEL_RIGHT_X] = supervisor::getFromDef("SENSOR_HEEL_RIGHT")->getPosition()[0];
    sml::Input[INPUT::HEEL_RIGHT_Y] = supervisor::getFromDef("SENSOR_HEEL_RIGHT")->getPosition()[1];
    sml::Input[INPUT::HEEL_RIGHT_Z] = supervisor::getFromDef("SENSOR_HEEL_RIGHT")->getPosition()[2];

#ifdef WEBOTS6
    double lefthipx = supervisor::getFromDef("HIP_LEFT")->getPosition()[0];
    double lefthipy = supervisor::getFromDef("HIP_LEFT")->getPosition()[1];
    double lefthipz = supervisor::getFromDef("HIP_LEFT")->getPosition()[2];
    double righthipx = supervisor::getFromDef("HIP_RIGHT")->getPosition()[0];
    double righthipy = supervisor::getFromDef("HIP_RIGHT")->getPosition()[1];
    double righthipz = supervisor::getFromDef("HIP_RIGHT")->getPosition()[2];
#else
    double lefthipx = supervisor::getFromDef("LEFT_LEG")->getPosition()[0];
    double lefthipy = supervisor::getFromDef("LEFT_LEG")->getPosition()[1];
    double lefthipz = supervisor::getFromDef("LEFT_LEG")->getPosition()[2];
    double righthipx = supervisor::getFromDef("RIGHT_LEG")->getPosition()[0];
    double righthipy = supervisor::getFromDef("RIGHT_LEG")->getPosition()[1];
    double righthipz = supervisor::getFromDef("RIGHT_LEG")->getPosition()[2];
#endif
    sml::Input[INPUT::MIDHIP_X] = lefthipx/2.0+righthipx/2.0;
    sml::Input[INPUT::MIDHIP_Y] = lefthipy/2.0+righthipy/2.0;
    sml::Input[INPUT::MIDHIP_Z] = lefthipz/2.0+righthipz/2.0;
}

