//#include "Python.h"
#include <iostream>
#include <string>
#include <optimization/webots.hh> //to include the optimizer framework
#include <unistd.h>
#define GetCurrentDir getcwd


using namespace optimization::messages::task;
using namespace std;

//static string path="/home/dzeladin/Development/sml/controller_current";
static string path="/home/efx/Development/PHD/Airi/controller_current";
string getFullPath(){
	char buf[FILENAME_MAX];
	if (!GetCurrentDir(buf, sizeof(buf)))
		exit;
	std::string str(buf, std::find(buf, buf + FILENAME_MAX, '\0'));
	return str;
	
}

string int2str(int Number){
	return static_cast<ostringstream*>( &(ostringstream() << Number) )->str();
}
int main(int argc, char *argv[])
{
	optimization::TaskReader reader = optimization::TaskReader(cin);
	optimization::messages::task::Task &task = reader.Task();
//	const optimization::messages::task::Task &task = reader.Task();
	long unikid = task.uniqueid();
	string exec = path + "/webots_tools/wavy_ground_generator.py " + int2str(unikid) + ".wbt -p 15 -w 30 -r 1 ";
	system(exec.c_str());
	

//	Py_SetProgramName(argv[0]);  /* optional but recommended */
//	Py_Initialize();
//	string exec_python = "import os; print os.path.abspath('../webots/worlds/')+'/'+'" + int2str(unikid) + ".wbt'\n";
	cout << path << "/webots/worlds/" << unikid << ".wbt" << endl;
//	PyRun_SimpleString(exec_python.c_str());
//	Py_Finalize();
	return 0;
}
